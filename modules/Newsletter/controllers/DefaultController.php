<?php

namespace app\modules\Newsletter\controllers;

use yii\web\Controller;

/**
 * Default controller for the `Newsletter` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
