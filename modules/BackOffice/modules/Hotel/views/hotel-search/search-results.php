<?php

    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\helpers\Inflector;

?>

<div>
    <h4 class="text-center"><?= count($results) ?> hoteluri desponibile</h4>
</div>
<div id="pager-wrap">
    <?php foreach ($results as $key => $result) { ?>
    <div class="row page-item">
        <div class="col-md-3 text-center">
            <img style="max-height: 190px;" src="<?= empty($result['Image']) ? 'https://www.mayflex.com/sites/default/files/no-image-box.png' : $result['Image'] ?>" class="img-responsive" />
        </div>
        <div class="col-md-6">
            <h3><?= $result['Name'] ?></h3>
            <div><?= $result['Address'] ?></div>
            <hr />
            <div><?= $result['RoomInfo'] ?></div>
        </div>
        <div class="col-md-3 text-center">
            <div>
                <?php for ($i = 1; $i < $result['Stars']; $i++) { ?>
                <i style="color: #bba548" class="fa fa-star"></i>
                <?php } ?>
            </div>
            <br />
            <div>
                <div>CEL MAI BUN TARIF</div>
                <h4><?= $result['MinPrice'] ?> <?= $result['Currency'] ?></h4>
                <div>
                    <?= Html::beginForm('/hotel/' . Inflector::slug($result['Name'])) ?>
                        <?= Html::hiddenInput('key', $key) ?>
                        <?= Html::hiddenInput('searchID', $searchID) ?>
                        <?= Html::hiddenInput('OperatorID', $result['OperatorID']) ?>
                        <?= Html::submitButton('Detalii', [
                            'class' => 'btn btn-primary'
                        ]) ?>
                    <?= Html::endForm() ?>
                </div>
            </div>
        </div>
    </div>
    <hr />
    <?php } ?>
</div>
