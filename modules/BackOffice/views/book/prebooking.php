<?php

    use yii\bootstrap\Html;
    use yii\widgets\ActiveForm;

?>

<div class="container">
    
    <h1>Datele client</h1>
    
    <?php $form = ActiveForm::begin() ?>
    
        <?= $form->field($bookingModel, 'OperatorID')->hiddenInput()->label(false) ?>
        <?= $form->field($bookingModel, 'ExternalID')->hiddenInput()->label(false) ?>
        <?= $form->field($bookingModel, 'RateKey')->hiddenInput()->label(false) ?>
        <?= $form->field($bookingModel, 'RateType')->hiddenInput()->label(false) ?>
    
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($bookingClientModel, 'FirstName')->textInput() ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($bookingClientModel, 'LastName')->textInput() ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($bookingClientModel, 'Email')->textInput() ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($bookingClientModel, 'Phone')->textInput() ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <?= $form->field($bookingClientModel, 'City')->textInput() ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($bookingClientModel, 'Street')->textInput() ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($bookingClientModel, 'StreetNumber')->textInput() ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($bookingClientModel, 'Apartament')->textInput() ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($bookingClientModel, 'Zip')->textInput() ?>
            </div>
        </div>
    
        <?= Html::submitButton('Trimite', [
            'class' => 'btn btn-primary'
        ]) ?>
    
    <?php ActiveForm::end() ?>
    
    <br />
    <br />
    
</div>