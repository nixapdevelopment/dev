<?php

namespace app\modules\BackOffice;

use Yii;
/**
 * backoffice module definition class
 */
class BackOffice extends \app\components\Module\BackOfficeModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\BackOffice\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }
}
