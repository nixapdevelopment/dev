<?php

use yii\helpers\Html;
use app\components\ActiveForm\ActiveForm;
use yii\widgets\Pjax;
use app\modules\Judet\models\Judet;
use app\modules\PunctDeLucru\models\PunctDeLucru;
use app\components\DatePicker\DatePicker;
use app\components\Enum\CASIndividuala;

/* @var $this yii\web\View */
/* @var $model app\modules\Salariati\models\Salariati */
/* @var $form yii\widgets\ActiveForm */

$checkboxMinTemplate = '
    <div class="checkbox">
        <div style="position: relative;top: 0px;">
            {beginLabel}{input} <b style="position:relative; top: 0px;">{labelTitle}</b>{endLabel}
        </div>
        <div>{error}{hint}</div>
    </div>
';

?>

<div id="edit-form-wrap">

    <?php Pjax::begin([
        'id' => 'edit-form-pjax-' . md5(microtime(true)),
        'enablePushState' => false,
    ]); ?>

        <?php $form = ActiveForm::begin([
            'id' => 'edit-form-' . md5(microtime(true)),
            'options' => [
                'data-pjax' => true
            ]
        ]); ?>
    
        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'Nume')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'Prenume')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'PunctDeLucru')->dropDownList(PunctDeLucru::getList()) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'Functie')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-2">
                <?= $form->field($model, 'DataAngajarii')->widget(DatePicker::className(), [
                    'options' => [
                        'value' => date(Yii::$app->params['displayDateFormat'], strtotime($model->DataAngajarii)),
                    ]
                ]) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'Tip')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'NormaPeZi')->textInput() ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'OrePeLuna')->textInput() ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'FunctieBaza')->checkbox() ?>
            </div>
        </div>
    
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'CASIndividuala')->dropDownList(CASIndividuala::getList()) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'CASAngajator')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
    
        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'ContrSomajIndividuala')->checkbox([
                    'template' => $checkboxMinTemplate,
                ]) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'ContrSomajAngajator')->checkbox([
                    'template' => $checkboxMinTemplate,
                ]) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'ContrFGCS')->checkbox([
                    'template' => $checkboxMinTemplate,
                ]) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'ContrAccMunca')->checkbox([
                    'template' => $checkboxMinTemplate,
                ]) ?>
            </div>
        </div>
    
        <div class="row">
            <div class="col-md-2 col-md-offset-1">
                <?= $form->field($model, 'ZileCOAn')->textInput() ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'TipSalariu')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'Avans')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'SalariuBrut')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'SalariuOrar')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
    
        <hr />
    
        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'CNP')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'Judet')->dropDownList(Judet::getList('Selectati')) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'Localitate')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'Strada')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-1">
                <?= $form->field($model, 'Numar')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-2">
                <?= $form->field($model, 'CodPostal')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'Bloc')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'Scara')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'Etaj')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'Apartament')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'Sector')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'Telefon')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'Email')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        
        <hr />
        
        <div class="row">
            <div class="col-md-2">
                <?= $form->field($model, 'NrContract')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'DataContract')->widget(DatePicker::className(), [
                    'options' => [
                        'value' => date(Yii::$app->params['displayDateFormat'], strtotime($model->DataContract)),
                    ]
                ]) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'CasaDeSanatate')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'Impozitat')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'TipPlata')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'Pensionar')->checkbox([
                    'template' => $checkboxMinTemplate,
                ]) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'FaraContribCCI')->checkbox([
                    'template' => $checkboxMinTemplate,
                ]) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'FaraContribSanatateAngajator')->checkbox([
                    'template' => $checkboxMinTemplate,
                ]) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'FaraContribSanatateSalariat')->checkbox([
                    'template' => $checkboxMinTemplate,
                ]) ?>
            </div>
        </div>
        
        <hr />
        
        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'CISerieNumar')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'CIEliberatDe')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'CIEliberatData')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'NormaPeZiSpecific')->textInput() ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'OrePeLunaSpecific')->textInput() ?>
            </div>
        </div>
        
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    
    <?php Pjax::end(); ?>

</div>
