<?php

namespace app\modules\Salariati;

/**
 * salariati module definition class
 */
class Salariati extends \app\components\Module\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Salariati\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
