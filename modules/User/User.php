<?php

namespace app\modules\User;

use app\components\Module\Module;

/**
 * user module definition class
 */
class User extends Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\User\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
