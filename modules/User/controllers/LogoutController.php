<?php

namespace app\modules\User\controllers;

use Yii;
use app\controllers\BackendController;

/**
 * Logout controller for the `user` module
 */
class LogoutController extends BackendController
{
    
    public function actionIndex()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }
    
}