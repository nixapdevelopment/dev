<?php

namespace app\modules\User\controllers;

use Yii;
use app\modules\User\models\User;
use app\controllers\FrontController;
use app\modules\User\models\LoginForm;

/**
 * Login controller for the `user` module
 */
class LoginController extends FrontController
{
    
    public function actionIndex()
    {
        $model = new LoginForm();
        
        if ($model->load(Yii::$app->request->post()) && $model->login())
        {
            switch (Yii::$app->user->identity->Type)
            {
                case User::TypeAccountant:
                    return $this->redirect(['/dashboard']);
                    
                case User::TypeSiteAdmin:
                    return $this->redirect(['/site']);
                    
                case User::TypeSuperAdmin:
                    return $this->redirect(['/backoffice']);
            }
        }
        
        return $this->render('login-form', [
            'model' => $model,
        ]);
    }
    
}