<?php

namespace app\modules\User\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use app\modules\User\models\UserCompany;

/**
 * This is the model class for table "User".
 *
 * @property integer $ID
 * @property string $UserName
 * @property string $Password
 * @property string $FirstName
 * @property string $LastName
 * @property string $AuthKey
 * @property string $Type
 * @property string $Status
 *
 * @property UserCompany[] $userCompanies
 */
class User extends ActiveRecord implements IdentityInterface
{
    
    const TypeAccountant = 'Accountant';
    const TypeSiteAdmin = 'SiteAdmin';
    const TypeSuperAdmin = 'SuperAdmin';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'User';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['UserName', 'Password', 'Status', 'Type'], 'required'],
            [['UserName', 'FirstName', 'LastName'], 'string', 'max' => 255],
            [['AuthKey', 'Type', 'Password', 'Status'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'UserName' => Yii::t('app', 'User Name'),
            'Password' => Yii::t('app', 'Parola'),
            'FirstName' => Yii::t('app', 'Nume'),
            'LastName' => Yii::t('app', 'Prenume'),
            'Status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCompanies()
    {
        return $this->hasMany(UserCompany::className(), ['UserID' => 'ID']);
    }
    
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return User::find()->where(['ID' => $id])->one();
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return User::findOne(['AuthKey' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return User::findOne(['UserName' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->ID;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->AuthKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->AuthKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->Password === md5($password);
    }
    
    public function getTypesList($addEmpty = false)
    {
        $res = $addEmpty ? ['' => $addEmpty] : [];
        
        return $res + [
            self::TypeSiteAdmin => Yii::t('app', 'Administrator site-ul'),
            self::TypeAccountant => Yii::t('app', 'Contabil'),
            self::TypeSuperAdmin => Yii::t('app', 'Super Admin'),
        ];
    }
    
}