<?php

namespace app\modules\Tour\models;

use Yii;
use app\modules\Location\models\Location;
use app\modules\Tour\models\TourPackageDate;
use app\modules\Tour\models\TourPackageDeparture;

/**
 * This is the model class for table "TourPackage".
 *
 * @property integer $ID
 * @property integer $OperatorID
 * @property integer $OperatorPackageID
 * @property integer $HotelID
 * @property integer $LocationID
 * @property string $TransportType
 * @property integer $Duration
 * @property string $Currency
 * @property string $Name
 * @property string $Description
 * @property integer $Visible
 * @property integer $IsRecomended
 * @property string $Extra
 * @property string $Type
 *
 * @property Operator $operator
 * @property Location $location
 * @property TourPackageDate[] $dates
 * @property TourPackageDeparture[] $departures
 */
class TourPackage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'TourPackage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['OperatorID', 'OperatorPackageID', 'HotelID', 'LocationID', 'TransportType', 'Duration', 'Currency', 'Name', 'Description'], 'required'],
            [['OperatorID', 'OperatorPackageID', 'HotelID', 'LocationID', 'Duration', 'Visible', 'IsRecomended'], 'integer'],
            [['Description', 'Extra'], 'string'],
            [['Visible'], 'default', 'value' => 0],
            [['TransportType', 'Name'], 'string', 'max' => 255],
            [['Currency'], 'string', 'max' => 50],
            //[['OperatorID'], 'exist', 'skipOnError' => true, 'targetClass' => Operator::className(), 'targetAttribute' => ['OperatorID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'OperatorID' => Yii::t('app', 'Operator ID'),
            'OperatorPackageID' => Yii::t('app', 'Operator Package ID'),
            'HotelID' => Yii::t('app', 'Hotel ID'),
            'LocationID' => Yii::t('app', 'Location ID'),
            'TransportType' => Yii::t('app', 'Transport Type'),
            'Duration' => Yii::t('app', 'Duration'),
            'Currency' => Yii::t('app', 'Currency'),
            'Name' => Yii::t('app', 'Name'),
            'Description' => Yii::t('app', 'Description'),
            'IsRecomended' => Yii::t('app', 'Is Recomended'),
            'Extra' => Yii::t('app', 'Extra'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperator()
    {
        return $this->hasOne(Operator::className(), ['ID' => 'OperatorID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(Location::className(), ['ID' => 'LocationID']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDates()
    {
        return $this->hasMany(TourPackageDate::className(), ['TourPackageID' => 'ID']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartures()
    {
        return $this->hasMany(TourPackageDeparture::className(), ['TourPackageID' => 'ID']);
    }
    
}
