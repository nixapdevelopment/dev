<?php

namespace app\modules\Tour\models;

use Yii;

/**
 * This is the model class for table "TourDestinationDepartureDate".
 *
 * @property integer $ID
 * @property integer $TourDestinationDepartureID
 * @property string $Date
 *
 * @property TourDestinationDeparture $tourDestinationDeparture
 */
class TourDestinationDepartureDate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'TourDestinationDepartureDate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['TourDestinationDepartureID', 'Date'], 'required'],
            [['TourDestinationDepartureID'], 'integer'],
            [['Date'], 'safe'],
            [['TourDestinationDepartureID'], 'exist', 'skipOnError' => true, 'targetClass' => TourDestinationDeparture::className(), 'targetAttribute' => ['TourDestinationDepartureID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'TourDestinationDepartureID' => Yii::t('app', 'Tour Destination Departure ID'),
            'Date' => Yii::t('app', 'Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTourDestinationDeparture()
    {
        return $this->hasOne(TourDestinationDeparture::className(), ['ID' => 'TourDestinationDepartureID']);
    }
}
