<?php

namespace app\modules\Tour\models;

use Yii;
use app\modules\Operator\Operator\models\Operator;

/**
 * This is the model class for table "TourHotel".
 *
 * @property integer $ID
 * @property integer $OperatorID
 * @property integer $OperatorHotelID
 * @property string $Name
 * @property integer $LocationID
 * @property string $Latitude
 * @property string $Longitude
 * @property string $RoomTypes
 * @property string $Stars
 * @property string $Type
 *
 * @property Operator $operator
 * @property TourPackage[] $tourPackages
 */
class TourHotel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'TourHotel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Latitude', 'Longitude'], 'default', 'value' => 0],
            [['OperatorID', 'OperatorHotelID', 'Name', 'LocationID', 'Latitude', 'Longitude', 'RoomTypes', 'Type'], 'required'],
            [['OperatorID', 'OperatorHotelID', 'LocationID'], 'integer'],
            [['Latitude', 'Longitude', 'Stars'], 'number'],
            [['Name', 'Code'], 'string', 'max' => 255],
            [['RoomTypes'], 'string', 'max' => 1000],
            [['Description'], 'string'],
            [['Type'], 'string', 'max' => 50],
            [['OperatorID'], 'exist', 'skipOnError' => true, 'targetClass' => Operator::className(), 'targetAttribute' => ['OperatorID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'OperatorID' => Yii::t('app', 'Operator ID'),
            'OperatorHotelID' => Yii::t('app', 'Operator Hotel ID'),
            'Name' => Yii::t('app', 'Name'),
            'LocationID' => Yii::t('app', 'Location ID'),
            'Latitude' => Yii::t('app', 'Latitude'),
            'Longitude' => Yii::t('app', 'Longitude'),
            'RoomTypes' => Yii::t('app', 'Room Types'),
            'Type' => Yii::t('app', 'Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperator()
    {
        return $this->hasOne(Operator::className(), ['ID' => 'OperatorID']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTourPackages()
    {
        return $this->hasMany(TourPackage::className(), ['HotelID' => 'ID']);
    }
    
}
