<?php

namespace app\modules\Tour\models;

use Yii;
use yii\base\Model;

class TourBookingForm extends Model
{
    
    public $FirstName;
    public $LastName;
    public $Gender;
    public $BirthDate;
    public $Type;
    public $RoomID;

    public function rules()
    {
        return [
            [['FirstName', 'LastName', 'Gender', 'BirthDate', 'Type', 'RoomID'], 'required'],
            [['FirstName', 'LastName'], 'string', 'max' => 255],
            [['Gender', 'BirthDate', 'Type'], 'string', 'max' => 50],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'FirstName' => Yii::t('app', 'Nume'),
            'LastName' => Yii::t('app', 'Prenume'),
            'Gender' => Yii::t('app', 'Gen'),
            'BirthDate' => Yii::t('app', 'Data nasterii'),
            'Type' => Yii::t('app', 'Type'),
        ];
    }
    
}