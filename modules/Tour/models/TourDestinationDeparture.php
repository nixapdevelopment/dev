<?php

namespace app\modules\Tour\models;

use Yii;

/**
 * This is the model class for table "TourDestinationDeparture".
 *
 * @property integer $ID
 * @property integer $OperatorID
 * @property integer $TourDestinationID
 * @property integer $CountryID
 * @property integer $CityID
 *
 * @property TourDestination $tourDestination
 * @property TourDestinationDepartureDate[] $tourDestinationDepartureDates
 */
class TourDestinationDeparture extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'TourDestinationDeparture';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['OperatorID', 'TourDestinationID', 'CountryID', 'CityID'], 'required'],
            [['OperatorID', 'TourDestinationID', 'CountryID', 'CityID'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'OperatorID' => Yii::t('app', 'Operator ID'),
            'TourDestinationID' => Yii::t('app', 'Tour Destination ID'),
            'CountryID' => Yii::t('app', 'Country ID'),
            'CityID' => Yii::t('app', 'City ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTourDestination()
    {
        return $this->hasOne(TourDestination::className(), ['ID' => 'TourDestinationID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTourDestinationDepartureDates()
    {
        return $this->hasMany(TourDestinationDepartureDate::className(), ['TourDestinationDepartureID' => 'ID']);
    }
    
    public function getCity()
    {
        return $this->hasOne(\app\modules\Location\models\Location::className(), ['ID' => 'CityID']);
    }
    
}
