<?php

namespace app\modules\Tour\models;

use Yii;
use app\modules\Location\models\Location;
use app\modules\Operator\Operator\models\Operator;

/**
 * This is the model class for table "TourDestination".
 *
 * @property integer $ID
 * @property integer $OperatorID
 * @property integer $ParentID
 * @property integer $LocationID
 * @property string $LocationType
 * @property string $Zone
 *
 * @property TopDestination $parent
 * @property Location $location
 * @property Operator $operator
 * @property TourDestinationDeparture[] $tourDestinationDepartures
 */
class TourDestination extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'TourDestination';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Zone'], 'default', 'value' => null],
            [['OperatorID', 'ParentID', 'LocationID', 'LocationType'], 'required'],
            [['OperatorID', 'ParentID', 'LocationID'], 'integer'],
            [['LocationType'], 'string', 'max' => 50],
            [['Zone'], 'string', 'max' => 255],
            [['ParentID'], 'exist', 'skipOnError' => true, 'targetClass' => TopDestination::className(), 'targetAttribute' => ['ParentID' => 'ID']],
            [['LocationID'], 'exist', 'skipOnError' => true, 'targetClass' => Location::className(), 'targetAttribute' => ['LocationID' => 'ID']],
            [['OperatorID'], 'exist', 'skipOnError' => true, 'targetClass' => Operator::className(), 'targetAttribute' => ['OperatorID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'OperatorID' => Yii::t('app', 'Operator ID'),
            'ParentID' => Yii::t('app', 'Parent ID'),
            'LocationID' => Yii::t('app', 'Location ID'),
            'LocationType' => Yii::t('app', 'Location Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(TopDestination::className(), ['ID' => 'ParentID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(Location::className(), ['ID' => 'LocationID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperator()
    {
        return $this->hasOne(Operator::className(), ['ID' => 'OperatorID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTourDestinationDepartures()
    {
        return $this->hasMany(TourDestinationDeparture::className(), ['TourDestinationID' => 'ID']);
    }
}
