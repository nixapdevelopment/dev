<?php

namespace app\modules\Tour\models;

use Yii;

/**
 * This is the model class for table "TourPackageDate".
 *
 * @property integer $ID
 * @property integer $TourPackageID
 * @property string $Date
 * @property integer $Sold
 * @property integer $Visible
 * 
 * @property TourPackage $tourPackage
 */
class TourPackageDate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'TourPackageDate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['TourPackageID', 'Date', 'Sold', 'Visible'], 'required'],
            [['TourPackageID', 'Sold', 'Visible'], 'integer'],
            [['Date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'TourPackageID' => Yii::t('app', 'Tour Package ID'),
            'Date' => Yii::t('app', 'Date'),
            'Sold' => Yii::t('app', 'Sold'),
            'Visible' => Yii::t('app', 'Visible'),
        ];
    }
    
    public function getTourPackage()
    {
        return $this->hasOne(TourPackage::className(), ['ID' => 'TourPackageID']);
    }
    
}
