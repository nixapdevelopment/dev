<?php

namespace app\modules\Tour\models;

use Yii;
use app\modules\Location\models\Location;

/**
 * This is the model class for table "TourPackageDeparture".
 *
 * @property integer $ID
 * @property integer $CountryID
 * @property integer $LocationID
 *
 * @property Location $location
 * @property TourPackage $tourPackage
 */
class TourPackageDeparture extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'TourPackageDeparture';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CountryID', 'LocationID'], 'required'],
            [['CountryID', 'LocationID'], 'integer'],
            [['CountryID'], 'exist', 'skipOnError' => true, 'targetClass' => Location::className(), 'targetAttribute' => ['CountryID' => 'ID']],
            [['LocationID'], 'exist', 'skipOnError' => true, 'targetClass' => Location::className(), 'targetAttribute' => ['LocationID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'LocationID' => Yii::t('app', 'Location ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(Location::className(), ['ID' => 'LocationID']);
    }
    
    public function getTourPackage()
    {
        return $this->hasOne(TourPackage::className(), ['ID' => 'TourPackageID']);
    }
    
}
