<?php

namespace app\modules\Tour;

use Yii;

/**
 * tour module definition class
 */
class Tour extends \app\components\Module\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Tour\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
        $this->layoutPath = Yii::getAlias('@app/views/themes/front/layouts');
    }
}
