<?php

namespace app\modules\Tour\controllers;

use yii\web\Controller;
use app\modules\Tour\parsers\ChristianTour\StaticDataParser;

class TourParserController extends Controller
{
    
    public function init()
    {
        parent::init();
    }

    public function actionParse()
    {
        //(new \app\modules\Tour\parsers\ChristianTour\TourSearchProvider())->search();
        
//        set_time_limit(0);
//        ignore_user_abort(true);
//        
//        $hbProvider = new \app\modules\Parser\providers\HotelbedsProvider();
//        $hbProvider->getLocations();
//        
//        $sunProvider = new \app\modules\Parser\providers\SunhotelsProvider();
//        $sunProvider->getLocations();
//        
//        $whlProvider = new \app\modules\Parser\providers\WhlProvider();
//        $whlProvider->getLocations();
//        
        $ctParser = new StaticDataParser();
        $ctParser->parse();
        
//        $paralelaParser = new \app\modules\Tour\parsers\Paralela\StaticDataParser();
//        $paralelaParser->parse();
    }
    
}