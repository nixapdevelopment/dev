<?php

namespace app\modules\Tour\controllers;

/* @var $tour app\modules\Tour\entities\Tour */

use Yii;
use yii\web\Controller;
use app\modules\Booking\models\SearchInfo;
use app\modules\Tour\models\TourBookingForm;
use app\modules\Operator\Operator\models\Operator;

class OrderController extends Controller
{
    
    public function init()
    {
        parent::init();
        
        $this->layout = 'frontend';
    }

    public function actionIndex($hash, $nr, $rateKeyData)
    {
        $rateKeyData = unserialize(urldecode($rateKeyData));
        $cacheFile = Yii::getAlias('@app/modules/Tour/cache/' . $hash . '.txt');
        $results = unserialize(file_get_contents($cacheFile));
        $tour = $results[$rateKeyData['Key']];
        $offer = $tour->Offers[$nr];
        
        $operator = Operator::findOne($tour->OperatorID);
        
        $searchID = Yii::$app->session->get('SearchID');
        $searchModel = SearchInfo::findOne($searchID);
        
        $data = unserialize($searchModel->Data);
        
        // get cancelation policies
        $paxModels = [];
        foreach ($data['Adults'] as $roomID => $d)
        {
            for ($i = 1; $i <= $d; $i++)
            {
                $paxModels[] = new TourBookingForm([
                    'RoomID' => $roomID,
                    'Type' => 'Adult',
                ]);
            }
        }
        foreach ($data['Childs'] as $roomID => $d)
        {
            for ($i = 1; $i <= $d; $i++)
            {
                $paxModels[] = new TourBookingForm([
                    'RoomID' => $roomID,
                    'Type' => 'Child',
                ]);
            }
        }
        
        return $this->render('index', [
            'tour' => $tour,
            'offer' => $offer,
            'data' => $data,
            'paxModels' => $paxModels,
        ]);
    }
    
}