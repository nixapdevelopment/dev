<?php

namespace app\modules\Tour\parsers\Paralela;

use yii\httpclient\Client;
use app\modules\Location\models\Location;
use app\modules\Tour\models\TourDestination;
use app\modules\Tour\models\TourDestinationDeparture;
use app\modules\Tour\models\TourDestinationDepartureDate;
use app\modules\Operator\Operator\models\OperatorLocation;
use app\modules\Tour\parsers\ChristianTour\TranslatorHelper;


class StaticDataParser
{
    
    public $operatorID = 8;

    public $endPoint = 'http://rezervari.paralela45.ro/server_xml/server.php';
    
    public $email = 'XML_DIALECT';
    
    public $password = 'fcv34t453reg';
    
    
    public function parse()
    {
        set_time_limit(0);
        ignore_user_abort(true);
        
        $this->parsePackages();
    }
    
    public function parsePackages()
    {
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('post')
            ->setHeaders([
                'Content-Type' => 'text/xml;charset=UTF-8',
            ])
            ->setUrl($this->endPoint)
            ->setContent('<?xml version="1.0" encoding="UTF-8"?> 
                            <Request RequestType="getPackageNVRoutesRequest">
                                <AuditInfo>
                                    <RequestId>001</RequestId>
                                    <RequestUser>' . $this->email . '</RequestUser>
                                    <RequestPass>' . $this->password . '</RequestPass>
                                    <RequestTime>' . date('Y-m-d\TH:i:s') . '</RequestTime>
                                    <RequestLang>EN</RequestLang>
                                </AuditInfo>
                                <RequestDetails>
                                    <getPackageNVRoutesRequest>
                                        <Transport>search</Transport>
                                    </getPackageNVRoutesRequest>
                                </RequestDetails>
                            </Request>
            ')
            ->send();
        
        if ($response->isOk)
        {
            echo '<pre>';
            print_r($response->data['ResponseDetails']['getPackageNVRoutesResponse']['Country']);
            exit;
            
            TourDestination::deleteAll(['OperatorID' => $this->operatorID]);
            
            foreach ($response->data['ResponseDetails']['getPackageNVRoutesResponse']['Country'] as $result)
            {
                $country = OperatorLocation::find()->with('location')->where(['OperatorLocationID' => $result['CountryCode'], 'OperatorID' => 7])->limit(1)->one();
                
                if (empty($country->location->ID)) continue;
                
                $tourDestination = new TourDestination();
                $tourDestination->OperatorID = $this->operatorID;
                $tourDestination->ParentID = 0;
                $tourDestination->LocationID = $country->location->ID;
                $tourDestination->LocationType = Location::TypeCountry;
                $tourDestination->save(false);
                
                $subDestinations = reset($result['Destinations']['Destination']);
                if (!is_array($subDestinations))
                {
                    $subDestinations = [
                        0 => $result['Destinations']['Destination'],
                    ];
                }
                else
                {
                    $subDestinations = $result['Destinations']['Destination'];
                }
                
                foreach ($subDestinations as $destination)
                {
                    if (empty($destination['ZoneName'])) continue;
                    
                    $zoneName = trim($destination['ZoneName']);
                    
                    $city = OperatorLocation::find()->with('location')->where(['OperatorLocationID' => $destination['CityCode'], 'OperatorID' => 7])->limit(1)->one();
                    
                    if (empty($city->location->ID)) exit('No city');
                    
                    $tourSubSubDestination = new TourDestination();
                    $tourSubSubDestination->OperatorID = $this->operatorID;
                    $tourSubSubDestination->ParentID = $tourDestination->ID;
                    $tourSubSubDestination->LocationID = $city->location->ID;
                    $tourSubSubDestination->LocationType = Location::TypeCity;
                    $tourSubSubDestination->Zone = $zoneName;
                    $tourSubSubDestination->save(false);
                    
                    // parse Destination Departure
                    $departures = reset($destination['Departures']['Departure']);
                    
                    if (!is_array($departures))
                    {
                        $departures = [
                            0 => $destination['Departures']['Departure'],
                        ];
                    }
                    else
                    {
                        $departures = $destination['Departures']['Departure'];
                    }
                    
                    foreach ($departures as $departure)
                    {
                        $departureCountry = OperatorLocation::find()->with('location')->where(['OperatorLocationID' => $departure['CountryCode'], 'OperatorID' => 7])->limit(1)->one();
                        
                        if (empty($departureCountry->location->ID)) continue;
                        
                        $departureCity = OperatorLocation::find()->with('location')->where(['OperatorLocationID' => $departure['CityCode'], 'OperatorID' => 7])->limit(1)->one();
                        
                        if (empty($departureCity->location->ID)) continue;
                        
                        $tourDestinationDeparture = new TourDestinationDeparture();
                        $tourDestinationDeparture->TourDestinationID = $tourSubSubDestination->ID;
                        $tourDestinationDeparture->CountryID = $departureCountry->location->ID;
                        $tourDestinationDeparture->CityID = $departureCity->location->ID;
                        $tourDestinationDeparture->save(false);
                        
                        // parse dates
                        if (!is_array($departure['Dates']['Date']))
                        {
                            $dates = [
                                0 => $departure['Dates']['Date'],
                            ];
                        }
                        else
                        {
                            $dates = $departure['Dates']['Date'];
                        }
                        
                        foreach ($dates as $date)
                        {
                            $date = date('Y-m-d', strtotime($date));
                            
                            $tourDestinationDepartureDate = new TourDestinationDepartureDate();
                            $tourDestinationDepartureDate->TourDestinationDepartureID = $tourDestinationDeparture->ID;
                            $tourDestinationDepartureDate->Date = $date;
                            $tourDestinationDepartureDate->save(false);
                        }
                    }
                }
            }
        }
    }
    
}