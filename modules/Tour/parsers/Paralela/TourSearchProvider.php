<?php 

namespace app\modules\Tour\parsers\Paralela;

use yii\httpclient\Client;
use app\modules\Location\models\Location;
use app\modules\Tour\entities\Tour;
use app\modules\Tour\entities\HotelOffer;
use app\modules\Tour\entities\OfferService;
use app\modules\Operator\Operator\models\OperatorLocation;


class TourSearchProvider
{
    
    public $operatorID = 8;

    public $endPoint = 'http://rezervari.paralela45.ro/server_xml/server.php';
    
    public $email = 'XML_DIALECT';
    
    public $password = 'fcv34t453reg';
    
    
    public function search($data)
    {
        $destinationCity = OperatorLocation::find()->where(['OperatorID' => 7, 'LocationID' => $data['DestinationCityID']])->limit(1)->one();
        $destinationCountry = OperatorLocation::find()->where(['OperatorID' => 7, 'LocationID' => $data['DestinationCountryID']])->limit(1)->one();
        
        if (is_null($destinationCity) || is_null($destinationCountry))
        {
            return [];
        }
        
        $departureCity = OperatorLocation::find()->with('location')->where(['OperatorID' => 7, 'LocationID' => $data['DepartureID']])->limit(1)->one();
        $departureCountry = OperatorLocation::find()->where(['OperatorID' => 7, 'LocationID' => $departureCity->location->CountryID])->limit(1)->one();
        
        if (is_null($departureCity) || is_null($departureCountry))
        {
            return [];
        }
        
        $checkIn = date('Y-m-d', strtotime($data['Date']));
        $checkOut = date('Y-m-d', strtotime('+7 days', strtotime($data['Date'])));
        
        $rooms = '';
        for ($i = 1; $i <= $data['Rooms']; $i++)
        {
            $adults = (int)$data['Adults'][$i];
            $childs = isset($data['Childs'][$i]) ? (int)$data['Childs'][$i] : 0;
            $childsAge = isset($data['ChildAge'][$i]) ? $data['ChildAge'][$i] : [];
            
            $rooms .= '<Room Code="' . $this->defineRoomCode($adults, $childs, $childsAge) . '" NoAdults="' . $adults . '" ' . (!empty($childs) ? 'NoChildren="' . $childs . '"' : '') . '>';
            
            if (!empty($childs))
            {
                $rooms .= '<Children>';
                for ($j = 1; $j <= $childs; $j++)
                {
                    $rooms .= '<Age>' . $childsAge[$j] . '</Age>';
                }
                $rooms .= '</Children>';
            }
            
            $rooms .= '</Room>';
        }
        
        $xml = '<?xml version="1.0" encoding="UTF-8"?>
                <Request RequestType="getPackageNVPriceRequest">
                    <AuditInfo>
                        <RequestId>' . time() . '</RequestId>
                        <RequestUser>' . $this->email . '</RequestUser>
                        <RequestPass>' . $this->password . '</RequestPass>
                        <RequestTime>' . date('Y-m-d\TH:i:s') . '</RequestTime>
                        <RequestLang>EN</RequestLang>
                    </AuditInfo>
                    <RequestDetails>
                        <getPackageNVPriceRequest>
                            <CountryCode>' . $destinationCountry->OperatorLocationID . '</CountryCode>
                            <CityCode>' . $destinationCity->OperatorLocationID . '</CityCode>
                            <DepCountryCode>' . $departureCountry->OperatorLocationID . '</DepCountryCode>
                            <DepCityCode>' . $departureCity->OperatorLocationID . '</DepCityCode>
                            <Transport>plane</Transport>
                            <CurrencyCode>EUR</CurrencyCode>
                            <PeriodOfStay>
                                <CheckIn>' . $checkIn . '</CheckIn>
                                <CheckOut>' . $checkOut . '</CheckOut>
                            </PeriodOfStay>
                            <Days>1</Days>
                            <Rooms>
                                ' . $rooms . '
                            </Rooms>
                        </getPackageNVPriceRequest>
                    </RequestDetails>
                </Request>';
        
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('post')
            ->setHeaders([
                'Content-Type' => 'text/xml;charset=UTF-8',
            ])
            ->setUrl($this->endPoint)
            ->setContent($xml)
            ->send();
        
        if ($response->isOk)
        {
            if (empty($response->data['ResponseDetails']['getPackageNVPriceResponse']['Hotel']))
            {
                return [];
            }
            
            $results = [];
            foreach ($response->data['ResponseDetails']['getPackageNVPriceResponse']['Hotel'] as $tourData)
            {
                $hotel = $tourData['Product'];
                
                $tour = new Tour();
                $tour->OperatorID = $this->operatorID;
                $tour->CountryCode = $hotel['CountryCode'];
                $tour->CityName = $hotel['CityName'];
                $tour->CityCode = $hotel['CityCode'];
                $tour->HotelClass = $hotel['Class'];
                $tour->HotelCode = $hotel['ProductCode'];
                $tour->HotelName = $hotel['ProductName'];
                $tour->HotelStars = (int)$hotel['ProductCategory'];
                $tour->Latitude = $hotel['Latitude'];
                $tour->Longitude = $hotel['Longitude'];
                $tour->MainImage = $hotel['FirstImage'];
                $tour->TourOpCode = $hotel['TourOpCode'];
                $tour->Offers = [];
                
                if (isset($tourData['Offers']['Offer'][0]))
                {
                    $offers = $tourData['Offers']['Offer'];
                }
                else
                {
                    $offers = [0 => $tourData['Offers']['Offer']];
                }
                
                foreach ($offers as $offer)
                {
                    $hotelOffer = new HotelOffer();
                    $hotelOffer->Availability = $offer['Availability'];
                    $hotelOffer->BoardName = $offer['Meals']['Meal'];
                    $hotelOffer->CheckIn = date('Y-m-d', strtotime($offer['PeriodOfStay']['CheckIn']));
                    $hotelOffer->CheckOut = date('Y-m-d', strtotime($offer['PeriodOfStay']['CheckOut']));
                    $hotelOffer->OldPrice = round($offer['PriceNoRedd'], 2);
                    $hotelOffer->Price = round($offer['Gross'], 2);
                    $hotelOffer->RateKeyData = [
                        'PackageId' => $offer['PackageId'],
                        'PackageVariantId' => $offer['PackageVariantId'],
                        'Key' => $this->operatorID . '-' . $tour->HotelCode,
                    ];
                    $hotelOffer->RoomName = $offer['BookingRoomTypes']['Room'];
                    $hotelOffer->Services = [];
                    
                    if (isset($offer['PriceDetails']['Services']['Service'][0]))
                    {
                        $servies = $offer['PriceDetails']['Services']['Service'];
                    }
                    else
                    {
                        $servies = [0 => $offer['PriceDetails']['Services']['Service']];
                    }
                    
                    foreach ($servies as $service)
                    {
                        $offerService = new OfferService();
                        $offerService->Availability = $service['Availability'];
                        $offerService->Name = ucfirst($service['Name']);
                        $offerService->Price = round($service['Gross'], 2);
                        $offerService->Type = $service['Type'];
                        
                        $hotelOffer->Services[] = $offerService;
                    }
                    
                    $tour->Offers[] = $hotelOffer;
                }
                
                if (!empty($tour->HotelCode))
                {
                    $results[$this->operatorID . '-' . $tour->HotelCode] = $tour;
                }
            }
            
            return $results;
        }
        else
        {
            return [];
        }
        
        
//        $departureLocation = OperatorLocation::find()->with('location.lang')->where(['OperatorID' => $this->operatorID, 'LocationID' => $data['SearchFormModel']['CityFrom']])->limit(1)->one();
//        $destination = OperatorLocation::findOne(['OperatorID' => $this->operatorID, 'LocationID' => $data['SearchFormModel']['ZoneTo']]);
//        
//        $departure = TourDeparture::find()->where(['LocationID' => $departureLocation->LocationID])->all();
        
//        echo '<pre>';
//        print_r($departure);
//        exit;
        
//        $params = http_build_query([
//            'email' => $this->email,
//            'password' => $this->password,
//            'transport_id' => 2,
//            'departure_id' => 176,
//            'destination_id' => 237,
//            'departure_date' => '2018-04-26',
//            'duration' => 7,
//            'type' => empty($data['SearchFormModel']['Hotel']) ? 'public' : 'private',
//            'hotel_id' => empty($data['SearchFormModel']['Hotel']) ? '' : $data['SearchFormModel']['Hotel'],
//        ]);
//        
//        $requestUrl = $this->endPoint . 'packages/search';
//        
//        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_URL, $requestUrl);
//        curl_setopt($ch, CURLOPT_HEADER, 0);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//        curl_setopt($ch, CURLOPT_POST, 1);
//        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
//        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
//        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
//
//        $response = curl_exec($ch);
//
//        curl_close($ch);
//
//        $result = json_decode($response, true);
//        
//        if (!empty($result['search_id']))
//        {
//            $params = http_build_query([
//                'email' => $this->email,
//                'password' => $this->password,
//                'search_id' => $result['search_id'],
//                'limit' => 10,
//                'offset' => 0,
//                'filters' => [
//                    'price_interval' => [
//                        'min_value' => 0,
//                        'max_value' => 10000,
//                    ]
//                ],
//            ]);
//
//            $requestUrl = $this->endPoint . 'packages/results';
//
//            $ch = curl_init();
//            curl_setopt($ch, CURLOPT_URL, $requestUrl);
//            curl_setopt($ch, CURLOPT_HEADER, 0);
//            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//            curl_setopt($ch, CURLOPT_POST, 1);
//            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
//            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
//            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
//
//            $response = curl_exec($ch);
//
//            curl_close($ch);
//
//            $results = json_decode($response, true);
//            
//            echo '<pre>';
//            print_r($results);
//            exit;
//        }
    }
    
    public function defineRoomCode($adults, $childs = 0, $childsAge = [])
    {
        return RoomsConverter::getRoom($adults, $childs, $childsAge);
    }
    
    public function getInfo($tour)
    {
        $xml = '<?xml version="1.0" encoding="UTF-8"?>
                <Request RequestType="getProductInfoRequest">
                    <AuditInfo>
                        <RequestId>' . time() . '</RequestId>
                        <RequestUser>' . $this->email . '</RequestUser>
                        <RequestPass>' . $this->password . '</RequestPass>
                        <RequestTime>' . date('c') . '</RequestTime>
                        <RequestLang>EN</RequestLang>
                    </AuditInfo>
                    <RequestDetails>
                        <getProductInfoRequest>
                            <ProductType>hotel</ProductType>
                            <CountryCode>' . $tour->CountryCode . '</CountryCode>
                            <CityCode>' . $tour->CityCode . '</CityCode>
                            <TourOpCode>' . $tour->TourOpCode . '</TourOpCode>
                            <ProductCode>' . $tour->HotelCode . '</ProductCode>
                        </getProductInfoRequest>
                    </RequestDetails>
                </Request>';
        
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('post')
            ->setHeaders([
                'Content-Type' => 'text/xml;charset=UTF-8',
            ])
            ->setUrl($this->endPoint)
            ->setContent($xml)
            ->send();
        
        if ($response->isOk)
        {
            $info = $response->data['ResponseDetails']['getProductInfoResponse']['Product'];
            
            return [
                'Description' => html_entity_decode($info['DescriptionDet']),
                'Pictures' => $info['Pictures']['Picture'],
                'Facilities' => $info['Facilities']['Facility'],
            ];
        }
    }
    
    public function doBooking($tour)
    {
        $xml = '<?xml version="1.0" encoding="UTF-8"?>
                <AuditInfo>
                    <RequestId>' . time() . '</RequestId>
                    <RequestUser>' . $this->email . '</RequestUser>
                    <RequestPass>' . $this->password . '</RequestPass>
                    <RequestTime>' . date('c') . '</RequestTime>
                    <RequestLang>EN</RequestLang>
                </AuditInfo>
                <Request RequestType="getProductInfoRequest">
                    <RequestDetails>
                        <getProductInfoRequest>
                            <ProductType>hotel</ProductType>
                            <CountryCode>' . $tour->CountryCode . '</CountryCode>
                            <CityCode>' . $tour->CityCode . '</CityCode>
                            <TourOpCode>' . $tour->TourOpCode . '</TourOpCode>
                            <ProductCode>' . $tour->HotelCode . '</ProductCode>
                        </getProductInfoRequest>
                    </RequestDetails>
                    <RequestDetails>
                        <getHotelServiceTypesRequest>
                            <CountryCode>TR</CountryCode>
                            <CityCode>TRALN</CityCode>
                            <TourOpCode>EU</TourOpCode>
                            <ProductCode>TR0012</ProductCode>
                            <VariantId>1|1068741_669_1</VariantId>
                            <Language>RO</Language>
                            <PeriodOfStay>
                            <CheckIn>2012-09-21</CheckIn>
                            <CheckOut>2012-09-28</CheckOut>
                            </PeriodOfStay>
                        </getHotelServiceTypesRequest>
                    </RequestDetails> 
                </Request>';
        
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('post')
            ->setHeaders([
                'Content-Type' => 'text/xml;charset=UTF-8',
            ])
            ->setUrl($this->endPoint)
            ->setContent($xml)
            ->send();
        
        if ($response->isOk)
        {
            
        }
    }
    
}