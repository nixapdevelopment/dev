<?php

namespace app\modules\Tour\parsers\ChristianTour;


class TranslatorHelper
{
    
    public static function convertTransportType($type)
    {
        $type = trim(strval($type));
        switch ($type) {
            case '1':
                return 'Bus';
            case '2':
                return 'Avia';
            case '3':
                return 'Individual';
            default:
                throw new \yii\base\InvalidValueException("Undefined transport type `$type`");
        }
    }
    
}
