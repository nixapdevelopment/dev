<?php 

namespace app\modules\Tour\parsers\ChristianTour;

use app\modules\Operator\Operator\models\OperatorLocation;
use app\modules\Tour\models\TourDeparture;


class TourSearchProvider
{
    
    public $operatorID = 6;

    public $endPoint = 'http://api.new.christiantour.ro/api/v1/';
    
    public $email = 'office@dialecttour.ro';
    
    public $password = 'AuVdKoPdQqHb2kbu';
    
    
    public function search($data)
    {
        echo '<pre>';
        print_r($data);
        exit;
        
        $departureLocation = OperatorLocation::find()->with('location.lang')->where(['OperatorID' => $this->operatorID, 'LocationID' => $data['SearchFormModel']['CityFrom']])->limit(1)->one();
        $destination = OperatorLocation::findOne(['OperatorID' => $this->operatorID, 'LocationID' => $data['SearchFormModel']['ZoneTo']]);
        
        $departure = TourDeparture::find()->where(['LocationID' => $departureLocation->LocationID])->all();
        
//        echo '<pre>';
//        print_r($departure);
//        exit;
        
        $params = http_build_query([
            'email' => $this->email,
            'password' => $this->password,
            'transport_id' => 2,
            'departure_id' => 176,
            'destination_id' => 237,
            'departure_date' => '2018-04-26',
            'duration' => 7,
            'type' => empty($data['SearchFormModel']['Hotel']) ? 'public' : 'private',
            'hotel_id' => empty($data['SearchFormModel']['Hotel']) ? '' : $data['SearchFormModel']['Hotel'],
        ]);
        
        $requestUrl = $this->endPoint . 'packages/search';
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $requestUrl);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        $response = curl_exec($ch);

        curl_close($ch);

        $result = json_decode($response, true);
        
        if (!empty($result['search_id']))
        {
            $params = http_build_query([
                'email' => $this->email,
                'password' => $this->password,
                'search_id' => $result['search_id'],
                'limit' => 10,
                'offset' => 0,
                'filters' => [
                    'price_interval' => [
                        'min_value' => 0,
                        'max_value' => 10000,
                    ]
                ],
            ]);

            $requestUrl = $this->endPoint . 'packages/results';

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $requestUrl);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

            $response = curl_exec($ch);

            curl_close($ch);

            $results = json_decode($response, true);
            
            echo '<pre>';
            print_r($results);
            exit;
        }
    }
    
}