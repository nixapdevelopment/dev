<?php

namespace app\modules\Tour\parsers\ChristianTour;

use Yii;
use yii\helpers\ArrayHelper;
use app\modules\Location\models\Location;
use app\modules\Location\models\LocationLang;
use app\modules\Operator\Operator\models\OperatorLocation;
use app\modules\Tour\models\TourHotel;
use app\modules\Tour\models\TourPackage;
use app\modules\Tour\models\TourPackageDate;
use app\modules\Tour\models\TourPackageDeparture;
use app\modules\Tour\parsers\ChristianTour\TranslatorHelper;


class StaticDataParser
{
    
    public $operatorID = 6;

    public $endPoint = 'http://api.christiantour.ro/api/v1/static/';
    
    public $email = 'office@dialecttour.ro';
    
    public $password = 'AuVdKoPdQqHb2kbu';
    
    
    public function parse()
    {
        set_time_limit(0);
        ignore_user_abort(true);
        
        $this->parseLocations();
        $this->parseHotels();
        $this->parsePackages();
    }
    
    private function parseLocations()
    {
        $cacheFile = realpath(__DIR__ . DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR . 'locations.txt');
        
        if ((filemtime($cacheFile)) < time())
        {
            $params = http_build_query([
                'email' => $this->email,
                'password' => $this->password,
            ]);

            $requestUrl = $this->endPoint . 'destinations';

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $requestUrl);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

            $response = curl_exec($ch);

            file_put_contents($cacheFile, $response);

            curl_close($ch);
        }
        else
        {
            $response = file_get_contents($cacheFile);
        }

        $results = json_decode($response, true);
        
        foreach ($results as $key => $val)
        {
            if ($val['what_api'] == 1)
            {
                unset($results[$key]);
            }
        }
        
        OperatorLocation::deleteAll(['OperatorID' => $this->operatorID]);
        
        $i = 0;
        $locationStack = [];
        foreach ($results as $result)
        {
            $locationType = trim($result['type']);
            if (empty($locationType))
            {
                continue;
            }
            
            switch ($locationType)
            {
                case 'country':
                    $type = Location::TypeCountry;
                    break;
                case 'city':
                    $type = Location::TypeCity;
                    break;
                case 'area':
                    $type = Location::TypeArea;
                    break;
            }
            
            if (!empty($type))
            {
                $code = (int)$result['id'];
                $name = trim($result['content']['title']);
                
                $location = Location::find()->joinWith('lang')->where(['Name' => $name, 'Type' => $type])->one();

                if (empty($location->ID))
                {
                    $location = new Location();
                    $location->Type = $type;
                    $location->Latitude = 0;
                    $location->Longitude = 0;
                    $location->Code = NULL;
                    
                    if ($type == Location::TypeCity)
                    {
                        $location->CountryID = $locationStack[Location::TypeCountry][$result['country_id']];
                        $location->ParentID = $locationStack[Location::TypeCountry][$result['country_id']];
                    }
                    
                    if ($type == Location::TypeArea)
                    {
                        if (empty($locationStack[Location::TypeCity][$result['parent_id']]))
                        {
                            continue;
                        }
                        
                        $location->CountryID = $locationStack[Location::TypeCity][$result['parent_id']];
                        $location->ParentID = $locationStack[Location::TypeCity][$result['parent_id']];
                    }
                    
                    $location->save(false);

                    $locationLang = new LocationLang();
                    $locationLang->LangID = 'ro';
                    $locationLang->LocationID = $location->ID;
                    $locationLang->Name = $name;
                    $locationLang->save(false);
                }
                
                $locationStack[$type][$result['id']] = $location->ID;
                
                $operatorLocation = new OperatorLocation();
                $operatorLocation->OperatorID = $this->operatorID;
                $operatorLocation->LocationID = $location->ID;
                $operatorLocation->OperatorLocationID = $code;
                $operatorLocation->save(false);
            }
            
            $i++;
        }
        
        echo '<p>Locations: ' . count($results) . '</p>';
    }
    
    private function parseHotels()
    {
        $cacheFile = realpath(__DIR__ . DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR . 'hotels.txt');
        
        if ((filemtime($cacheFile)) < time())
        {
            $params = http_build_query([
                'email' => $this->email,
                'password' => $this->password,
            ]);

            $requestUrl = $this->endPoint . 'hotels';

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $requestUrl);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

            $response = curl_exec($ch);

            curl_close($ch);
            
            file_put_contents($cacheFile, $response);
        }
        else
        {
            $response = file_get_contents($cacheFile);
        }

        $results = json_decode($response, true);

        if ($results)
        {
            TourHotel::deleteAll(['OperatorID' => $this->operatorID]);
            
            foreach ($results as $result)
            {
                $operatorLocation = OperatorLocation::find()->where(['OperatorID' => $this->operatorID, 'OperatorLocationID' => $result['destination_id']])->limit(1)->one();
                
                if (!empty($operatorLocation->ID))
                {
                    $tourHotel = new TourHotel();
                    $tourHotel->OperatorID = $this->operatorID;
                    $tourHotel->OperatorHotelID = $result['id'];
                    $tourHotel->LocationID = $operatorLocation->LocationID;
                    $tourHotel->Name = $result['title'];
                    $tourHotel->Latitude = (float)$result['latitude'];
                    $tourHotel->Longitude = (float)$result['longitude'];
                    $tourHotel->Stars = (int)$result['class'];
                    $tourHotel->Description = !empty($result['content']['long_description']) ? $result['content']['long_description'] : '';
                    $tourHotel->Type = isset($result['types'][0]['title']) ? trim($result['types'][0]['title']) : 'Other';
                    
                    $roomsArr = [];
                    foreach ($result['rooms'] as $room)
                    {
                        $roomsArr[] = $room['room_content']['title'];
                    }
                    
                    $tourHotel->RoomTypes = implode('|', $roomsArr);
                    
                    $tourHotel->save(false);
                }
            }
        }
        
        echo '<p>Hotels: ' . count($results) . '</p>';
    }
    
    public function parsePackages()
    {
        $cacheFile = realpath(__DIR__ . DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR . 'packages.txt');
        
        if ((filemtime($cacheFile)) < time())
        {
            $params = http_build_query([
                'email' => $this->email,
                'password' => $this->password,
            ]);

            $requestUrl = $this->endPoint . 'packages';

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $requestUrl);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

            $response = curl_exec($ch);

            curl_close($ch);
            
            file_put_contents($cacheFile, $response);
        }
        else
        {
            $response = file_get_contents($cacheFile);
        }
        
        if ($response)
        {
            $results = json_decode($response, true);
            
            TourPackage::deleteAll(['OperatorID' => $this->operatorID]);

            foreach ($results as $result)
            {
                $destinationLocation = OperatorLocation::find()->where(['OperatorID' => $this->operatorID, 'OperatorLocationID' => (int)$result['destination_id']])->limit(1)->one();

                if (!empty($destinationLocation->ID))
                {
                    // save tour general data
                    $tourPackage = new TourPackage();   
                    $tourPackage->OperatorID = $this->operatorID;
                    $tourPackage->OperatorPackageID = $result['id'];
                    $tourPackage->HotelID = $result['hotel_id'];
                    $tourPackage->LocationID = $destinationLocation->LocationID;
                    $tourPackage->TransportType = TranslatorHelper::convertTransportType($result['transport_id']);
                    $tourPackage->Duration = (int)$result['duration'];
                    $tourPackage->Currency = strtoupper($result['currency']);
                    $tourPackage->Name = $result['title'];
                    $tourPackage->Description = empty($result['content']['description']) ? '' : $result['content']['description'];
                    $tourPackage->Visible = (int)$result['visible'];
                    $tourPackage->IsRecomended = intval($result['is_recommended']) == 1 ? 1 : 0;
                    $tourPackage->Type = 'Tour';

                    // extra data for tour
                    $extraArr = [];
                    foreach ($result['extra_content'] as $item)
                    {
                        $extraArr[] = [
                            'Title' => $item['title'],
                            'Content' => $item['content'],
                        ];
                    }
                    $tourPackage->Extra = serialize($extraArr);

                    $tourPackage->save(false);

                    // save tour departure dates
                    $insert = [];
                    foreach ($result['departure_dates'] as $date)
                    {
                        $insert[] = [
                            'ID' => null,
                            'TourPackageID' => $tourPackage->ID,
                            'Date' => date('Y-m-d', strtotime($date['date'])),
                            'Sold' => (int)$date['sold'],
                            'Visible' => (int)$date['visible'],
                        ];
                    }
                    
                    if (count($insert) > 0)
                    {
                        Yii::$app->db->createCommand()->batchInsert(
                            TourPackageDate::tableName(), 
                            (new TourPackageDate())->attributes(), 
                            $insert
                        )->execute();
                    }

                    // save tour departure points (locations)
                    $departurePoints = ArrayHelper::getColumn($result['departure_points'], function($item) {
                        return (int)$item['departure_point'];
                    });
                    $departureLocations = OperatorLocation::find()->joinWith('location')->where(['OperatorLocationID' => $departurePoints, 'OperatorID' => $this->operatorID])->all();

                    $insert = [];
                    foreach ($departureLocations as $departureLocation)
                    {
                        $insert[] = [
                            'ID' => null,
                            'TourPackageID' => $tourPackage->ID,
                            'CountryID' => $departureLocation->location->CountryID,
                            'LocationID' => $departureLocation->location->ID,
                        ];
                    }

                    if (count($insert) > 0)
                    {
                        Yii::$app->db->createCommand()->batchInsert(
                            TourPackageDeparture::tableName(), 
                            (new TourPackageDeparture())->attributes(), 
                            $insert
                        )->execute();
                    }
                }
            }

            echo '<p>Tours: ' . count($results) . '</p>';
        }
    }
    
}