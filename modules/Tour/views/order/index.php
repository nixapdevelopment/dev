<?php

    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use kartik\date\DatePicker;

?>

<div class="container">
    <h1>Booking</h1>
    <?php $form = ActiveForm::begin() ?>
        <?php foreach ($paxModels as $key => $paxModel) { ?>
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($paxModel, "[$key]FirstName")->textInput([
                    'placeholder' => $paxModel->getAttributeLabel('FirstName') . ' Adult ' . ($key + 1),
                ]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($paxModel, "[$key]LastName")->textInput([
                    'placeholder' => $paxModel->getAttributeLabel('LastName') . ' Adult ' . ($key + 1),
                ]) ?>
            </div>
            <div class="col-md-1">
                <?= $form->field($paxModel, "[$key]Gender")->dropDownList(['B' => 'B', 'F' => 'F']) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($paxModel, "[$key]BirthDate")->widget(DatePicker::className(), [
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'dd.mm.yyyy',
                    ],
                ]) ?>
            </div>
        </div>
        <?php } ?>
        <div class="row">
            <div class="col-md-2 col-md-offset-5">
                <?= Html::submitButton('Trimite', ['class' => 'reservations btn btn-block']) ?>
            </div>
        </div>
    <?php ActiveForm::end() ?>
</div>