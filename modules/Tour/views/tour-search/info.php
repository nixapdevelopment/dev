<?php

    /* @var $tour app\modules\Tour\entities\Tour */

    $offer = $tour->Offers[$nr];

?>

<div class="row text-center text-uppercase">
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <b>Perioada:</b>
            </div>
            <div class="panel-body">
                <?= date('d.m.Y', strtotime($offer->CheckIn)) ?> - <?= date('d.m.Y', strtotime($offer->CheckOut)) ?>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <b>Camere:</b>
            </div>
            <div class="panel-body">
                <?= $offer->RoomName ?>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <b>Masa inclusa in oferta:</b>
            </div>
            <div class="panel-body">
                <?= $offer->getBoards() ?>
            </div>
        </div>
    </div>
</div>
<br />
<div class="row">
    <div class="col-md-6">
        <div>
            <b>Servicii incluse:</b>
            <div>
                <?php foreach ($offer->Services as $service) { ?>
                <div>
                    &bull; <?= $service->Name ?>
                </div>
                <?php } ?>
            </div>
        </div>
        <h3>
            <b>Pret:</b> <?= $offer->Price ?> EUR
        </h3>
        <hr />
        <div>
            <?php foreach ($info['Pictures'] as $img) { ?>
            <a data-lightbox="tur-images" class="tur-images" href="<?= $img ?>">
                <img style="height: 70px; margin-bottom: 5px;" src="<?= $img ?>" />
            </a>
            <?php } ?>
        </div>
        <hr />
        <div>
            <a href="<?= \yii\helpers\Url::to(['/tour/order/index', 'hash' => $hash, 'nr' => $nr, 'rateKeyData' => urlencode(serialize($tour->Offers[$nr]->RateKeyData))]) ?>" class="reservations">
                Rezerva
            </a>
        </div>
    </div>
    <div class="col-md-6">
        <div style="line-height: 1.1;">
            <?= $info['Description'] ?>
        </div>
    </div>
</div>

<script>
    //$.post('<?= yii\helpers\Url::to(['/tour/tour-search/info-details']) ?>');
</script>