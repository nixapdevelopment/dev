<?php

    use yii\widgets\ActiveForm;
    use yii\helpers\Html;
    use kartik\depdrop\DepDrop;
    use yii\helpers\Url;
    use app\views\themes\front\assets\FrontAsset;
    
    $bundle = FrontAsset::register($this);

?>
<br />
<section class="filter different">
    <div class="container">
        <div class="filter-tab">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="">
                    <a href="#hoteluri" aria-controls="hoteluri" role="tab" data-toggle="tab">
                        <span class="fa fa-building"></span>
                        Hoteluri
                    </a>
                </li>
                <li role="presentation" class="active">
                    <a href="sejur-avion.html">
                        <span class="fa fa-plane"></span>
                        Sejur Avion
                    </a>
                </li>
                <li role="presentation">
                    <a href="sejur-autocar.html">
                        <span class="fa fa-bus"></span>
                        Sejur Autocar
                    </a>
                </li>
            </ul>
            <div class="content-filter">
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="hoteluri">
                        <?php $form = ActiveForm::begin([
                            'method' => 'get',
                            'options' => [
                                'id' => 'tour-search-form',
                                'class' => 'sejur-avion-filter',
                            ],
                        ]) ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="big-width-select">
                                                <label>
                                                    Plecare din
                                                </label>
                                                <?= Html::activeDropDownList($model, 'CityFrom', $departures, [
                                                    'class' => 'style-select',
                                                    'id' => 'CityFrom',
                                                ]) ?>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="big-width-select">
                                                <label>
                                                    Tara destinatie
                                                </label>
                                                <?= DepDrop::widget([
                                                    'model' => $model,
                                                    'attribute' => 'CountryTo',
                                                    'type' => DepDrop::TYPE_SELECT2,
                                                    'options' => [
                                                        'id' => 'CountryTo',
                                                    ],
                                                    'pluginOptions' => [
                                                        'url' => Url::to(['/tour/tour-search/get-countries']),
                                                        'depends' => ['CityFrom'],
                                                    ],
                                                ]) ?>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="big-width-select">
                                                <label>
                                                    Zona
                                                </label>
                                                <?= DepDrop::widget([
                                                    'model' => $model,
                                                    'attribute' => 'ZoneTo',
                                                    'type' => DepDrop::TYPE_SELECT2,
                                                    'options' => [
                                                        'id' => 'ZoneTo',
                                                    ],
                                                    'pluginOptions' => [
                                                        'url' => Url::to(['/tour/tour-search/get-zones']),
                                                        'depends' => ['CityFrom', 'CountryTo'],
                                                    ],
                                                ]) ?>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="big-width-select">
                                                <label>
                                                    Statiune:
                                                </label>
                                                <?= DepDrop::widget([
                                                    'model' => $model,
                                                    'attribute' => 'StationTo',
                                                    'type' => DepDrop::TYPE_SELECT2,
                                                    'options' => [
                                                        'id' => 'StationTo',
                                                    ],
                                                    'pluginOptions' => [
                                                        'url' => Url::to(['/tour/tour-search/get-stations']),
                                                        'depends' => ['ZoneTo'],
                                                    ],
                                                ]) ?>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="big-width-select">
                                                <label>
                                                    Hotel
                                                </label>
                                                <?= DepDrop::widget([
                                                    'model' => $model,
                                                    'attribute' => 'Hotel',
                                                    'type' => DepDrop::TYPE_SELECT2,
                                                    'options' => [
                                                        'id' => 'Hotel',
                                                    ],
                                                    'pluginOptions' => [
                                                        'url' => Url::to(['/tour/tour-search/get-hotels']),
                                                        'depends' => ['ZoneTo', 'StationTo'],
                                                    ],
                                                ]) ?>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <label>
                                                Data plecare
                                            </label>
                                            <?= DepDrop::widget([
                                                'model' => $model,
                                                'attribute' => 'Date',
                                                'type' => DepDrop::TYPE_SELECT2,
                                                'options' => [
                                                    'id' => 'Date',
                                                ],
                                                'pluginOptions' => [
                                                    'url' => Url::to(['/tour/tour-search/get-dates']),
                                                    'depends' => ['ZoneTo', 'StationTo'],
                                                ],
                                            ]) ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 mt10">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <label>
                                                                Durata
                                                            </label>
                                                            <?= DepDrop::widget([
                                                                'model' => $model,
                                                                'attribute' => 'Duration',
                                                                'type' => DepDrop::TYPE_SELECT2,
                                                                'options' => [
                                                                    'id' => 'Duration',
                                                                ],
                                                                'pluginOptions' => [
                                                                    'url' => Url::to(['/tour/tour-search/get-durations']),
                                                                    'depends' => ['ZoneTo'],
                                                                ],
                                                            ]) ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="big-width-select">
                                                        <label>
                                                            Ordoneaza dupa
                                                        </label>
                                                        <select class="style-select" name="">
                                                            <option value="Masa1">
                                                                De la mic la mare
                                                            </option>
                                                            <option value="Masa2">
                                                                De la mare la mic
                                                            </option>
                                                            <option value="Masa3">
                                                                Dupa disponibilitate
                                                            </option>
                                                            <option value="Masa4">
                                                                Numarul de stele
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="big-width-select">
                                                        <label>
                                                            Camere
                                                        </label>
                                                        <?= Html::activeDropDownList($model, 'Rooms', $rooms, [
                                                            'class' => 'style-select',
                                                        ]) ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 text-left">
                                            <div class="room-controls" style="display: block">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <div class="form-group big-width-select">
                                                            <label class="control-label">Adulti</label>
                                                            <select class="form-control style-select" name="Adults[1]">
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group big-width-select">
                                                            <label class="control-label">Copii</label>
                                                            <select class="form-control style-select" name="Childs[1]">
                                                                <option value="0">0</option>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="child-age-control" style="display: none;">
                                                            <label class="label-control">
                                                                Virsta copii
                                                            </label>
                                                            <div class="form-group">
                                                                <select class="form-control style-select" name="ChildAge[1][1]">
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                    <option value="12">12</option>
                                                                    <option value="13">13</option>
                                                                    <option value="14">14</option>
                                                                    <option value="15">15</option>
                                                                    <option value="16">16</option>
                                                                    <option value="17">17</option>
                                                                    <option value="18">18</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="child-age-control" style="display: none;">
                                                            <div class="form-group">
                                                                <select class="form-control style-select" name="ChildAge[1][2]">
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                    <option value="12">12</option>
                                                                    <option value="13">13</option>
                                                                    <option value="14">14</option>
                                                                    <option value="15">15</option>
                                                                    <option value="16">16</option>
                                                                    <option value="17">17</option>
                                                                    <option value="18">18</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="child-age-control" style="display: none;">
                                                            <div class="form-group">
                                                                <select class="form-control style-select" name="ChildAge[1][3]" disabled="disabled">
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                    <option value="12">12</option>
                                                                    <option value="13">13</option>
                                                                    <option value="14">14</option>
                                                                    <option value="15">15</option>
                                                                    <option value="16">16</option>
                                                                    <option value="17">17</option>
                                                                    <option value="18">18</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="child-age-control" style="display: none;">
                                                            <div class="form-group">
                                                                <select class="form-control style-select" name="ChildAge[1][4]" disabled="disabled">
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                    <option value="12">12</option>
                                                                    <option value="13">13</option>
                                                                    <option value="14">14</option>
                                                                    <option value="15">15</option>
                                                                    <option value="16">16</option>
                                                                    <option value="17">17</option>
                                                                    <option value="18">18</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="room-controls no-label" style="display: none">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <select class="form-control style-select" name="Adults[2]">
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <select class="form-control style-select" name="Childs[2]">
                                                                <option value="0">0</option>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="child-age-control" style="display: none;">
                                                            <div class="form-group">
                                                                <select class="form-control style-select" name="ChildAge[2][1]">
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                    <option value="12">12</option>
                                                                    <option value="13">13</option>
                                                                    <option value="14">14</option>
                                                                    <option value="15">15</option>
                                                                    <option value="16">16</option>
                                                                    <option value="17">17</option>
                                                                    <option value="18">18</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="child-age-control" style="display: none;">
                                                            <div class="form-group">
                                                                <select class="form-control style-select" name="ChildAge[2][2]">
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                    <option value="12">12</option>
                                                                    <option value="13">13</option>
                                                                    <option value="14">14</option>
                                                                    <option value="15">15</option>
                                                                    <option value="16">16</option>
                                                                    <option value="17">17</option>
                                                                    <option value="18">18</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="child-age-control" style="display: none;">
                                                            <div class="form-group">
                                                                <select class="form-control style-select" name="ChildAge[2][3]" disabled="disabled">
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                    <option value="12">12</option>
                                                                    <option value="13">13</option>
                                                                    <option value="14">14</option>
                                                                    <option value="15">15</option>
                                                                    <option value="16">16</option>
                                                                    <option value="17">17</option>
                                                                    <option value="18">18</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="child-age-control" style="display: none;">
                                                            <div class="form-group">
                                                                <select class="form-control style-select" name="ChildAge[2][4]" disabled="disabled">
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                    <option value="12">12</option>
                                                                    <option value="13">13</option>
                                                                    <option value="14">14</option>
                                                                    <option value="15">15</option>
                                                                    <option value="16">16</option>
                                                                    <option value="17">17</option>
                                                                    <option value="18">18</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="room-controls no-label" style="display: none">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <select class="form-control style-select" name="Adults[3]">
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <select class="form-control style-select" name="Childs[3]">
                                                                <option value="0">0</option>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="child-age-control" style="display: none;">
                                                            <div class="form-group">
                                                                <select class="form-control style-select" name="ChildAge[3][1]">
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                    <option value="12">12</option>
                                                                    <option value="13">13</option>
                                                                    <option value="14">14</option>
                                                                    <option value="15">15</option>
                                                                    <option value="16">16</option>
                                                                    <option value="17">17</option>
                                                                    <option value="18">18</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="child-age-control" style="display: none;">
                                                            <div class="form-group">
                                                                <select class="form-control style-select" name="ChildAge[3][2]">
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                    <option value="12">12</option>
                                                                    <option value="13">13</option>
                                                                    <option value="14">14</option>
                                                                    <option value="15">15</option>
                                                                    <option value="16">16</option>
                                                                    <option value="17">17</option>
                                                                    <option value="18">18</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="child-age-control" style="display: none;">
                                                            <div class="form-group">
                                                                <select class="form-control style-select" name="ChildAge[3][3]" disabled="disabled">
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                    <option value="12">12</option>
                                                                    <option value="13">13</option>
                                                                    <option value="14">14</option>
                                                                    <option value="15">15</option>
                                                                    <option value="16">16</option>
                                                                    <option value="17">17</option>
                                                                    <option value="18">18</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="child-age-control" style="display: none;">
                                                            <div class="form-group">
                                                                <select class="form-control style-select" name="ChildAge[3][4]" disabled="disabled">
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                    <option value="12">12</option>
                                                                    <option value="13">13</option>
                                                                    <option value="14">14</option>
                                                                    <option value="15">15</option>
                                                                    <option value="16">16</option>
                                                                    <option value="17">17</option>
                                                                    <option value="18">18</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="room-controls no-label" style="display: none">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <select class="form-control style-select" name="Adults[4]">
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <select class="form-control style-select" name="Childs[4]">
                                                                <option value="0">0</option>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="child-age-control" style="display: none;">
                                                            <div class="form-group">
                                                                <select class="form-control style-select" name="ChildAge[4][1]">
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                    <option value="12">12</option>
                                                                    <option value="13">13</option>
                                                                    <option value="14">14</option>
                                                                    <option value="15">15</option>
                                                                    <option value="16">16</option>
                                                                    <option value="17">17</option>
                                                                    <option value="18">18</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="child-age-control" style="display: none;">
                                                            <div class="form-group">
                                                                <select class="form-control style-select" name="ChildAge[4][2]">
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                    <option value="12">12</option>
                                                                    <option value="13">13</option>
                                                                    <option value="14">14</option>
                                                                    <option value="15">15</option>
                                                                    <option value="16">16</option>
                                                                    <option value="17">17</option>
                                                                    <option value="18">18</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="child-age-control" style="display: none;">
                                                            <div class="form-group">
                                                                <select class="form-control style-select" name="ChildAge[4][3]" disabled="disabled">
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                    <option value="12">12</option>
                                                                    <option value="13">13</option>
                                                                    <option value="14">14</option>
                                                                    <option value="15">15</option>
                                                                    <option value="16">16</option>
                                                                    <option value="17">17</option>
                                                                    <option value="18">18</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="child-age-control" style="display: none;">
                                                            <div class="form-group">
                                                                <select class="form-control style-select" name="ChildAge[4][4]" disabled="disabled">
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                    <option value="12">12</option>
                                                                    <option value="13">13</option>
                                                                    <option value="14">14</option>
                                                                    <option value="15">15</option>
                                                                    <option value="16">16</option>
                                                                    <option value="17">17</option>
                                                                    <option value="18">18</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="mt15">
                                        <button class="form-submit" type="button">
                                            Cauta
                                        </button>
                                    </div>
                                </div>
                            </div>
                        <?php ActiveForm::end() ?>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="sejur-avion">

                    </div>
                    <div role="tabpanel" class="tab-pane" id="sejur-automobil">

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="result-sejur">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="sort-hotel-sidebar">
                    <div class="hotel-name">
                        <label class="name-filter">
                            Hotel name
                        </label>
                        <div class="search-hotel-filter">
                            <input type="text">
                            <button class="btn btn-default">
                                <span class="fa fa-search"></span>
                            </button>
                        </div>
                    </div>
                    <hr />
                    <div class="costume-input">
                        <div class="category-hotel">
                            <label class="name-filter">
                                Board
                            </label>
                            <input type="checkbox" id="board1" name="board1">
                            <label for="board1">
                                <span class="check"></span>
                                Incl. breakfast
                            </label>
                            <input type="checkbox" id="board2" name="board2">
                            <label for="board2">
                                <span class="check"></span>
                                Self catering (apartments)
                            </label>
                            <input type="checkbox" id="board3" name="board3">
                            <label for="board3">
                                <span class="check"></span>
                                Half board
                            </label>
                            <input type="checkbox" id="board4" name="board4">
                            <label for="board4">
                                <span class="check"></span>
                                Room only (hotels)
                            </label>
                            <input type="checkbox" id="board5" name="board5">
                            <label for="board5">
                                <span class="check"></span>
                                Incl. breakfast, lunch and dinner
                            </label>
                        </div>
                        <hr />
                        <div class="category-hotel">
                            <label class="name-filter">
                                Category
                            </label>
                            <input type="checkbox" id="five-disable-star" name="five-disable-star">
                            <!--label for="five-disable-star">
                                <span class="check"></span>
                                <img class="img-responsive" src="<?= $bundle->baseUrl ?>/images/five-disable-star.png" alt="">
                            </label-->
                            <input type="checkbox" id="one-star" name="one-star">
                            <label for="one-star">
                                <span class="check"></span>
                                <img class="img-responsive" src="<?= $bundle->baseUrl ?>/images/one-star.png" alt="">
                            </label>
                            <input type="checkbox" id="two-star" name="two-star">
                            <label for="two-star">
                                <span class="check"></span>
                                <img class="img-responsive" src="<?= $bundle->baseUrl ?>/images/two-star.png" alt="">
                            </label>
                            <input type="checkbox" id="three-star" name="three-star">
                            <label for="three-star">
                                <span class="check"></span>
                                <img class="img-responsive" src="<?= $bundle->baseUrl ?>/images/three-star.png" alt="">
                            </label>
                            <input type="checkbox" id="four-star" name="four-star">
                            <label for="four-star">
                                <span class="check"></span>
                                <img class="img-responsive" src="<?= $bundle->baseUrl ?>/images/four-star.png" alt="">
                            </label>
                            <input type="checkbox" id="five-star" name="five-star">
                            <label for="five-star">
                                <span class="check"></span>
                                <img class="img-responsive" src="<?= $bundle->baseUrl ?>/images/five-star.png" alt="">
                            </label>
                        </div>
                        <!--div class="customer-reviews">
                            <label class="name-filter">
                                Customer reviews
                            </label>
                            <input type="checkbox" id="one-review" name="one-review">
                            <label for="one-review">
                                <span class="check"></span>
                                <div class="icon-trip1 icon-trip"></div>
                            </label>
                            <input type="checkbox" id="two-review" name="two-review">
                            <label for="two-review">
                                <span class="check"></span>
                                <div class="icon-trip2 icon-trip"></div>
                            </label>
                            <input type="checkbox" id="three-review" name="three-review">
                            <label for="three-review">
                                <span class="check"></span>
                                <div class="icon-trip3 icon-trip"></div>
                            </label>
                            <input type="checkbox" id="four-review" name="four-review">
                            <label for="four-review">
                                <span class="check"></span>
                                <div class="icon-trip4 icon-trip"></div>
                            </label>
                            <input type="checkbox" id="five-review" name="five-review">
                            <label for="five-review">
                                <span class="check"></span>
                                <div class="icon-trip5 icon-trip"></div>
                            </label>
                        </div-->
                        <hr />
                        <div class="price">
                            <label class="name-filter">
                                Price
                            </label>
                            <b>10 €</b><input class="filter-price span2" type="text"  value="" data-slider-min="10" data-slider-max="1000" data-slider-step="5" data-slider-value="[250,450]"/> <b>€ 1000</b>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="result-search">
                    <div class="characters-panel">
                        <ul>
                            <li>
                                <span class="fa fa-info-circle"></span>
                                Informatie hotel
                            </li>
                            <li>
                                <span class="fa fa-ticket"></span>
                                Oferta speciala
                            </li>
                            <li>
                                <span class="fa fa-clock-o"></span>
                                Early booking
                            </li>
                            <li>
                                <span class="fa fa-fire"></span>
                                Free child
                            </li>
                            <li>
                                <span class="fa fa-check-circle"></span>
                                Disponibil
                            </li>
                            <li>
                                <span class="fa fa-check-circle disable"></span>
                                La cerere
                            </li>
                            <li>
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     viewBox="0 0 65.518 65.518" style="enable-background:new 0 0 65.518 65.518;" xml:space="preserve">
                                <g>
                                <path d="M32.759,0C14.696,0,0,14.695,0,32.759s14.695,32.759,32.759,32.759s32.759-14.695,32.759-32.759S50.822,0,32.759,0z
                                      M6,32.759C6,18.004,18.004,6,32.759,6c6.648,0,12.734,2.443,17.419,6.472L12.472,50.178C8.443,45.493,6,39.407,6,32.759z
                                      M32.759,59.518c-5.948,0-11.447-1.953-15.895-5.248l37.405-37.405c3.295,4.448,5.248,9.947,5.248,15.895
                                      C59.518,47.514,47.514,59.518,32.759,59.518z"/>
                                </g>
                                </svg>
                                Nedisponibil
                            </li>
                        </ul>
                    </div>
                    <div class="info-about-result">
                        <span class="text">
                            Rezultate cautare:
                        </span>
                        <span class="result-text">
                            Alanya, 23-06-2018, 7 nopti: 47 pachete, 13 hoteluri
                        </span>
                    </div>
                    <div class="result-group-item sejur">
                        <div class="result-wrap">
                            <div class="info-result head">
                                <div class="row no-padding">
                                    <div class="col-md-4 text-left">
                                        Nume hotel
                                    </div>
                                    <div class="col-md-2 text-center">
                                        Categorie camere
                                    </div>
                                    <div class="col-md-2 text-center">
                                        Board basis
                                    </div>
                                    <div class="col-md-1 text-center">
                                        Pret
                                    </div>
                                    <div class="col-md-1 text-center">
                                        Avion
                                    </div>
                                    <div class="col-md-1 text-center">
                                        Hotel
                                    </div>
                                    <div class="col-md-1">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="result-content">
                            <div class="sejur-item">
                                <div class="row no-padding">
                                    <div class="col-md-4 text-left">
                                        <div class="row no-padding">
                                            <div class="col-md-9">
                                                <div class="hotel-name">
                                                    The Inn Resort & Spa- Charter
                                                    Avion Antalya 2018 (Iasi)
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="pull-right">
                                                    <span class="fa fa-info-circle"></span>
                                                </div>
                                                <div class="pull-right star">
                                                    <img class="img-responsive" src="<?= $bundle->baseUrl ?>/images/five-star.png" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 text-center">
                                        <div class="room-type">
                                            Promo Room
                                            <a href="#" class="open-all-room">
                                                <span class="fa fa-plus-square toggle-room"></span>
                                                <span class="fa fa-minus-square hidden-icon toggle-room"></span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-2 text-center">
                                        All inclusive
                                    </div>
                                    <div class="col-md-1 text-center">
                                        <div>
                                            <span class="fa fa-clock-o"></span>
                                        </div>
                                        <div class="old-price">
                                            EUR 2014
                                        </div>
                                        <div class="new-price">
                                            EUR 2108
                                        </div>
                                    </div>
                                    <div class="col-md-1 text-center">
                                        <span class="fa fa-check-circle"></span>
                                    </div>
                                    <div class="col-md-1 text-center">
                                        <span class="fa fa-check-circle"></span>
                                    </div>
                                    <div class="col-md-1">
                                        <a href="" class="reservations">
                                            Rezerva
                                        </a>
                                    </div>
                                </div>
                                <div class="toggling-room">
                                    <div class="row no-padding">
                                        <div class="col-md-4 text-left">
                                            <div class="row no-padding">
                                                <div class="col-md-9">
                                                    <div class="hotel-name">
                                                        The Inn Resort & Spa- Charter
                                                        Avion Antalya 2018 (Iasi)
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="pull-right">
                                                        <span class="fa fa-info-circle"></span>
                                                    </div>
                                                    <div class="pull-right star">
                                                        <img class="img-responsive" src="<?= $bundle->baseUrl ?>/images/five-star.png" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 text-center">
                                            <div class="room-type">
                                                Promo Room
                                            </div>
                                        </div>
                                        <div class="col-md-2 text-center">
                                            All inclusive
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <div>
                                                <span class="fa fa-clock-o"></span>
                                            </div>
                                            <div class="old-price">
                                                EUR 2014
                                            </div>
                                            <div class="new-price">
                                                EUR 2108
                                            </div>
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <span class="fa fa-check-circle"></span>
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <span class="fa fa-check-circle"></span>
                                        </div>
                                        <div class="col-md-1">
                                            <a href="" class="reservations">
                                                Rezerva
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="toggling-room">
                                    <div class="row no-padding">
                                        <div class="col-md-4 text-left">
                                            <div class="row no-padding">
                                                <div class="col-md-9">
                                                    <div class="hotel-name">
                                                        The Inn Resort & Spa- Charter
                                                        Avion Antalya 2018 (Iasi)
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="pull-right">
                                                        <span class="fa fa-info-circle"></span>
                                                    </div>
                                                    <div class="pull-right star">
                                                        <img class="img-responsive" src="<?= $bundle->baseUrl ?>/images/five-star.png" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 text-center">
                                            <div class="room-type">
                                                Promo Room
                                            </div>
                                        </div>
                                        <div class="col-md-2 text-center">
                                            All inclusive
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <div>
                                                <span class="fa fa-clock-o"></span>
                                            </div>
                                            <div class="old-price">
                                                EUR 2014
                                            </div>
                                            <div class="new-price">
                                                EUR 2108
                                            </div>
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <span class="fa fa-check-circle"></span>
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <span class="fa fa-check-circle"></span>
                                        </div>
                                        <div class="col-md-1">
                                            <a href="" class="reservations">
                                                Rezerva
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="toggling-room">
                                    <div class="row no-padding">
                                        <div class="col-md-4 text-left">
                                            <div class="row no-padding">
                                                <div class="col-md-9">
                                                    <div class="hotel-name">
                                                        The Inn Resort & Spa- Charter
                                                        Avion Antalya 2018 (Iasi)
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="pull-right">
                                                        <span class="fa fa-info-circle"></span>
                                                    </div>
                                                    <div class="pull-right star">
                                                        <img class="img-responsive" src="<?= $bundle->baseUrl ?>/images/five-star.png" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 text-center">
                                            <div class="room-type">
                                                Promo Room
                                            </div>
                                        </div>
                                        <div class="col-md-2 text-center">
                                            All inclusive
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <div>
                                                <span class="fa fa-clock-o"></span>
                                            </div>
                                            <div class="old-price">
                                                EUR 2014
                                            </div>
                                            <div class="new-price">
                                                EUR 2108
                                            </div>
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <span class="fa fa-check-circle"></span>
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <span class="fa fa-check-circle"></span>
                                        </div>
                                        <div class="col-md-1">
                                            <a href="" class="reservations">
                                                Rezerva
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="toggling-room">
                                    <div class="row no-padding">
                                        <div class="col-md-4 text-left">
                                            <div class="row no-padding">
                                                <div class="col-md-9">
                                                    <div class="hotel-name">
                                                        The Inn Resort & Spa- Charter
                                                        Avion Antalya 2018 (Iasi)
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="pull-right">
                                                        <span class="fa fa-info-circle"></span>
                                                    </div>
                                                    <div class="pull-right star">
                                                        <img class="img-responsive" src="<?= $bundle->baseUrl ?>/images/five-star.png" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 text-center">
                                            <div class="room-type">
                                                Promo Room
                                            </div>
                                        </div>
                                        <div class="col-md-2 text-center">
                                            All inclusive
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <div>
                                                <span class="fa fa-clock-o"></span>
                                            </div>
                                            <div class="old-price">
                                                EUR 2014
                                            </div>
                                            <div class="new-price">
                                                EUR 2108
                                            </div>
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <span class="fa fa-check-circle"></span>
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <span class="fa fa-check-circle"></span>
                                        </div>
                                        <div class="col-md-1">
                                            <a href="" class="reservations">
                                                Rezerva
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="toggling-room">
                                    <div class="row no-padding">
                                        <div class="col-md-4 text-left">
                                            <div class="row no-padding">
                                                <div class="col-md-9">
                                                    <div class="hotel-name">
                                                        The Inn Resort & Spa- Charter
                                                        Avion Antalya 2018 (Iasi)
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="pull-right">
                                                        <span class="fa fa-info-circle"></span>
                                                    </div>
                                                    <div class="pull-right star">
                                                        <img class="img-responsive" src="<?= $bundle->baseUrl ?>/images/five-star.png" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 text-center">
                                            <div class="room-type">
                                                Promo Room
                                            </div>
                                        </div>
                                        <div class="col-md-2 text-center">
                                            All inclusive
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <div>
                                                <span class="fa fa-clock-o"></span>
                                            </div>
                                            <div class="old-price">
                                                EUR 2014
                                            </div>
                                            <div class="new-price">
                                                EUR 2108
                                            </div>
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <span class="fa fa-check-circle"></span>
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <span class="fa fa-check-circle"></span>
                                        </div>
                                        <div class="col-md-1">
                                            <a href="" class="reservations">
                                                Rezerva
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sejur-item">
                                <div class="row no-padding">
                                    <div class="col-md-4 text-left">
                                        <div class="row no-padding">
                                            <div class="col-md-9">
                                                <div class="hotel-name">
                                                    The Inn Resort & Spa- Charter
                                                    Avion Antalya 2018 (Iasi)
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="pull-right">
                                                    <span class="fa fa-info-circle"></span>
                                                </div>
                                                <div class="pull-right star">
                                                    <img class="img-responsive" src="<?= $bundle->baseUrl ?>/images/five-star.png" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 text-center">
                                        <div class="room-type">
                                            Promo Room
                                        </div>
                                    </div>
                                    <div class="col-md-2 text-center">
                                        All inclusive
                                    </div>
                                    <div class="col-md-1 text-center">
                                        <div>
                                            <span class="fa fa-clock-o"></span>
                                        </div>
                                        <div class="old-price">
                                            EUR 2014
                                        </div>
                                        <div class="new-price">
                                            EUR 2108
                                        </div>
                                    </div>
                                    <div class="col-md-1 text-center">
                                        <span class="fa fa-check-circle"></span>
                                    </div>
                                    <div class="col-md-1 text-center">
                                        <span class="fa fa-check-circle"></span>
                                    </div>
                                    <div class="col-md-1">
                                        <a href="" class="reservations">
                                            Rezerva
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="sejur-item">
                                <div class="row no-padding">
                                    <div class="col-md-4 text-left">
                                        <div class="row no-padding">
                                            <div class="col-md-9">
                                                <div class="hotel-name">
                                                    The Inn Resort & Spa- Charter
                                                    Avion Antalya 2018 (Iasi)
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="pull-right">
                                                    <span class="fa fa-info-circle"></span>
                                                </div>
                                                <div class="pull-right star">
                                                    <img class="img-responsive" src="<?= $bundle->baseUrl ?>/images/five-star.png" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 text-center">
                                        <div class="room-type">
                                            Promo Room
                                        </div>
                                    </div>
                                    <div class="col-md-2 text-center">
                                        All inclusive
                                    </div>
                                    <div class="col-md-1 text-center">
                                        <div>
                                            <span class="fa fa-clock-o"></span>
                                        </div>
                                        <div class="old-price">
                                            EUR 2014
                                        </div>
                                        <div class="new-price">
                                            EUR 2108
                                        </div>
                                    </div>
                                    <div class="col-md-1 text-center">
                                        <span class="fa fa-check-circle"></span>
                                    </div>
                                    <div class="col-md-1 text-center">
                                        <span class="fa fa-check-circle"></span>
                                    </div>
                                    <div class="col-md-1">
                                        <a href="" class="reservations">
                                            Rezerva
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="sejur-item">
                                <div class="row no-padding">
                                    <div class="col-md-4 text-left">
                                        <div class="row no-padding">
                                            <div class="col-md-9">
                                                <div class="hotel-name">
                                                    The Inn Resort & Spa- Charter
                                                    Avion Antalya 2018 (Iasi)
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="pull-right">
                                                    <span class="fa fa-info-circle"></span>
                                                </div>
                                                <div class="pull-right star">
                                                    <img class="img-responsive" src="<?= $bundle->baseUrl ?>/images/five-star.png" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 text-center">
                                        <div class="room-type">
                                            Promo Room
                                            <a href="#" class="open-all-room">
                                                <span class="fa fa-plus-square toggle-room"></span>
                                                <span class="fa fa-minus-square hidden-icon toggle-room"></span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-2 text-center">
                                        All inclusive
                                    </div>
                                    <div class="col-md-1 text-center">
                                        <div>
                                            <span class="fa fa-clock-o"></span>
                                        </div>
                                        <div class="old-price">
                                            EUR 2014
                                        </div>
                                        <div class="new-price">
                                            EUR 2108
                                        </div>
                                    </div>
                                    <div class="col-md-1 text-center">
                                        <span class="fa fa-check-circle"></span>
                                    </div>
                                    <div class="col-md-1 text-center">
                                        <span class="fa fa-check-circle"></span>
                                    </div>
                                    <div class="col-md-1">
                                        <a href="" class="reservations">
                                            Rezerva
                                        </a>
                                    </div>
                                </div>
                                <div class="toggling-room">
                                    <div class="row no-padding">
                                        <div class="col-md-4 text-left">
                                            <div class="row no-padding">
                                                <div class="col-md-9">
                                                    <div class="hotel-name">
                                                        The Inn Resort & Spa- Charter
                                                        Avion Antalya 2018 (Iasi)
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="pull-right">
                                                        <span class="fa fa-info-circle"></span>
                                                    </div>
                                                    <div class="pull-right star">
                                                        <img class="img-responsive" src="<?= $bundle->baseUrl ?>/images/five-star.png" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 text-center">
                                            <div class="room-type">
                                                Promo Room
                                            </div>
                                        </div>
                                        <div class="col-md-2 text-center">
                                            All inclusive
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <div>
                                                <span class="fa fa-clock-o"></span>
                                            </div>
                                            <div class="old-price">
                                                EUR 2014
                                            </div>
                                            <div class="new-price">
                                                EUR 2108
                                            </div>
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <span class="fa fa-check-circle"></span>
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <span class="fa fa-check-circle"></span>
                                        </div>
                                        <div class="col-md-1">
                                            <a href="" class="reservations">
                                                Rezerva
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sejur-item">
                                <div class="row no-padding">
                                    <div class="col-md-4 text-left">
                                        <div class="row no-padding">
                                            <div class="col-md-9">
                                                <div class="hotel-name">
                                                    The Inn Resort & Spa- Charter
                                                    Avion Antalya 2018 (Iasi)
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="pull-right">
                                                    <span class="fa fa-info-circle"></span>
                                                </div>
                                                <div class="pull-right star">
                                                    <img class="img-responsive" src="<?= $bundle->baseUrl ?>/images/five-star.png" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 text-center">
                                        <div class="room-type">
                                            Promo Room
                                            <a href="#" class="open-all-room">
                                                <span class="fa fa-plus-square toggle-room"></span>
                                                <span class="fa fa-minus-square hidden-icon toggle-room"></span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-2 text-center">
                                        All inclusive
                                    </div>
                                    <div class="col-md-1 text-center">
                                        <div>
                                            <span class="fa fa-clock-o"></span>
                                        </div>
                                        <div class="old-price">
                                            EUR 2014
                                        </div>
                                        <div class="new-price">
                                            EUR 2108
                                        </div>
                                    </div>
                                    <div class="col-md-1 text-center">
                                        <span class="fa fa-check-circle"></span>
                                    </div>
                                    <div class="col-md-1 text-center">
                                        <span class="fa fa-check-circle"></span>
                                    </div>
                                    <div class="col-md-1">
                                        <a href="" class="reservations">
                                            Rezerva
                                        </a>
                                    </div>
                                </div>
                                <div class="toggling-room">
                                    <div class="row no-padding">
                                        <div class="col-md-4 text-left">
                                            <div class="row no-padding">
                                                <div class="col-md-9">
                                                    <div class="hotel-name">
                                                        The Inn Resort & Spa- Charter
                                                        Avion Antalya 2018 (Iasi)
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="pull-right">
                                                        <span class="fa fa-info-circle"></span>
                                                    </div>
                                                    <div class="pull-right star">
                                                        <img class="img-responsive" src="<?= $bundle->baseUrl ?>/images/five-star.png" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 text-center">
                                            <div class="room-type">
                                                Promo Room
                                            </div>
                                        </div>
                                        <div class="col-md-2 text-center">
                                            All inclusive
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <div>
                                                <span class="fa fa-clock-o"></span>
                                            </div>
                                            <div class="old-price">
                                                EUR 2014
                                            </div>
                                            <div class="new-price">
                                                EUR 2108
                                            </div>
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <span class="fa fa-check-circle"></span>
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <span class="fa fa-check-circle"></span>
                                        </div>
                                        <div class="col-md-1">
                                            <a href="" class="reservations">
                                                Rezerva
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sejur-item">
                                <div class="row no-padding">
                                    <div class="col-md-4 text-left">
                                        <div class="row no-padding">
                                            <div class="col-md-9">
                                                <div class="hotel-name">
                                                    The Inn Resort & Spa- Charter
                                                    Avion Antalya 2018 (Iasi)
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="pull-right">
                                                    <span class="fa fa-info-circle"></span>
                                                </div>
                                                <div class="pull-right star">
                                                    <img class="img-responsive" src="<?= $bundle->baseUrl ?>/images/five-star.png" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 text-center">
                                        <div class="room-type">
                                            Promo Room
                                        </div>
                                    </div>
                                    <div class="col-md-2 text-center">
                                        All inclusive
                                    </div>
                                    <div class="col-md-1 text-center">
                                        <div>
                                            <span class="fa fa-clock-o"></span>
                                        </div>
                                        <div class="old-price">
                                            EUR 2014
                                        </div>
                                        <div class="new-price">
                                            EUR 2108
                                        </div>
                                    </div>
                                    <div class="col-md-1 text-center">
                                        <span class="fa fa-check-circle"></span>
                                    </div>
                                    <div class="col-md-1 text-center">
                                        <span class="fa fa-check-circle"></span>
                                    </div>
                                    <div class="col-md-1">
                                        <a href="" class="reservations">
                                            Rezerva
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="sejur-item">
                                <div class="row no-padding">
                                    <div class="col-md-4 text-left">
                                        <div class="row no-padding">
                                            <div class="col-md-9">
                                                <div class="hotel-name">
                                                    The Inn Resort & Spa- Charter
                                                    Avion Antalya 2018 (Iasi)
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="pull-right">
                                                    <span class="fa fa-info-circle"></span>
                                                </div>
                                                <div class="pull-right star">
                                                    <img class="img-responsive" src="<?= $bundle->baseUrl ?>/images/five-star.png" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 text-center">
                                        <div class="room-type">
                                            Promo Room
                                        </div>
                                    </div>
                                    <div class="col-md-2 text-center">
                                        All inclusive
                                    </div>
                                    <div class="col-md-1 text-center">
                                        <div>
                                            <span class="fa fa-clock-o"></span>
                                        </div>
                                        <div class="old-price">
                                            EUR 2014
                                        </div>
                                        <div class="new-price">
                                            EUR 2108
                                        </div>
                                    </div>
                                    <div class="col-md-1 text-center">
                                        <span class="fa fa-check-circle"></span>
                                    </div>
                                    <div class="col-md-1 text-center">
                                        <span class="fa fa-check-circle"></span>
                                    </div>
                                    <div class="col-md-1">
                                        <a href="" class="reservations">
                                            Rezerva
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="sejur-item">
                                <div class="row no-padding">
                                    <div class="col-md-4 text-left">
                                        <div class="row no-padding">
                                            <div class="col-md-9">
                                                <div class="hotel-name">
                                                    The Inn Resort & Spa- Charter
                                                    Avion Antalya 2018 (Iasi)
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="pull-right">
                                                    <span class="fa fa-info-circle"></span>
                                                </div>
                                                <div class="pull-right star">
                                                    <img class="img-responsive" src="<?= $bundle->baseUrl ?>/images/five-star.png" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 text-center">
                                        <div class="room-type">
                                            Promo Room
                                            <a href="#" class="open-all-room">
                                                <span class="fa fa-plus-square toggle-room"></span>
                                                <span class="fa fa-minus-square hidden-icon toggle-room"></span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-2 text-center">
                                        All inclusive
                                    </div>
                                    <div class="col-md-1 text-center">
                                        <div>
                                            <span class="fa fa-clock-o"></span>
                                        </div>
                                        <div class="old-price">
                                            EUR 2014
                                        </div>
                                        <div class="new-price">
                                            EUR 2108
                                        </div>
                                    </div>
                                    <div class="col-md-1 text-center">
                                        <span class="fa fa-check-circle"></span>
                                    </div>
                                    <div class="col-md-1 text-center">
                                        <span class="fa fa-check-circle"></span>
                                    </div>
                                    <div class="col-md-1">
                                        <a href="" class="reservations">
                                            Rezerva
                                        </a>
                                    </div>
                                </div>
                                <div class="toggling-room">
                                    <div class="row no-padding">
                                        <div class="col-md-4 text-left">
                                            <div class="row no-padding">
                                                <div class="col-md-9">
                                                    <div class="hotel-name">
                                                        The Inn Resort & Spa- Charter
                                                        Avion Antalya 2018 (Iasi)
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="pull-right">
                                                        <span class="fa fa-info-circle"></span>
                                                    </div>
                                                    <div class="pull-right star">
                                                        <img class="img-responsive" src="<?= $bundle->baseUrl ?>/images/five-star.png" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 text-center">
                                            <div class="room-type">
                                                Promo Room
                                            </div>
                                        </div>
                                        <div class="col-md-2 text-center">
                                            All inclusive
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <div>
                                                <span class="fa fa-clock-o"></span>
                                            </div>
                                            <div class="old-price">
                                                EUR 2014
                                            </div>
                                            <div class="new-price">
                                                EUR 2108
                                            </div>
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <span class="fa fa-check-circle"></span>
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <span class="fa fa-check-circle"></span>
                                        </div>
                                        <div class="col-md-1">
                                            <a href="" class="reservations">
                                                Rezerva
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php $this->registerJs("
    $('#tour-search-form .form-submit').click(function(e){
        e.preventDefault();
        
        $.get('" . Url::to(['/tour/tour-search/search']) . "', $('#tour-search-form').serialize(), function(html){
            
        });
    });
") ?>