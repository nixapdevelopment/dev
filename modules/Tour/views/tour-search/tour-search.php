<?php

    use yii\widgets\ActiveForm;
    use yii\helpers\Html;
    use kartik\depdrop\DepDrop;
    use yii\helpers\Url;
    use app\views\themes\front\assets\FrontAsset;
    use yii\bootstrap\Modal;
    use kartik\select2\Select2;
    
    $bundle = FrontAsset::register($this);

?>
<br />
<section class="filter different">
    <div class="container">
        <div class="filter-tab">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="">
                    <a href="#hoteluri" aria-controls="hoteluri" role="tab" data-toggle="tab">
                        <span class="fa fa-building"></span>
                        Hoteluri
                    </a>
                </li>
                <li role="presentation" class="active">
                    <a href="sejur-avion.html">
                        <span class="fa fa-plane"></span>
                        Sejur Avion
                    </a>
                </li>
                <li role="presentation">
                    <a href="sejur-autocar.html">
                        <span class="fa fa-bus"></span>
                        Sejur Autocar
                    </a>
                </li>
            </ul>
            <div class="content-filter">
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="hoteluri">
                        <?php $form = ActiveForm::begin([
                            'method' => 'get',
                            'options' => [
                                'id' => 'tour-search-form',
                                'class' => 'sejur-avion-filter',
                            ],
                        ]) ?>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="big-width-select">
                                        <label>
                                            <?= Yii::t('app', 'Oras plecare') ?>
                                        </label>
                                        <?= Select2::widget([
                                            'name' => 'DepartureCityID',
                                            'value' => '',
                                            'data' => $departures,
                                            'options' => ['id' => 'DepartureCityID']
                                        ]); ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="big-width-select">
                                        <label>
                                            <?= Yii::t('app', 'Tara destinatie') ?>
                                        </label>
                                        <?= DepDrop::widget([
                                            'name' => 'DestinationCountryID',
                                            'type' => DepDrop::TYPE_SELECT2,
                                            'options' => [
                                                'id' => 'DestinationCountryID',
                                            ],
                                            'pluginOptions' => [
                                                'url' => Url::toRoute(['/tour/tour-search/get-destination-countries', 'transport' => 'Avia']),
                                                'depends' => ['DepartureCityID'],
                                            ],
                                        ]) ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="big-width-select">
                                        <label>
                                            <?= Yii::t('app', 'Zona destinatie') ?>
                                        </label>
                                        <?= DepDrop::widget([
                                            'name' => 'DestinationCityID',
                                            'type' => DepDrop::TYPE_SELECT2,
                                            'options' => [
                                                'id' => 'DestinationCityID',
                                            ],
                                            'pluginOptions' => [
                                                'url' => Url::toRoute(['/tour/tour-search/get-destination-cities', 'transport' => 'Avia']),
                                                'depends' => ['DestinationCountryID'],
                                            ],
                                        ]) ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="big-width-select">
                                        <label>
                                            <?= Yii::t('app', 'Hotel') ?>
                                        </label>
                                        <?= DepDrop::widget([
                                            'name' => 'DestinationHotelID',
                                            'type' => DepDrop::TYPE_SELECT2,
                                            'options' => [
                                                'id' => 'DestinationHotelID',
                                            ],
                                            'pluginOptions' => [
                                                'url' => Url::toRoute(['/tour/tour-search/get-destination-hotels', 'transport' => 'Avia']),
                                                'depends' => ['DestinationCityID'],
                                            ],
                                        ]) ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="big-width-select">
                                        <label>
                                            <?= Yii::t('app', 'Data plecare') ?>
                                        </label>
                                        <?= DepDrop::widget([
                                            'name' => 'DepartureDate',
                                            'type' => DepDrop::TYPE_SELECT2,
                                            'options' => [
                                                'id' => 'DepartureDate',
                                            ],
                                            'pluginOptions' => [
                                                'url' => Url::toRoute(['/tour/tour-search/get-departure-dates', 'transport' => 'Avia']),
                                                'depends' => ['DepartureCityID', 'DestinationCityID'],
                                            ],
                                        ]) ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="big-width-select">
                                        <label>
                                            <?= Yii::t('app', 'Durata') ?>
                                        </label>
                                        <?= DepDrop::widget([
                                            'name' => 'Duration',
                                            'type' => DepDrop::TYPE_SELECT2,
                                            'options' => [
                                                'id' => 'Duration',
                                            ],
                                            'pluginOptions' => [
                                                'url' => Url::toRoute(['/tour/tour-search/get-tour-durations', 'transport' => 'Avia']),
                                                'depends' => ['DepartureCityID', 'DestinationCityID'],
                                            ],
                                        ]) ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt15">
                                <div class="col-md-4">
                                    <div class="big-width-select">
                                        <label>
                                            Camere
                                        </label>
                                        <?= Html::dropDownList('Rooms', null, $rooms, [
                                            'class' => 'style-select',
                                        ]) ?>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="room-controls" style="display: block">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="form-group big-width-select">
                                                    <label class="control-label">Adulti</label>
                                                    <select class="form-control style-select" name="Adults[1]">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group big-width-select">
                                                    <label class="control-label">Copii</label>
                                                    <select class="form-control style-select" name="Childs[1]">
                                                        <option value="0">0</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="child-age-control" style="display: none;">
                                                    <label class="label-control">
                                                        Virsta copii
                                                    </label>
                                                    <div class="form-group">
                                                        <select class="form-control style-select" name="ChildAge[1][1]">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                            <option value="13">13</option>
                                                            <option value="14">14</option>
                                                            <option value="15">15</option>
                                                            <option value="16">16</option>
                                                            <option value="17">17</option>
                                                            <option value="18">18</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="child-age-control" style="display: none;">
                                                    <div class="form-group">
                                                        <select class="form-control style-select" name="ChildAge[1][2]">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                            <option value="13">13</option>
                                                            <option value="14">14</option>
                                                            <option value="15">15</option>
                                                            <option value="16">16</option>
                                                            <option value="17">17</option>
                                                            <option value="18">18</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="child-age-control" style="display: none;">
                                                    <div class="form-group">
                                                        <select class="form-control style-select" name="ChildAge[1][3]" disabled="disabled">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                            <option value="13">13</option>
                                                            <option value="14">14</option>
                                                            <option value="15">15</option>
                                                            <option value="16">16</option>
                                                            <option value="17">17</option>
                                                            <option value="18">18</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="child-age-control" style="display: none;">
                                                    <div class="form-group">
                                                        <select class="form-control style-select" name="ChildAge[1][4]" disabled="disabled">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                            <option value="13">13</option>
                                                            <option value="14">14</option>
                                                            <option value="15">15</option>
                                                            <option value="16">16</option>
                                                            <option value="17">17</option>
                                                            <option value="18">18</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="room-controls no-label" style="display: none">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <select class="form-control style-select" name="Adults[2]">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <select class="form-control style-select" name="Childs[2]">
                                                        <option value="0">0</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="child-age-control" style="display: none;">
                                                    <div class="form-group">
                                                        <select class="form-control style-select" name="ChildAge[2][1]">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                            <option value="13">13</option>
                                                            <option value="14">14</option>
                                                            <option value="15">15</option>
                                                            <option value="16">16</option>
                                                            <option value="17">17</option>
                                                            <option value="18">18</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="child-age-control" style="display: none;">
                                                    <div class="form-group">
                                                        <select class="form-control style-select" name="ChildAge[2][2]">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                            <option value="13">13</option>
                                                            <option value="14">14</option>
                                                            <option value="15">15</option>
                                                            <option value="16">16</option>
                                                            <option value="17">17</option>
                                                            <option value="18">18</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="child-age-control" style="display: none;">
                                                    <div class="form-group">
                                                        <select class="form-control style-select" name="ChildAge[2][3]" disabled="disabled">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                            <option value="13">13</option>
                                                            <option value="14">14</option>
                                                            <option value="15">15</option>
                                                            <option value="16">16</option>
                                                            <option value="17">17</option>
                                                            <option value="18">18</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="child-age-control" style="display: none;">
                                                    <div class="form-group">
                                                        <select class="form-control style-select" name="ChildAge[2][4]" disabled="disabled">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                            <option value="13">13</option>
                                                            <option value="14">14</option>
                                                            <option value="15">15</option>
                                                            <option value="16">16</option>
                                                            <option value="17">17</option>
                                                            <option value="18">18</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="room-controls no-label" style="display: none">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <select class="form-control style-select" name="Adults[3]">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <select class="form-control style-select" name="Childs[3]">
                                                        <option value="0">0</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="child-age-control" style="display: none;">
                                                    <div class="form-group">
                                                        <select class="form-control style-select" name="ChildAge[3][1]">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                            <option value="13">13</option>
                                                            <option value="14">14</option>
                                                            <option value="15">15</option>
                                                            <option value="16">16</option>
                                                            <option value="17">17</option>
                                                            <option value="18">18</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="child-age-control" style="display: none;">
                                                    <div class="form-group">
                                                        <select class="form-control style-select" name="ChildAge[3][2]">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                            <option value="13">13</option>
                                                            <option value="14">14</option>
                                                            <option value="15">15</option>
                                                            <option value="16">16</option>
                                                            <option value="17">17</option>
                                                            <option value="18">18</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="child-age-control" style="display: none;">
                                                    <div class="form-group">
                                                        <select class="form-control style-select" name="ChildAge[3][3]" disabled="disabled">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                            <option value="13">13</option>
                                                            <option value="14">14</option>
                                                            <option value="15">15</option>
                                                            <option value="16">16</option>
                                                            <option value="17">17</option>
                                                            <option value="18">18</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="child-age-control" style="display: none;">
                                                    <div class="form-group">
                                                        <select class="form-control style-select" name="ChildAge[3][4]" disabled="disabled">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                            <option value="13">13</option>
                                                            <option value="14">14</option>
                                                            <option value="15">15</option>
                                                            <option value="16">16</option>
                                                            <option value="17">17</option>
                                                            <option value="18">18</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="room-controls no-label" style="display: none">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <select class="form-control style-select" name="Adults[4]">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <select class="form-control style-select" name="Childs[4]">
                                                        <option value="0">0</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="child-age-control" style="display: none;">
                                                    <div class="form-group">
                                                        <select class="form-control style-select" name="ChildAge[4][1]">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                            <option value="13">13</option>
                                                            <option value="14">14</option>
                                                            <option value="15">15</option>
                                                            <option value="16">16</option>
                                                            <option value="17">17</option>
                                                            <option value="18">18</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="child-age-control" style="display: none;">
                                                    <div class="form-group">
                                                        <select class="form-control style-select" name="ChildAge[4][2]">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                            <option value="13">13</option>
                                                            <option value="14">14</option>
                                                            <option value="15">15</option>
                                                            <option value="16">16</option>
                                                            <option value="17">17</option>
                                                            <option value="18">18</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="child-age-control" style="display: none;">
                                                    <div class="form-group">
                                                        <select class="form-control style-select" name="ChildAge[4][3]" disabled="disabled">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                            <option value="13">13</option>
                                                            <option value="14">14</option>
                                                            <option value="15">15</option>
                                                            <option value="16">16</option>
                                                            <option value="17">17</option>
                                                            <option value="18">18</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="child-age-control" style="display: none;">
                                                    <div class="form-group">
                                                        <select class="form-control style-select" name="ChildAge[4][4]" disabled="disabled">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                            <option value="13">13</option>
                                                            <option value="14">14</option>
                                                            <option value="15">15</option>
                                                            <option value="16">16</option>
                                                            <option value="17">17</option>
                                                            <option value="18">18</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mt15">
                                <button class="form-submit" type="button">
                                    Cauta
                                </button>
                            </div>
                        <?php ActiveForm::end() ?>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="sejur-avion">

                    </div>
                    <div role="tabpanel" class="tab-pane" id="sejur-automobil">

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="result-sejur">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <form id="filter-form" class="sort-hotel-sidebar">
                    <div class="hotel-name">
                        <label class="name-filter">
                            Hotel name
                        </label>
                        <div class="search-hotel-filter">
                            <input name="Name" type="text">
                            <button class="btn btn-default">
                                <span class="fa fa-search"></span>
                            </button>
                        </div>
                    </div>
                    <br />
                    <div class="costume-input">
                        <div class="category-hotel">
                            <label class="name-filter">
                                Category
                            </label>
                            <input type="checkbox" id="five-disable-star" name="five-disable-star">
                            <label for="five-disable-star">
                                <span class="check"></span>
                                <img class="img-responsive" src="<?= $bundle->baseUrl ?>/images/five-disable-star.png" alt="">
                            </label>
                            <input type="checkbox" id="one-star" name="one-star">
                            <label for="one-star">
                                <span class="check"></span>
                                <img class="img-responsive" src="<?= $bundle->baseUrl ?>/images/one-star.png" alt="">
                            </label>
                            <input type="checkbox" id="two-star" name="two-star">
                            <label for="two-star">
                                <span class="check"></span>
                                <img class="img-responsive" src="<?= $bundle->baseUrl ?>/images/two-star.png" alt="">
                            </label>
                            <input type="checkbox" id="three-star" name="three-star">
                            <label for="three-star">
                                <span class="check"></span>
                                <img class="img-responsive" src="<?= $bundle->baseUrl ?>/images/three-star.png" alt="">
                            </label>
                            <input type="checkbox" id="four-star" name="four-star">
                            <label for="four-star">
                                <span class="check"></span>
                                <img class="img-responsive" src="<?= $bundle->baseUrl ?>/images/four-star.png" alt="">
                            </label>
                            <input type="checkbox" id="five-star" name="five-star">
                            <label for="five-star">
                                <span class="check"></span>
                                <img class="img-responsive" src="<?= $bundle->baseUrl ?>/images/five-star.png" alt="">
                            </label>
                        </div>
                        <div class="price">
                            <label class="name-filter">
                                Price
                            </label>
                            <b>10 €</b><input class="filter-price span2" type="text"  value="" data-slider-min="10" data-slider-max="1000" data-slider-step="5" data-slider-value="[250,450]"/> <b>€ 1000</b>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-9">
                <div class="result-search">
                    <div class="characters-panel">
                        <ul>
                            <li>
                                <span class="fa fa-info-circle"></span>
                                Informatie hotel
                            </li>
                            <li>
                                <span class="fa fa-ticket"></span>
                                Oferta speciala
                            </li>
                            <li>
                                <span class="fa fa-clock-o"></span>
                                Early booking
                            </li>
                            <li>
                                <span class="fa fa-fire"></span>
                                Free child
                            </li>
                            <li>
                                <span class="fa fa-check-circle"></span>
                                Disponibil
                            </li>
                            <li>
                                <span class="fa fa-check-circle disable"></span>
                                La cerere
                            </li>
                            <li>
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 65.518 65.518" style="enable-background:new 0 0 65.518 65.518;" xml:space="preserve">
                                    <g>
                                        <path d="M32.759,0C14.696,0,0,14.695,0,32.759s14.695,32.759,32.759,32.759s32.759-14.695,32.759-32.759S50.822,0,32.759,0z
                                             M6,32.759C6,18.004,18.004,6,32.759,6c6.648,0,12.734,2.443,17.419,6.472L12.472,50.178C8.443,45.493,6,39.407,6,32.759z
                                             M32.759,59.518c-5.948,0-11.447-1.953-15.895-5.248l37.405-37.405c3.295,4.448,5.248,9.947,5.248,15.895
                                            C59.518,47.514,47.514,59.518,32.759,59.518z"/>
                                    </g>
                                </svg>
                                Nedisponibil
                            </li>
                        </ul>
                    </div>
                    <div class="info-about-result">
                        <div>&nbsp;</div>
                    </div>
                    <div class="result-group-item sejur">
                       <div class="result-wrap">
                           <div class="info-result head" style="background-color: antiquewhite;">
                               <div class="row no-padding">
                                   <div class="col-md-4 text-left">
                                       Nume hotel
                                   </div>
                                   <div class="col-md-2 text-center">
                                       Categorie camere
                                   </div>
                                   <div class="col-md-2 text-center">
                                       Board basis
                                   </div>
                                   <div class="col-md-1 text-center">
                                       Pret
                                   </div>
                                   <div class="col-md-1 text-center">
                                       Avion
                                   </div>
                                   <div class="col-md-1 text-center">
                                       Hotel
                                   </div>
                                   <div class="col-md-1">

                                   </div>
                               </div>
                           </div>
                       </div>
                       <div class="result-content">
                           
                       </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div id="loader" style="display: none; position: fixed;z-index: 9999999999;background-color: rgba(34, 34, 34, 0.68);top: 0;width: 100%;bottom: 0;">
    <div style="position: absolute;background: #fff;left: 50%;top: 40%;width: 240px;height: 150px;margin-left: -120px;">
        <div style="text-align: center;font-size: 34px;line-height: 140px;">
            Loading...
        </div>
    </div>
</div>

<?php Modal::begin([
    'id' => 'offer-modal',
    'header' => '<h3>Detalii sejurului</h3>',
]) ?>

    <div class="text-center">
        <img src="https://s-media-cache-ak0.pinimg.com/originals/65/97/87/659787288f1824921c38dcf4d3158768.gif" />
    </div>

<?php Modal::end() ?>

<style>
    #offer-modal .modal-dialog {
        width: 1140px
    }
</style>


<?php $this->registerJs("
    
    $('.result-content').on('click', 'a.get-offer', function(){
        var elem = $(this);
        $('#offer-modal .modal-body').html('<div class=\"text-center\"><img src=\"https://s-media-cache-ak0.pinimg.com/originals/65/97/87/659787288f1824921c38dcf4d3158768.gif\" /></div>');
        $('#offer-modal').modal('show');
        
        $.post('" . Url::to(['/tour/tour-search/info']) . "', {hash: elem.attr('data-hash'), rateKeyData: elem.attr('data-rate-key'), nr: elem.attr('data-nr')}, function(json){
            $('#offer-modal .modal-body').html(json.html);
        }, 'json');
    });

    $('.form-submit').click(function(){
        if ($('#DepartureDate').val() == null || $('#DepartureDate').val() == '')
        {
            alert('Selectati data plecarii');
            return;
        }
        
        $('#tour-search-form select[name^=Adults]:hidden, #tour-search-form select[name^=Childs]:hidden, #tour-search-form select[name^=ChildAge]:hidden').attr('disabled', 'disabled');
        
        $('#loader').fadeIn();

        $.post('" . Url::to(['/tour/tour-search/search']) . "', {data: $('#tour-search-form').serialize(), filters: []}, function(json){
            $('.result-content').html(json.html);
            $('.info-about-result').html(json.summary);
            $('#loader').fadeOut();
        }, 'json');
    });
    
    $('#filter-form input').change(function(){
        $.post('" . Url::to(['/tour/tour-search/search']) . "', {data: $('#tour-search-form').serialize(), filters: $('#filter-form').serialize()}, function(json){
            $('.result-content').html(json.html);
            $('.info-about-result').html(json.summary);
            $('#loader').fadeOut();
        }, 'json');
    });
    
    $('.result-content').on('click', '.more-info', function(){
        var elem = $(this);
        var row = elem.closest('.sejur-item');
        
        if (elem.hasClass('fa-minus'))
        {
            row.find('.tour-price-results').slideUp();
            elem.removeClass('fa-minus');
        }
        else
        {
            row.find('.tour-price-results').slideDown();
            elem.addClass('fa-minus');
        }
    });
    
") ?>
