<?php

namespace app\modules\Tour\entities;

/**
 * Description of Hotel Offer
 */
class HotelOffer
{
    
    /** 
     * Availability type (Immediate, OnRequest)
     * @var string
     */
    public $Availability;
    
    /** 
     * CheckIn date Y-m-d
     * @var string
     */
    public $CheckIn;
    
    /** 
     * CheckOut date Y-m-d
     * @var string
     */
    public $CheckOut;
    
    /** 
     * Product price
     * @var float
     */
    public $Price;
    
    /** 
     * Old price
     * @var float
     */
    public $OldPrice;
    
    /** 
     * Rate key data
     * @var array
     */
    public $RateKeyData = [];
    
    /** 
     * Room name
     * @var string
     */
    public $RoomName;
    
    /** 
     * Board name
     * @var string
     */
    public $BoardName;
    
    /**
     * List included servicces
     * @var OfferService[]
     */
    public $Services;
    
    public function getBoards()
    {
        if (is_array($this->BoardName))
        {
            return implode(', ', $this->BoardName);
        }
        
        return $this->BoardName;
    }
    
}
