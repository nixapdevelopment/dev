<?php

namespace app\modules\Tour\entities;

/**
* Tour common class
*/
class Tour
{
    
    /**
     * Internal operator ID
     * @var integer
     */
    public $OperatorID;
    
    /**
     * Tour hotel name
     * @var string
     */
    public $HotelName;
    
    /**
     * Tour hotel code
     * @var string
     */
    public $HotelCode;
    
    /**
     * Operator country code
     * @var string
     */
    public $CountryCode;
    
    /**
     * Operator city code
     * @var string
     */
    public $CityCode;
    
    /**
     * Operator city name
     * @var string
     */
    public $CityName;
    
    /** 
     * Hotel main image
     * @var string
     */
    public $MainImage;
    
    /** 
     * Hotel stars
     * @var integer 0 - 5
     */
    public $HotelStars;
    
    /** 
     * Hotel class (Hotel, Villa, Appartament, etc...)
     * @var string
     */
    public $HotelClass;
    
    /** 
     * Hotel Latitude
     * @var float
     */
    public $Latitude;
    
    /** 
     * Hotel Longitude
     * @var float
     */
    public $Longitude;
    
    /** 
     * Price sets
     * @var TourPriceSet[]
     */
    public $PriceSets = [];
    
    /** 
     * Tour hotel offers
     * @var HotelOffer[]
     */
    public $Offers = [];
    
    /** 
     * Tour operator code
     * @var string  
     */
    public $TourOpCode = '';
    
    /** 
     * Tour operator code
     * @return HotelOffer  
     */
    public function firstOffer()
    {
        return reset($this->Offers);                
    }
    
}
