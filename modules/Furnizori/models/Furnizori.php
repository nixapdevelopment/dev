<?php

namespace app\modules\Furnizori\models;

use Yii;
use app\modules\Agenti\models\Agenti;
use app\modules\Judet\models\Judet;

/**
 * This is the model class for table "Furnizori".
 *
 * @property integer $ID
 * @property string $Denumire
 * @property string $CodFiscal
 * @property string $ContAnalitic
 * @property integer $Judet
 * @property string $Adresa
 * @property string $ContBancar
 * @property string $Banca
 * @property string $NrRegistrulComertului
 * @property string $Telefon
 * @property string $Email
 * @property integer $Agent
 */
class Furnizori extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Furnizori';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Denumire', 'CodFiscal', 'ContAnalitic'], 'required'],
            [['Judet', 'Agent'], 'integer'],
            [['Denumire', 'CodFiscal', 'ContAnalitic', 'Adresa', 'ContBancar', 'Banca', 'NrRegistrulComertului', 'Telefon', 'Email'], 'string', 'max' => 255],
            [['Agent'], 'exist', 'skipOnError' => true, 'targetClass' => Agenti::className(), 'targetAttribute' => ['Agent' => 'ID']],
            [['Judet'], 'exist', 'skipOnError' => true, 'targetClass' => Judet::className(), 'targetAttribute' => ['Judet' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Denumire' => Yii::t('app', 'Denumire'),
            'CodFiscal' => Yii::t('app', 'Cod Fiscal'),
            'ContAnalitic' => Yii::t('app', 'Cont Analitic'),
            'Judet' => Yii::t('app', 'Judet'),
            'Adresa' => Yii::t('app', 'Adresa'),
            'ContBancar' => Yii::t('app', 'Cont Bancar'),
            'Banca' => Yii::t('app', 'Banca'),
            'NrRegistrulComertului' => Yii::t('app', 'Nr. Registrul Comertului'),
            'Telefon' => Yii::t('app', 'Telefon'),
            'Email' => Yii::t('app', 'Email'),
            'Agent' => Yii::t('app', 'Agent'),
        ];
    }
    
    public function getJudet()
    {
        return $this->hasOne(Judet::className(), ['ID' => 'Judet']);
    }
    
    public function getAgenti()
    {
        return $this->hasOne(Agenti::className(), ['ID' => 'Agent']);
    }
    
}
