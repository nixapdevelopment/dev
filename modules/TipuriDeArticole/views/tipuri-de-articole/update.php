<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\TipuriDeArticole\models\TipuriDeArticole */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Tipuri De Articole',
]) . $model->ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tipuri De Articoles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="tipuri-de-articole-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
