<?php

namespace app\modules\TipuriDeArticole\models;

use Yii;

/**
 * This is the model class for table "TipuriDeArticole".
 *
 * @property integer $ID
 * @property string $Denumire
 * @property string $ContArticol
 * @property string $Diferente
 * @property string $TVANeexigibila
 * @property string $Cheltuieli
 * @property string $Venituri
 * @property integer $Aprovizionabil
 * @property integer $Consumabil
 * @property integer $Vandabil
 * @property integer $Produs
 * @property integer $Nestocat
 */
class TipuriDeArticole extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'TipuriDeArticole';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Denumire', 'ContArticol'], 'required'],
            [['Aprovizionabil', 'Consumabil', 'Vandabil', 'Produs', 'Nestocat'], 'integer'],
            [['Denumire'], 'string', 'max' => 255],
            [['ContArticol', 'Diferente', 'TVANeexigibila', 'Cheltuieli', 'Venituri'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Denumire' => Yii::t('app', 'Denumire'),
            'ContArticol' => Yii::t('app', 'Cont Articol'),
            'Diferente' => Yii::t('app', 'Diferente'),
            'TVANeexigibila' => Yii::t('app', 'TVA neexigibila'),
            'Cheltuieli' => Yii::t('app', 'Cheltuieli'),
            'Venituri' => Yii::t('app', 'Venituri'),
            'Aprovizionabil' => Yii::t('app', 'Aprovizionabil'),
            'Consumabil' => Yii::t('app', 'Consumabil'),
            'Vandabil' => Yii::t('app', 'Vandabil'),
            'Produs' => Yii::t('app', 'Produs'),
            'Nestocat' => Yii::t('app', 'Nestocat'),
        ];
    }
}
