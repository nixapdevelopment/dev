<?php

namespace app\modules\Testimonials;

use app\components\Module\SiteModule;

/**
 * Testimonials module definition class
 */
class Testimonials extends SiteModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Testimonials\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
