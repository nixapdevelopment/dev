<?php

namespace app\modules\OurInfo;

use app\components\Module\SiteModule;

/**
 * OurInfo module definition class
 */
class OurInfo extends SiteModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\OurInfo\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
