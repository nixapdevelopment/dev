<?php

namespace app\modules\OurInfo\controllers;

use app\controllers\SiteBackendController;

/**
 * Default controller for the `OurInfo` module
 */
class DefaultController extends SiteBackendController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
