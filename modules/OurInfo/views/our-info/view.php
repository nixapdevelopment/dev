<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\OurInfo\models\OurInfo */

$this->title = $model->ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Our Infos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="our-info-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?php
    if ($model->image) {
        echo Html::img($model->image->imagePath, ['width' => 400, 'class' => 'centered']);
    }
    ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

            'lang.Title',
            'lang.ShortContent',
            [   'attribute' => 'lang.Content',
                'format' => 'raw',
            ],

            'CreatedAt',
        ],
    ]) ?>

</div>
