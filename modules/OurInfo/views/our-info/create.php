<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\OurInfo\models\OurInfo */

$this->title = Yii::t('app', 'Create Our Info');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Our Infos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="our-info-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
