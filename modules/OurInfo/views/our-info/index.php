<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\OurInfo\models\OurInfoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Our Infos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="our-info-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Our Info'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'Image',
                'format' => 'html',
                'value'=>function($data) { if($data->image){return Html::img($data->image->thumbPath, ['width' => 50, 'class' => 'image']);} },


            ],
            'lang.Title',
            'lang.ShortContent',
            [
                    'attribute' => 'CreatedAt',
                    'filter' => false,
            ],



            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
