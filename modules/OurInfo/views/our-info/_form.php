<?php

use yii\helpers\Html;
use yii\bootstrap\Tabs;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\modules\OurInfo\models\OurInfo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="our-info-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
        ]
    ]); ?>

    <?php
    $initialPreview = [];
    $initialPreviewConfig = [];
    if ($model->image){



    $initialPreview[] = Html::img($model->image->imagePath,['width' => 200]);
    $initialPreviewConfig[] = [
    'url' => \yii\helpers\Url::to(['/site/our-info/our-info/image-delete']),
    'key' => $model->image->ID,
    ];
    }

    ?>

    <div class="row">
        <div class="col-md-12">
            <?php
            echo FileInput::widget([
                'name' => 'Image',
                'options'=>['accept'=>'image/*'],
                'pluginOptions' => [
                    'overwriteInitial'=>false,
                    'maxFileSize'=>2800,
                    'fileActionSettings' => [
                        'fileActionSettings' => [
                            'showZoom' => false,
                            'showDelete' => true,
                        ],
                    ],
                    'browseClass' => 'btn btn-success',
                    'uploadClass' => 'btn btn-info',
                    'removeClass' => 'btn btn-danger',
                    'showRemove' => false,
                    'showUpload' => false,
                    'initialPreview' => $initialPreview,
                    'initialPreviewConfig' => $initialPreviewConfig,

                ],

            ]);

            ?>

        </div></div>
    <div>


    <?= $form->field($model, 'CreatedAt')->textInput(['type' => 'hidden'])->label(false) ?>
    <?php
    $items = [];
    foreach ($model->langs as $langID => $langModel)
    {
        $items[] = [
            'label' => strtoupper($langID),
            'content' => $this->render('_desc_form',[
                'form' => $form,
                'langModel' => $langModel,
            ]),

        ];
    }

    echo Tabs::widget([
        'items' => $items,
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
