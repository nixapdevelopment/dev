<?php
use app\modules\Location\models\Location;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use dosamigos\tinymce\TinyMce;

?>

<?= $form->field($langModel, "[$langModel->LangID]Title")->textInput() ?>
<?= $form->field($langModel, "[$langModel->LangID]ShortContent")->textArea() ?>
<?= $form->field($langModel, "[$langModel->LangID]Content")->textArea()->widget(TinyMce::className(),[]) ?>



