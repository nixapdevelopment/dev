<?php
namespace app\modules\Articole\models;

use Yii;
 
class Articole extends \kartik\tree\models\Tree
{
    
    const SCENARIO_FOLDER = 'folder';
    const SCENARIO_ITEM = 'item';


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Articole';
    }    
    
    /**
     * Override isDisabled method if you need as shown in the  
     * example below. You can override similarly other methods
     * like isActive, isMovable etc.
     */
    public function isDisabled()
    {
        return parent::isDisabled();
    }
    
    public function attributeLabels() {
        return [
            'Name' => Yii::t('app', 'Denumire'),
            'UM' => Yii::t('app', 'Unitate de masura'),
            'StocCurent' => Yii::t('app', 'Stoc curent'),
        ];
    }
    
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['SKU', 'UM', 'Tip', 'CodBare'], 'safe'];
        $rules[] = [['PretVanzare', 'StocCurent', 'PretVanzareCuTVA', 'TVA'], 'default', 'value' => 0];
        $rules[] = [['UM', 'Tip'], 'required', 'on' => self::SCENARIO_ITEM, 'when' => function($model){
            return !$model->isNewRecord;
        }, 'whenClient' => "function (attribute, value) {
            return " . !$model->isNewRecord .";
        }"];
        $rules[] = [['PretVanzare', 'StocCurent', 'PretVanzareCuTVA', 'TVA'], 'number', 'min' => 0, 'on' => self::SCENARIO_ITEM];
        return $rules;
    }
    
    public function getChilds()
    {
        return $this->hasMany(Articole::className(), ['root' => 'id']);
    }
    
}