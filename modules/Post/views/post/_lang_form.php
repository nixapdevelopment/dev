<?php
 use app\modules\Category\models\Category;
 use kartik\datetime\DateTimePicker;
 use yii\bootstrap\Tabs;
?>



<?= $form->field($model, 'CreatedAt')->widget(DateTimePicker::className(),[

    'options' => [

        'value' => $model->niceDate,
    ],

    'pluginOptions' => [
        'format' => Yii::$app->params['dateTimeFormatJS'],
        'todayHighlight' => true
    ]]) ?>

<?php
$items = [];
foreach ($model->langs as $langID => $langModel)
{
    $items[] = [
        'label' => strtoupper($langID),
        'content' => $this->render('_desc_form',[
            'form' => $form,
            'langModel' => $langModel,
        ]),
    ];
}

echo Tabs::widget([
    'items' => $items,
]);
?>
