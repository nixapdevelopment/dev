<?php

use yii\helpers\Html;
use app\components\GridView\GridView;
use yii\widgets\Pjax;
use app\modules\Post\models\Post;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\Post\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Posts');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


<?php Pjax::begin();?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],


            [
                'attribute' => 'lang.Title',
                'label' => 'Title',
            ],
            [
                'attribute' => 'lang.Content',
                'label' => 'Content',
            ],
            [
                'attribute' => 'CreatedAt',
                'label' => 'Created',
                'filter' => '',

                'value' => function ($model){
                    return $model->niceDate;
                },


            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'header'=> Html::a(Yii::t('app', 'Adauga'), ['create'], ['class' => 'btn btn-success','data-pjax'=> 0])
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
