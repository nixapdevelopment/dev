<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\modules\Post\models\Post */

$this->title = $model->ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Posts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'lang.Title',

            [
                'attribute' => 'CreatedAt',
                'label' => 'Created',
                'value' => function ($model){
                    return $model->niceDate;
                },
            ],
            [
                'attribute' => 'lang.Content',
                'format' => 'raw',
            ]


        ],
    ]) ?>

</div>
    <span>Comments:</span>

    <?php  Pjax::begin();
        $provider = new ArrayDataProvider([
             'allModels' => $model->comments,
             'pagination' => [
                 'pageSize' => 10,
             ],
        ]);

    echo GridView::widget([
        'dataProvider' => $provider,
        'columns' => [
            'Content',
            'CreatedAt:datetime',

        ],
    ]);

         Pjax::end(); ?>