<?php

    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use yii\bootstrap\Modal;
    use yii\helpers\Url;
    use kartik\grid\GridView;
    use yii\helpers\ArrayHelper;
    use kartik\select2\Select2;

?>

<?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-3">

        </div>
        <div class="col-md-6">
            <?= $form->field($sliderModel, 'Name') ?>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label style="display: block;" class="control-label">&nbsp;</label>
                <?= Html::submitButton($sliderModel->isNewRecord ? 'Create' : 'Update', ['class' => $sliderModel->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
    </div>

<?php ActiveForm::end(); ?>


<?php if($sliderModel->isNewRecord != "Create"){ ?>

<hr />

<div>
    <?= Html::button('<i class="fa fa-plus"></i> Add slider item', [
        'class' => 'btn btn-success edit-slider-item',
        'slider-item-id' => 0
    ]) ?>
</div>

<div>
        
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'options' => [
                    'width' => '50px'
                ]
            ],
            [
                'label'  => 'Name',
                'value' => function($model){
                    return $model->Name;
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'options' => [
                    'width' => '100px'
                ],
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'title' => 'Edit'
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash text-danger"></span>', $url, [
                            'title' => 'Delete',
                            'onclick' => "return confirm('Delete?')"
                        ]);
                    }
                ],
                'template' => '<div class="text-center">{update}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{delete}</div>'
            ],
        ],
    ]); ?>
    
    
</div>



<?php Modal::begin([
    'id' => 'edit-slider-item-modal',
    'header' => 'Edit slider item',
    'size' => Modal::SIZE_LARGE
]); ?>

<?php Modal::end(); ?>

<?php $this->registerJs("
    $(document).on('click', '.edit-slider-item', function(){
        $('#edit-slider-item-modal .modal-body').empty();
        $.post('" . Url::to(['get-item']) . "', {id: $(this).attr('slider-item-id'), sliderID:".Yii::$app->getRequest()->getQueryParam('id')."}, function(html){
            $('#edit-slider-item-modal .modal-body').html(html);
            $('#edit-slider-item-modal').modal();
        });
    });
") ?>

<?php } ?>