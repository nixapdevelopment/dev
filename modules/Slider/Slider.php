<?php

namespace app\modules\Slider;

use app\components\Module\SiteModule;
/**
 * Slider module definition class
 */
class Slider extends SiteModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Slider\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
