<?php

namespace app\modules\Comment\models;

use Yii;
use app\modules\Post\models\Post;

/**
 * This is the model class for table "Comment".
 *
 * @property integer $ID
 * @property integer $PostID
 * @property string $Status
 * @property string $Content
 * @property string $CreatedAt
 *
 * @property Post $post
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const StatusActive = 'active';
    const StatusInActive = 'inactive';

    public static function tableName()
    {
        return 'Comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['PostID', 'Status', 'Content'], 'required'],
            [['PostID'], 'integer'],
            [['Content'], 'string'],
            [['CreatedAt'], 'safe'],
            [['Status'], 'string', 'max' => 50],
            [['PostID'], 'exist', 'skipOnError' => true, 'targetClass' => Post::className(), 'targetAttribute' => ['PostID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'PostID' => Yii::t('app', 'Post ID'),
            'Status' => Yii::t('app', 'Status'),
            'Content' => Yii::t('app', 'Content'),
            'CreatedAt' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
         return $this->hasOne(Post::className(), ['ID' => 'PostID'])->with('lang');
    }

    public function getNiceDate()
    {
        return  date(Yii::$app->params['dateTimeFormatPHP'],strtotime($this->CreatedAt));
    }
    public static function getStatusList($addEmpty = false){
        $return = $addEmpty ? ['' => $addEmpty] : [];

        $list = [
            self::StatusActive => Yii::t('app', 'active'),
            self::StatusInActive => Yii::t('app', 'inactive'),
        ];

        return $return + $list;
    }
}
