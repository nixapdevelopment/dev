<?php

use yii\helpers\Html;
use app\components\GridView\GridView;
use yii\widgets\Pjax;
use kartik\datetime\DateTimePicker;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\Comment\models\CommentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Comments');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comment-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'PostID',
                'label' => 'On Post',
                'value' => 'post.lang.Title',
            ],
            'Status',
            'Content:ntext',
            [
                'attribute' => 'CreatedAt',
                'label' => 'Created',
                'value' => function ($model){
                    return $model->niceDate;
                },
                'filter'=> DateTimePicker::widget([
                    'model' => $searchModel,
                    'attribute'=> 'CreatedAt',
                    'options' => ['placeholder' => 'Select issue date ...'],
                    'pluginOptions' => [

                        'todayHighlight' => true
                    ]
                ]),

            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
