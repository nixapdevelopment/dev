<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\Comment\models\Comment;


/* @var $this yii\web\View */
/* @var $model app\modules\Comment\models\Comment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="comment-form">
    <div class="form-group">
        <?= Html::tag('span','Post : ')?>
        <?= Html::tag('span',$model->post->lang->Title)?>
    </div>
    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'Status')->dropDownList ( Comment::getStatusList() ) ?>

    <?= $form->field($model, 'Content')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'CreatedAt')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
