<?php

namespace app\modules\SeoPost\models;

use Yii;

/**
 * This is the model class for table "SeoPostLang".
 *
 * @property integer $ID
 * @property integer $SeoPostID
 * @property string $LangID
 * @property string $Title
 * @property string $Content
 *
 * @property SeoPost $seoPost
 */
class SeoPostLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SeoPostLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['LangID', 'Title', 'Content'], 'required'],
            [['SeoPostID'], 'integer'],
            [['Content'], 'string'],
            [['LangID'], 'string', 'max' => 2],
            [['Title'], 'string', 'max' => 255],
            [['SeoPostID'], 'exist', 'skipOnError' => true, 'targetClass' => SeoPost::className(), 'targetAttribute' => ['SeoPostID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'SeoPostID' => Yii::t('app', 'Seo Post ID'),
            'LangID' => Yii::t('app', 'Lang ID'),
            'Title' => Yii::t('app', 'Title'),
            'Content' => Yii::t('app', 'Content'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeoPost()
    {
        return $this->hasOne(SeoPost::className(), ['ID' => 'SeoPostID']);
    }
}
