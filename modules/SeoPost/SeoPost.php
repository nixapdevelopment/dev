<?php

namespace app\modules\SeoPost;

use app\components\Module\SiteModule;
/**
 * SeoPost module definition class
 */
class SeoPost extends SiteModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\SeoPost\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
