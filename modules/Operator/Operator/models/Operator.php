<?php

namespace app\modules\Operator\Operator\models;

use Yii;

/**
 * This is the model class for table "Operator".
 *
 * @property integer $ID
 * @property string $Name
 * @property string $Class
 *
 * @property OperatorLocation[] $operatorLocations
 */
class Operator extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Operator';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name', 'Class'], 'required'],
            [['Name', 'Class'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Name' => Yii::t('app', 'Name'),
            'Class' => Yii::t('app', 'Class'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperatorLocations()
    {
        return $this->hasMany(OperatorLocation::className(), ['OperatorID' => 'ID']);
    }
}
