<?php

namespace app\modules\Operator\Operator\models;

use Yii;
use app\modules\Location\models\Location;

/**
 * This is the model class for table "OperatorLocation".
 *
 * @property integer $ID
 * @property integer $LocationID
 * @property integer $OperatorID
 * @property string $OperatorLocationID
 *
 * @property Location $location
 * @property Operator $operator
 */
class OperatorLocation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'OperatorLocation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['LocationID', 'OperatorID', 'OperatorLocationID'], 'required'],
            [['LocationID', 'OperatorID'], 'integer'],
            [['OperatorLocationID'], 'string', 'max' => 255],
            [['LocationID'], 'exist', 'skipOnError' => true, 'targetClass' => Location::className(), 'targetAttribute' => ['LocationID' => 'ID']],
            [['OperatorID'], 'exist', 'skipOnError' => true, 'targetClass' => Operator::className(), 'targetAttribute' => ['OperatorID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'LocationID' => Yii::t('app', 'Location ID'),
            'OperatorID' => Yii::t('app', 'Operator ID'),
            'OperatorLocationID' => Yii::t('app', 'Operator Location ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(Location::className(), ['ID' => 'LocationID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperator()
    {
        return $this->hasOne(Operator::className(), ['ID' => 'OperatorID']);
    }
}
