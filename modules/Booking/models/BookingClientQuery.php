<?php

namespace app\modules\Booking\models;

/**
 * This is the ActiveQuery class for [[BookingClient]].
 *
 * @see BookingClient
 */
class BookingClientQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return BookingClient[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return BookingClient|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
