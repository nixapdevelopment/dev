<?php
use app\modules\Location\models\Location;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use dosamigos\tinymce\TinyMce;

?>

<?= $form->field($langModel, "[$langModel->LangID]Question")->textInput() ?>
<?= $form->field($langModel, "[$langModel->LangID]Content")->textarea()->widget(TinyMce::className()) ?>


