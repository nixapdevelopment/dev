<?php

namespace app\modules\Questions\models;

use Yii;

/**
 * This is the model class for table "QuestionsLang".
 *
 * @property integer $ID
 * @property string $LangID
 * @property integer $QuestionID
 * @property string $Question
 * @property string $Content
 *
 * @property Questions $question
 */
class QuestionsLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'QuestionsLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['LangID', 'QuestionID', 'Question', 'Content'], 'required'],
            [['QuestionID'], 'integer'],
            [['Content'], 'string'],
            [['LangID'], 'string', 'max' => 2],
            [['Question'], 'string', 'max' => 255],
            [['QuestionID'], 'exist', 'skipOnError' => true, 'targetClass' => Questions::className(), 'targetAttribute' => ['QuestionID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'LangID' => 'Lang ID',
            'QuestionID' => 'Question ID',
            'Question' => 'Question',
            'Content' => 'Content',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(Questions::className(), ['ID' => 'QuestionID']);
    }
}
