<?php

namespace app\modules\Parser\providers;

use Yii;
use yii\base\Component;

abstract class Provider extends Component
{
    
    public $cachePath = '@app/modules/Parser/cache';

    public abstract function getLocations();
    
    public abstract function searchHotels($data, $hash);
    
//    public abstract function hotelInfo($data);
//    
//    public abstract function doReservation($data);
//    
//    public abstract function renderResults();
    
}
