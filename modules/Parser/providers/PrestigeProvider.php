<?php

namespace app\modules\Parser\providers;

use yii\httpclient\Client;
use app\modules\Location\models\Location;
use app\modules\Location\models\LocationLang;
use app\modules\Operator\Operator\models\OperatorLocation;

class PrestigeProvider extends Provider
{
    
    public $operatorID = 1;

    public function getLocations()
    {
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('get')
            ->setUrl("http://online2.calypsotour.com/export/default.php?samo_action=api&version=1.0&oauth_token=e3d295d44ade456d8a1bbd8d245c2c79&type=xml&action=SearchTour_TOWNS")
            ->send();
        
        if ($response->isOk)
        {
            $turkey = Location::find()->where(['Code' => 'TR'])->one();
            
            if (empty($turkey->ID))
            {
                $turkey = new Location();
                $turkey->ParentID = null;
                $turkey->CountryID = null;
                $turkey->Code = 'TR';
                $turkey->Type = Location::TypeCountry;
                $turkey->Latitude = 0;
                $turkey->Longitude = 0;
                $turkey->save(false);

                $operatorLocation = new OperatorLocation();
                $operatorLocation->OperatorID = $this->operatorID;
                $operatorLocation->LocationID = $turkey->ID;
                $operatorLocation->OperatorLocationID = 72;
                $operatorLocation->save(false);

                $regionLang = new LocationLang();
                $regionLang->LangID = 'ro';
                $regionLang->LocationID = $turkey->ID;
                $regionLang->Name = 'Turkey';
                $regionLang->save(false);
            }
            
            foreach ($response->data['items']['item'] as $item)
            {
                $city = Location::find()->joinWith('lang')->where(['Name' => $item['name'], 'Type' => Location::TypeCity])->one();
                $region = Location::find()->joinWith('lang')->where(['Name' => $item['region'], 'Type' => Location::TypeRegion])->one();
                
                if (!$region)
                {
                    $region = new Location();
                    $region->ParentID = $turkey->ID;
                    $region->CountryID = $turkey->ID;
                    $region->Type = Location::TypeRegion;
                    $region->Latitude = 0;
                    $region->Longitude = 0;
                    $region->save(false);
                    
                    $operatorLocation = new OperatorLocation();
                    $operatorLocation->OperatorID = $this->operatorID;
                    $operatorLocation->LocationID = $region->ID;
                    $operatorLocation->OperatorLocationID = $item['regionKey'];
                    $operatorLocation->save(false);
                    
                    $regionLang = new LocationLang();
                    $regionLang->LangID = 'ro';
                    $regionLang->LocationID = $region->ID;
                    $regionLang->Name = $item['region'];
                    $regionLang->save(false);
                }
                
                if (!$city)
                {
                    $city = new Location();
                    $city->ParentID = $region->ID;
                    $city->CountryID = $turkey->ID;
                    $city->Type = Location::TypeCity;
                    $city->Latitude = 0;
                    $city->Longitude = 0;
                    $city->save(false);
                    
                    $operatorLocation = new OperatorLocation();
                    $operatorLocation->OperatorID = $this->operatorID;
                    $operatorLocation->LocationID = $city->ID;
                    $operatorLocation->OperatorLocationID = $item['id'];
                    $operatorLocation->save(false);
                    
                    $cityLang = new LocationLang();
                    $cityLang->LangID = 'ro';
                    $cityLang->LocationID = $city->ID;
                    $cityLang->Name = $item['name'];
                    $cityLang->save(false);
                }
            }
        }
    }
    
    public function searchHotels($data, $hash)
    {
        return [];
    }
    
}
