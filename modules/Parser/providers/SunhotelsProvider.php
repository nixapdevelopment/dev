<?php

namespace app\modules\Parser\providers;

use app\modules\Parser\providers\Provider;
use yii\httpclient\Client;
use app\modules\Location\models\Location;
use app\modules\Location\models\LocationLang;
use app\modules\Operator\Operator\models\OperatorLocation;
use app\modules\Parser\components\RemoteImage;
use app\modules\Parser\entities\Hotel;
use app\modules\Parser\entities\HotelRoom;
use app\modules\Parser\entities\RoomBoard;
use app\modules\Parser\entities\RoomBoardPrice;
use app\modules\Parser\entities\CancellationPolicy;

class SunhotelsProvider extends Provider
{

    public $operatorID = 4;
    public $login = 'Dialecttest';
    public $password = 'Dialecttest1';
    public $destinationEndpoint = 'http://xml.sunhotels.net/15/PostGet/StaticXMLAPI.asmx/GetDestinations';
    public $searchEndpoint = 'http://xml.sunhotels.net/15/PostGet/StaticXMLAPI.asmx/SearchV2';

    public function getLocations()
    {
        $client = new Client();
        $response = $client->createRequest()
                ->setMethod('get')
                ->setHeaders([
                    'Content-Type'   => 'text/xml;charset=UTF-8',
                    'Content-Length' => 0,
                ])
                ->setUrl($this->destinationEndpoint . "?userName=$this->login&password=$this->password&language=en&destinationCode=&sortBy=Destination&sortOrder=Ascending&exactDestinationMatch=")
                ->send();

        if ($response->isOk)
        {
            foreach ($response->data['Destinations']['Destination'] as $destination)
            {
                $country = Location::find()->joinWith('lang')->where(['Name' => $destination['CountryName'], 'Type' => Location::TypeCountry])->one();

                if (empty($country->ID))
                {
                    $country = new Location();
                    $country->Type = Location::TypeCountry;
                    $country->Latitude = 0;
                    $country->Longitude = 0;
                    $country->Code = $destination['CountryCode'];
                    $country->save(false);

                    $operatorLocation = new OperatorLocation();
                    $operatorLocation->OperatorID = $this->operatorID;
                    $operatorLocation->LocationID = $country->ID;
                    $operatorLocation->OperatorLocationID = $destination['destination_id'];
                    $operatorLocation->save(false);

                    $regionLang = new LocationLang();
                    $regionLang->LangID = 'ro';
                    $regionLang->LocationID = $country->ID;
                    $regionLang->Name = $destination['CountryName'];
                    $regionLang->save(false);
                }

                $location = Location::find()->joinWith('lang')->where(['Name' => $destination['DestinationName'], 'Type' => Location::TypeCity, 'CountryID' => $country->ID])->one();

                if (empty($location->ID))
                {
                    $location = new Location();
                    $location->Type = Location::TypeCity;
                    $location->ParentID = $country->ID;
                    $location->CountryID = $country->ID;
                    $location->Latitude = 0;
                    $location->Longitude = 0;
                    $location->Code = $destination['DestinationCode'];
                    $location->save(false);

                    $operatorLocation = new OperatorLocation();
                    $operatorLocation->OperatorID = $this->operatorID;
                    $operatorLocation->LocationID = $location->ID;
                    $operatorLocation->OperatorLocationID = $destination['destination_id'];
                    $operatorLocation->save(false);

                    $regionLang = new LocationLang();
                    $regionLang->LangID = 'ro';
                    $regionLang->LocationID = $location->ID;
                    $regionLang->Name = $destination['DestinationName'];
                    $regionLang->save(false);
                }
            }
        }
        else
        {
            echo '<pre>';
            print_r($response);
            exit;
        }
    }

    public function searchHotels($data, $hash)
    {

        $nights = (int) $data['Nights'];
        $plusTimeStr = $nights > 1 ? "+$nights days" : "+1 day";

        $checkIn = date('Y-m-d', strtotime($data['CheckIn']));
        $checkOut = date('Y-m-d', strtotime($plusTimeStr, strtotime($checkIn)));

        $rooms = (int) $data['Rooms'];

        $locationID = (int) $data['LocationID'];
        $location = Location::find()->joinWith(['operatorLocations'])->where(['Location.ID' => $locationID, 'OperatorLocation.OperatorID' => $this->operatorID])->one();

        if (empty($location->operatorLocations[$this->operatorID]))
        {
            return [];
        }

        $destinationID = $location->operatorLocations[$this->operatorID]->OperatorLocationID;
        $adults = array_sum($data['Adults']);
        $childs = array_sum($data['Childs']);

        $childAges = [];
        $infants = 0;
        if ($childs > 0 && isset($data['ChildAge']))
        {
            foreach ($data['ChildAge'] as $item)
            {
                foreach ($item as $it)
                {
                    if ($it == 1)
                    {
                        $infants++;
                    }
                    else
                    {
                        $childAges[] = (int) $it;
                    }
                }
            }
        }
        $childAgesStr = implode(',', $childAges);
        $childs = $childs - $infants;

        $roomsInBatch = $rooms > 1 ? $rooms : '';

        $client = new Client();
        $response = $client->createRequest()
                ->setMethod('get')
                ->setHeaders([
                    'Content-Type'   => 'text/xml;charset=UTF-8',
                    'Content-Length' => 0,
                ])
                ->setUrl($this->searchEndpoint . "?userName=$this->login&password=$this->password&language=en&currencies=EUR&checkInDate=$checkIn&checkOutDate=$checkOut&numberOfRooms=$rooms&destination=&destinationID=$destinationID&hotelIDs=&resortIDs=&accommodationTypes=Hotel&numberOfAdults=$adults&numberOfChildren=$childs&childrenAges=$childAgesStr&infant=$infants&sortBy=&sortOrder=&exactDestinationMatch=&blockSuperdeal=&showTransfer=&mealIds=&showCoordinates=1&showReviews=1&referencePointLatitude=&referencePointLongitude=&maxDistanceFromReferencePoint=&minStarRating=&maxStarRating=&featureIds=&minPrice=&maxPrice=&themeIds=&excludeSharedRooms=&excludeSharedFacilities=&prioritizedHotelIds=&totalRoomsInBatch=$rooms&paymentMethodId=&CustomerCountry=gb&B2C=")
                ->send();

        if ($response->isOk)
        {
            $result = [];
            foreach ($response->data['hotels']['hotel'] as $hotelOp)
            {
                if (empty($hotelOp['hotel.id']))
                {
                    continue;
                }
                
                $hotel = new Hotel();
                $hotel->ExternalID = $hotelOp['hotel.id'];
                $hotel->OperatorID = $this->operatorID;
                $hotel->Hash = $hash;
                $hotel->Stars = (int)$hotelOp['classification'];
                $hotel->Name = $hotelOp['name'];
                $hotel->Address = $hotelOp['hotel.address'];
                $hotel->Latitude = $hotelOp['coordinates']['latitude'];
                $hotel->Longitude = $hotelOp['coordinates']['longitude'];
                $hotel->IsBest = $hotelOp['isBest'] == 'true' ? true : false;
                $hotel->Description = $hotelOp['description'];
                $hotel->Reviews = $hotelOp['review'];
                
                $minPrice = [];
                
                // room types in hotel
                unset($roomtypes);
                if (isset($hotelOp['roomtypes']['roomtype'][0]))
                {
                    $roomtypes = $hotelOp['roomtypes']['roomtype'];
                }
                else
                {
                    $roomtypes[0] = $hotelOp['roomtypes']['roomtype'];
                }
                
                $roomInfo = [];
                $hotel->Rooms = [];
                foreach ($roomtypes as $keyRoom => $roomtype)
                {
                    $hotelRoom = new HotelRoom();
                    $hotelRoom->Name = $roomtype['room.type'];
                    
                    unset($rooms2);
                    if (isset($roomtype['rooms']['room'][0]))
                    {
                        $rooms2 = $roomtype['rooms']['room'];
                    }
                    else
                    {
                        $rooms2[0] = $roomtype['rooms']['room'];
                    }
                    
                    
                    foreach ($rooms2 as $room)
                    {
                        // cancelation policies
                        
                        if (!empty($room['cancellation_policies']['cancellation_policy']))
                        {
                            
                            if (isset($room['cancellation_policies']['cancellation_policy'][0]))
                            {
                                $cancelationPolicies = $room['cancellation_policies']['cancellation_policy'];
                            }
                            else
                            {
                                $cancelationPolicies[0] = $room['cancellation_policies']['cancellation_policy'];
                            }
                        }
                        
                        $cancelations = [];
                        foreach ($cancelationPolicies as $cp)
                        {
                            $cancellationPolicy = new CancellationPolicy();
                            $cancellationPolicy->Date = empty($cp['deadline']) ? null : date('d.m.Y H:i', strtotime('+' . $cp['deadline'] . ' hours', strtotime($data['CheckIn'] . ' 12:00:00')));
                            $cancellationPolicy->Amount = NULL;
                            $cancellationPolicy->Percent = floatval($cp['percentage']);
                            $cancellationPolicy->Description = $cp['text'];
                            
                            $cancelations[] = $cancellationPolicy;
                        }
                        
                        // room meals
                        unset($meals);
                        if (isset($room['meals']['meal'][0]))
                        {
                            $meals = $room['meals']['meal'];
                        }
                        else
                        {
                            $meals[0] = $room['meals']['meal'];
                        }
                        
                        
                        // prices (room and meals variations)
                        foreach ($meals as $meal)
                        {
                            $roomBoard = new RoomBoard();
                            $roomBoard->Name = $meal['name'];
                            
                            $roomBoardPrice = new RoomBoardPrice();
                            $roomBoardPrice->Price = round($meal['prices']['price'], 2);
                            $roomBoardPrice->Currency = 'EUR';
                            $roomBoardPrice->IsSuperDeal = $meal['isSuperDeal'] == 'true';
                            
                            $rateKeyData = [
                                0  => $roomtype['roomtype.ID'],
                                1  => $room['id'],
                                2  => $meal['id'],
                                3  => $rooms,
                                4  => $adults,
                                5  => $childs,
                                6  => $checkIn,
                                7  => $checkOut,
                            ];
                            $rateKeyData['rateKey'] = '';
                            $rateKeyData['boardCode'] = $meal['name'];
                            $rateKeyData['boardName'] = $meal['name'];
                            $rateKeyData['paymentType'] = '';
                            $rateKeyData['price'] = round($meal['prices']['price'], 2);
                            $rateKeyData['roomCode'] = $roomtype['roomtype.ID'];
                            $rateKeyData['roomName'] = $roomtype['room.type'];
                            $rateKeyData['roomCount'] = $data['Rooms'];
                            
                            $roomBoardPrice->RateKeyData = $rateKeyData;
                            
                            $roomBoardPrice->CancellationPolicies = $cancelations;
                            
                            $roomBoard->RoomBoardPrices[] = $roomBoardPrice;
                            
                            $hotelRoom->Boards[] = $roomBoard;
                        }
                        
                        
                    }
                    
                    $hotel->Rooms[] = $hotelRoom;
                    
                    $roomInfo[$roomtype['room.type']] = $roomtype['room.type'];
                }
                
                $hotel->RoomInfo = implode(', ', $roomInfo);
                
                $images = [];
                if (!empty($hotelOp['images']['image']))
                {
                    foreach ($hotelOp['images']['image'] as $img)
                    {
                        $images[] = 'http://www.sunhotels.net/Sunhotels.net/HotelInfo/hotelImage.aspx?id=' . $img['@attributes']['id'] . '&full=1';
                    }
                }
                
                $hotel->Images = $images;
                
                $hotel->MinPrice = $hotel->getMinPrice();
                
                $result[$hotel->OperatorID . '-' . $hotel->ExternalID] = $hotel;
            }
            
            return $result;
        }
        else
        {
            echo '<pre>';
            print_r($response);
            exit;
        }
    }

}
