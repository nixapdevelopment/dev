<?php

namespace app\modules\Parser\providers;

use yii\helpers\Url;
use yii\httpclient\Client;
use app\modules\Location\models\Location;
use app\modules\Location\models\LocationLang;
use app\modules\Operator\Operator\models\OperatorLocation;
use app\modules\Parser\entities\Hotel;
use app\modules\Parser\entities\HotelRoom;
use app\modules\Parser\entities\RoomBoard;
use app\modules\Parser\entities\RoomBoardPrice;
use app\modules\Parser\entities\CancellationPolicy;
use app\modules\Parser\components\RattingConverter;
use app\modules\Parser\components\RemoteImage;

class HotelbedsProvider extends Provider
{
    
    public $operatorID = 2;
    
    
    public $hApiKey = 'edue7xwv8u7fxvucj33vejha';
    public $hApiSecret = 'rWbH5fzfuQ';

    public function getLocations()
    {
        
        $signature = hash("sha256", 'yvgbs9qp3mwnevhtge4j27jr' . 'DM2htc6jj8' . time());
        
        $endpoint = "https://api.test.hotelbeds.com/activity-content-api/3.0/countries/ro";
        
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('get')
            ->setUrl($endpoint)
            ->setHeaders([
                "Api-Key"     => 'yvgbs9qp3mwnevhtge4j27jr',
                "X-Signature" => $signature,
                "Accept"      => "application/xml"
            ])
            ->send();
        
        if ($response->isOk)
        {
            OperatorLocation::deleteAll(['OperatorID' => $this->operatorID]);
            
            foreach ($response->data['countries'] as $country)
            {
                $code = $country['@attributes']['code'];
                $name = $country['@attributes']['name'];
                
                $country = Location::find()->joinWith('lang')->where(['Name' => $name, 'Type' => Location::TypeCountry])->one();
                
                if (empty($country->ID))
                {
                    $country = new Location();
                    $country->Type = Location::TypeCountry;
                    $country->Latitude = 0;
                    $country->Longitude = 0;
                    $country->Code = $code;
                    $country->save(false);

                    $regionLang = new LocationLang();
                    $regionLang->LangID = 'ro';
                    $regionLang->LocationID = $country->ID;
                    $regionLang->Name = $name;
                    $regionLang->save(false);
                }
                
                $operatorLocation = new OperatorLocation();
                $operatorLocation->OperatorID = $this->operatorID;
                $operatorLocation->LocationID = $country->ID;
                $operatorLocation->OperatorLocationID = $code;
                $operatorLocation->save(false);
            }
        }
        
        $countries = Location::find()->where(['Type' => Location::TypeCountry])->groupBy('Code')->all();
        
        foreach ($countries as $country)
        {
            if (!empty($country->Code))
            {
                $code = strtoupper($country->Code);
                
                $signature = hash("sha256", 'yvgbs9qp3mwnevhtge4j27jr' . 'DM2htc6jj8' . time());
        
                $endpoint = "https://api.test.hotelbeds.com/activity-content-api/3.0/destinations/en/$code";

                $client = new Client();
                $response = $client->createRequest()
                    ->setMethod('get')
                    ->setUrl($endpoint)
                    ->setHeaders([
                        "Api-Key"     => 'yvgbs9qp3mwnevhtge4j27jr',
                        "X-Signature" => $signature,
                        "Accept"      => "application/xml"
                    ])
                    ->send();

                if ($response->isOk)
                {
                    if (empty($response->data['country']))
                    {
                        continue;
                    }
                    
                    
                    foreach ($response->data['country']['destinations'] as $destination)
                    {
                        $code = $destination['@attributes']['code'];
                        $name = $destination['@attributes']['name'];
                        
                        if (empty($code))
                        {
                            continue;
                        }
                        
                        $city = Location::find()->joinWith('lang')->where(['Name' => $name])->limit(1)->one();
                        
                        if (empty($city->ID))
                        {
                            $city = new Location();
                            $city->Type = Location::TypeCity;
                            $city->CountryID = $country->ID;
                            $city->ParentID = $country->ID;
                            $city->Latitude = 0;
                            $city->Longitude = 0;
                            $city->Code = $code;
                            $city->save(false);
                            
                            $cityLang = new LocationLang();
                            $cityLang->LangID = 'ro';
                            $cityLang->LocationID = $city->ID;
                            $cityLang->Name = $name;
                            $cityLang->save(false);
                        }
                        
                        $operatorLocation = new OperatorLocation();
                        $operatorLocation->OperatorID = $this->operatorID;
                        $operatorLocation->LocationID = $city->ID;
                        $operatorLocation->OperatorLocationID = $code;
                        $operatorLocation->save(false);
                    }
                }
            }
        }
        
    }
    
    public function searchHotels($data, $hash)
    {
        $location = Location::find()->joinWith('operatorLocations')->where(['Location.ID' => $data['LocationID']])->one();
        
        $checkIn = date('Y-m-d', strtotime($data['CheckIn']));
        $nights = (int)$data['Nights'];
        $checkOut = date('Y-m-d', strtotime('+' . $nights . ' day' . ($nights > 1 ? 's' : ''), strtotime($data['CheckIn'])));
        
        $occupancies = '<occupancies>';
        
        $occupanciesData = [];
        for ($i = 1; $i <= $data['Rooms']; $i++)
        {
            $occupanciesData[$i]['Rooms'] = 1;
            $occupanciesData[$i]['Adults'] = $data['Adults'][$i];
            $occupanciesData[$i]['Childs'] = isset($data['Childs'][$i]) ? $data['Childs'][$i] : 0; // ???????????????????
            
            $occupancies .= '<occupancy rooms="1" adults="' . $data['Adults'][$i] . '" children="' . (isset($data['Childs'][$i]) ? $data['Childs'][$i] : 0) . '">';
            
            $occupancies .= '<paxes>';
            for ($j = 1; $j <= $data['Adults'][$i]; $j++)
            {
                $occupancies .= '<pax type="AD" />';
            }
            
            if (isset($data['Childs'][$i]))
            {
                for ($k = 1; $k <= $data['Childs'][$i]; $k++)
                {
                    $occupanciesData[$i]['Childs'][$k] = $data['ChildAge'][$i][$k];
                    $occupancies .= '<pax type="CH" age="' . $data['ChildAge'][$i][$k] . '" />';
                }
            }
            
            $occupancies .= '</paxes>';
            
            $occupancies .= '</occupancy>';
        }
        
        $occupancies .= '</occupancies>';
        
//        echo '<pre>';
//        print_r($occupancies);
//        exit;
        
        $operatorLocation = OperatorLocation::find()->where(['OperatorID' => $this->operatorID, 'LocationID' => $location->ID])->limit(1)->one();
        
//        echo '<pre>';
//        print_r($operatorLocation);
//        exit;
        
        if (empty($operatorLocation->OperatorLocationID))
        {
            return [];
        }
        
        $xml = '<?xml version="1.0" encoding="UTF-8"?>
            <availabilityRQ xmlns="http://www.hotelbeds.com/schemas/messages" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" dailyRate="false">
               <stay checkIn="' . $checkIn . '" checkOut="' . $checkOut . '" />
               ' . $occupancies . '
               <destination code="' . $operatorLocation->OperatorLocationID . '" />
               <filter packaging="true" maxRooms="' . $data['Rooms'] . '" />
            </availabilityRQ>
        ';
        
        $signature = hash("sha256", $this->hApiKey . $this->hApiSecret . time());
        
        $endpoint = "https://api.test.hotelbeds.com/hotel-api/1.2/hotels";
        
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('post')
            ->setUrl($endpoint)
            ->setHeaders([
                "Api-Key"      => $this->hApiKey,
                "X-Signature"  => $signature,
                "Content-Type" => "application/xml",
            ])
            ->setOptions([
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            ])
            ->setContent($xml)
            ->send();
        
        $result = [];
        
        if ($response->isOk && !empty($response->data['hotels']['hotels']))
        {
            $hotelCodes = [];
            foreach ($response->data['hotels']['hotels'] as $hotel)
            {
                $hotelCodes[$hotel['code']] = $hotel['code'];
                
                $roomInfo = [];
                foreach ($hotel['rooms'] as $room)
                {
                    $roomInfo[] = $room['name'];
                }
                
                $prices = [];
                if (!empty($hotel['rooms']))
                {
                    foreach ($hotel['rooms'] as $room)
                    {
//                        if (count($room['rates']) != $data['Rooms'])
//                        {
//                            continue;
//                        }
                        
                        foreach ($room['rates'] as $rate)
                        {
                            $cancellationPolicies = [];
                            foreach ($rate['cancellationPolicies'] as $cp)
                            {
                                $cancellationPolicies[] = [
                                    'from' => date('d.m.Y H:i:s', strtotime($cp['from'])),
                                    'amount' => $cp['amount'],
                                ];
                            }
                            
                            $rateKeyData = explode('|', $rate['rateKey']);
                            $rateKeyData['rateKey'] = $rate['rateKey'];
                            $rateKeyData['boardCode'] = $rate['boardName'];
                            $rateKeyData['boardName'] = $rate['boardName'];
                            $rateKeyData['paymentType'] = $rate['paymentType'];
                            $rateKeyData['price'] = $rate['net'];
                            $rateKeyData['roomCode'] = $room['code'];
                            $rateKeyData['roomName'] = $room['name'];
                            $rateKeyData['roomCount'] = $data['Rooms'];
                            
                            $prices[] = [
                                'rateKey' => $rate['rateKey'],
                                'rateKeyData' => $rateKeyData,
                                'code' => $room['code'],
                                'name' => $room['name'],
                                'rooms' => $rate['rooms'],
                                'adults' => $rate['adults'],
                                'rateType' => $rate['rateType'],
                                'children' => $rate['children'],
                                'boardCode' => $rate['boardCode'],
                                'boardName' => $rate['boardName'],
                                'paymentType' => $rate['paymentType'],
                                'price' => $rate['net'],
                                'cancellationPolicies' => $cancellationPolicies,
                                'offers' => $rate['offers'],
                            ];
                        }
                    }
                }
                
//                if (count($prices) == 0)
//                {
//                    unset($hotelCodes[$hotel['code']]);
//                    continue;
//                }
                
                $result[$this->operatorID . '-' . $hotel['code']] = [
                    'ExternalID' => $hotel['code'],
                    'OperatorID' => 2,
                    'hash' => $hash,
                    'Image' => '',
                    'Stars' => $hotel['categoryName'],
                    'Name' => $hotel['name'],
                    'Address' => $hotel['zoneName'],
                    'Latitude' => $hotel['latitude'],
                    'Longitude' => $hotel['longitude'],
                    'MinPrice' => $hotel['minRate'],
                    'Currency' => $hotel['currency'],
                    'RoomInfo' => implode(', ', $roomInfo),
                    'Url' => Url::to(['/hotel/' . $this->operatorID . '-' . $hotel['code'] . '/' . $hash]),
                    'Prices' => $prices,
                    'OccupanciesData' => $occupanciesData,
                ];
            }
            
            $signature = hash("sha256", $this->hApiKey . $this->hApiSecret . time());

            $endpoint = "https://api.test.hotelbeds.com/hotel-content-api/1.0/hotels?fields=all&codes=" . implode(',', $hotelCodes) . "&from=1&to=999";

            $client = new Client();
            $response = $client->createRequest()
                ->setMethod('get')
                ->setUrl($endpoint)
                ->setHeaders([
                    "Api-Key"      => $this->hApiKey,
                    "X-Signature"  => $signature,
                    "Content-Type" => "application/xml",
                ])
                ->setOptions([
                    'ssl' => array(
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                        'allow_self_signed' => true
                    )
                ])
                ->setContent($xml)
                ->send();
            
            if ($response->isOk)
            {
                foreach ($response->data['hotels'] as $hotel)
                {
                    $result[$this->operatorID . '-' . $hotel['code']]['Image'] = isset($hotel['images'][0]['path']) ? 'https://photos.hotelbeds.com/giata/' . $hotel['images'][0]['path'] : '';
                
                    $phones = [];
                    if (!empty($hotel['phones']))
                    {
                        foreach ($hotel['phones'] as $phone)
                        {
                            $phones[] = [
                                'phoneType' => $phone['phoneType'],
                                'phoneNumber' => $phone['phoneNumber'],
                            ];
                        }
                    }
                    $result[$this->operatorID . '-' . $hotel['code']]['Phones'] = $phones;

                    $rooms = [];
                    if (!empty($hotel['rooms']))
                    {
                        foreach ($hotel['rooms'] as $room)
                        {
                            $rooms[] = [
                                'roomCode' => $room['roomCode'],
                                'roomType' => $room['roomType'],
                                'characteristicCode' => $room['characteristicCode'],
                                'roomStays' => $room['roomStays']
                            ];
                        }
                    }
                    $result[$this->operatorID . '-' . $hotel['code']]['Rooms'] = $rooms;
                    
                    $images = [];
                    if (!empty($hotel['images']))
                    {
                        foreach ($hotel['images'] as $image)
                        {
                            if (in_array($image['imageTypeCode'], ['GEN', 'RES', 'DEP', 'COM', 'TER', 'PIS']))
                            {
                                $images[] = [
                                    'path' => 'https://photos.hotelbeds.com/giata/' . $image['path'],
                                ];
                            }
                        }
                    }
                    
                    $result[$this->operatorID . '-' . $hotel['code']]['Images'] = $images;
                    
                    $result[$this->operatorID . '-' . $hotel['code']]['Email'] = $hotel['email'];
                    $result[$this->operatorID . '-' . $hotel['code']]['Description'] = $hotel['description']['content'];
                }
                
                $results = [];
                foreach ($result as $key => $row)
                {
                    $hotelClass = new Hotel();
                    $hotelClass->ExternalID = $row['ExternalID'];
                    $hotelClass->OperatorID = $row['OperatorID'];
                    $hotelClass->Hash = $hash;
                    $hotelClass->MainImage = RemoteImage::getImageUrl(isset($row['Images'][0]['path']) ? $row['Images'][0]['path'] : '');
                    $hotelClass->Stars = RattingConverter::convert($row['Stars']);
                    $hotelClass->Name = $row['Name'];
                    $hotelClass->Address = $row['Address'];
                    $hotelClass->Latitude = $row['Latitude'];
                    $hotelClass->Longitude = $row['Longitude'];
                    $hotelClass->MinPrice = $row['MinPrice'];
                    $hotelClass->Currency = $row['Currency'];
                    $hotelClass->RoomInfo = $row['RoomInfo'];
                    $hotelClass->Email = $row['Email'];
                    $hotelClass->Description  = $row['Description'];
                    
                    $hotelClass->Images = [];
                    foreach ($row['Images'] as $img)
                    {
                        $hotelClass->Images[] = RemoteImage::getImageUrl($img['path']);
                    }
                    
                    $hotelClass->Rooms = [];
                    foreach ($row['Prices'] as $price)
                    {
                        $hotelRoomClass = new HotelRoom();
                        $hotelRoomClass->Name = $price['name'];
                        $hotelRoomClass->Boards = [];
                        
                        $hotelClass->Rooms[$price['code']] = $hotelRoomClass;
                    }
                    
                    foreach ($row['Prices'] as $price)
                    {
                        $roomBoardPriceClass = new RoomBoardPrice();
                        $roomBoardPriceClass->Currency = 'EUR';
                        $roomBoardPriceClass->IsSuperDeal = false;
                        $roomBoardPriceClass->Price = $price['price'];
                        $roomBoardPriceClass->RateKeyData = $price['rateKeyData'];
                        $roomBoardPriceClass->CancellationPolicies = [];
                        
                        $discount = 0;
                        if (!empty($price['offers']) && count($price['offers']) > 0)
                        {
                            foreach ($price['offers'] as $offrer)
                            {
                                $discount += ($offrer['amount'] * -1);
                            }
                        }
                        
                        $roomBoardPriceClass->PriceOld = $discount == 0 ? 0 : round($price['price'] + $discount, 2);
                        
                        $roomBoardPriceClass->DiscountPercent = $discount == 0 ? 0 : floor($discount / $roomBoardPriceClass->PriceOld * 100);
                        
                        foreach ($price['cancellationPolicies'] as $cp)
                        {
                            $policyClass = new CancellationPolicy();
                            $policyClass->Percent = empty($cp['amount']) ? 0 : round($cp['amount'] / $roomBoardPriceClass->Price * 100, 2);
                            $policyClass->Date = date('d.m.Y H:i:s', strtotime($cp['from']));
                            $policyClass->Description = $policyClass->Percent . '% until ' . $policyClass->Date;
                            
                            $roomBoardPriceClass->CancellationPolicies[] = $policyClass;
                        }
                        
                        $roomBoardClass = new RoomBoard();
                        $roomBoardClass->Name = $price['boardName'];
                        $roomBoardClass->RoomBoardPrices[] = $roomBoardPriceClass;
                        
                        $hotelClass->Rooms[$price['code']]->Boards[] = $roomBoardClass;
                    }
                    
                    $results[$key] = $hotelClass;
                }
                
                return $results;
            }
        }
        else
        {
            \Yii::warning('Hotelbeds developer over rate', 'Parser');
            return [];
        }
        
        return $result;
    }
    
    public function startPayment($bookingModel, $cardModel = null)
    {
        return true;
        
        $signature = hash("sha256", $this->hApiKey . $this->hApiSecret . time());
        
        $searchData = \Yii::$app->session->get('searchData');
        $orderData = \Yii::$app->session->get('orderData');
        
        $rooms = '';
        for ($i = 1; $i <= $searchData['Rooms']; $i++)
        {
            $paxes = '<paxes>';
            for ($j = 1; $j <= $searchData['Adults'][$i]; $j++)
            {
                $paxes .= '<pax roomId="' . $i . '" type="AD" name="Adult Name" surname="Adult Surname"></pax>';
            }
            for ($k = 1; $k <= $searchData['Childs'][$i]; $k++)
            {
                if (empty($searchData['Childs'][$i])) continue;
                
                $paxes .= '<pax roomId="' . $i . '" type="CH" age="' . $searchData['ChildAge'][$i][$k] . '"></pax>';
            }
            $paxes .= '</paxes>';
            
            $rooms .= '<room roomId="' . $i . '" rateKey="' . $bookingModel->RateKey . '">' . $paxes . '</room>';
        }
        
        $xml = '<?xml version="1.0" encoding="UTF-8"?>
                <bookingRQ xmlns="http://www.hotelbeds.com/schemas/messages" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                    <holder name="' . $bookingModel->bookingClient->FirstName . '" surname="' . $bookingModel->bookingClient->LastName . '"/>
                    <rooms>
                        ' . $rooms . '
                    </rooms>
                    <paymentData>
                        <paymentCard>
                            <cardHolderName>' . $cardModel->HolderName . '</cardHolderName>
                            <cardType>VI</cardType>
                            <cardNumber>' . $cardModel->Number . '</cardNumber>
                            <expiryDate>' . $cardModel->ExpirationDate . '</expiryDate>
                            <cardCVC>' . $cardModel->CVV . '</cardCVC>
                        </paymentCard>
                        <contactData>
                            <email>' . $bookingModel->bookingClient->Email . '</email>
                            <phoneNumber>' . $bookingModel->bookingClient->Phone . '</phoneNumber>
                        </contactData>
                    </paymentData>
                    <clientReference>Hotel reservation</clientReference>
                </bookingRQ>';
        
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('post')
            ->setUrl('https://api-secure.test.hotelbeds.com/hotel-api/1.0/bookings')
            ->setHeaders([
                "Api-Key"      => $this->hApiKey,
                "X-Signature"  => $signature,
                "Content-Type" => "application/xml",
            ])
            ->setContent($xml)
            ->send();
        
        if ($response->isOk)
        {
            echo '<pre>';
            print_r($response->data);
            exit;
        }
        
        echo '<pre>';
        print_r($response);
        exit;
    }
    
}