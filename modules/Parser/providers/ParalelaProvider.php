<?php

namespace app\modules\Parser\providers;

use app\modules\Parser\providers\Provider;
use yii\httpclient\Client;
use app\modules\Location\models\Location;
use app\modules\Location\models\LocationLang;
use app\modules\Operator\Operator\models\OperatorLocation;


class ParalelaProvider extends Provider
{
    
    public $operatorID = 7;
    
    public $login = 'XML_DIALECT';
    public $password = 'fcv34t453reg';
    
    public $endpoint = 'http://rezervari.paralela45.ro/server_xml/server.php';
    
    public function parse()
    {
        $this->getLocations();
    }

    public function getLocations()
    {
        set_time_limit(0);
        ignore_user_abort(true);
        
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('post')
            ->setHeaders([
                'Content-Type' => 'text/xml;charset=UTF-8',
            ])
            ->setUrl($this->endpoint)
            ->setContent('<?xml version="1.0" encoding="UTF-8"?> 
                <Request RequestType="getCountryRequest">
                    <AuditInfo>
                        <RequestId>001</RequestId>
                        <RequestUser>' . $this->login . '</RequestUser>
                        <RequestPass>' . $this->password . '</RequestPass>
                        <RequestTime>' . date('Y-m-d\TH:i:s') . '</RequestTime>
                        <RequestLang>EN</RequestLang>
                    </AuditInfo>
                    <RequestDetails>
                        <getCountryRequest/>
                    </RequestDetails>
                </Request> 
            ')
            ->send();
                
        $emptyCodes = [];
        $emptyCountries = [];
        
        if ($response->isOk)
        {
            OperatorLocation::deleteAll(['OperatorID' => $this->operatorID]);
            
            foreach ($response->data['ResponseDetails']['getCountryResponse']['Country'] as $country)
            {
                if (empty($country['CountryCode']) || empty($country['CountryName']))
                {
                    $emptyCodes[] = $country;
                    continue;
                }
                
                $code = $country['CountryCode'];
                $name = $country['CountryName'];
                
                $country = Location::find()->joinWith('lang')->where(['Name' => $name, 'Type' => Location::TypeCountry])->one();
                
                if (empty($country->ID))
                {
                    $country = new Location();
                    $country->Type = Location::TypeCountry;
                    $country->Latitude = 0;
                    $country->Longitude = 0;
                    $country->Code = $code;
                    $country->save(false);

                    $regionLang = new LocationLang();
                    $regionLang->LangID = 'ro';
                    $regionLang->LocationID = $country->ID;
                    $regionLang->Name = $name;
                    $regionLang->save(false);
                }
                
                $operatorLocation = new OperatorLocation();
                $operatorLocation->OperatorID = $this->operatorID;
                $operatorLocation->LocationID = $country->ID;
                $operatorLocation->OperatorLocationID = $code;
                $operatorLocation->save(false);
                
                $client = new Client();
                $response = $client->createRequest()
                    ->setMethod('post')
                    ->setHeaders([
                        'Content-Type' => 'text/xml;charset=UTF-8',
                    ])
                    ->setUrl($this->endpoint)
                    ->setContent('<?xml version="1.0" encoding="UTF-8"?> 
                        <Request RequestType="getCityRequest">
                            <AuditInfo>
                                <RequestId>001</RequestId>
                                <RequestUser>' . $this->login . '</RequestUser>
                                <RequestPass>' . $this->password . '</RequestPass>
                                <RequestTime>' . date('Y-m-d\TH:i:s') . '</RequestTime>
                                <RequestLang>EN</RequestLang>
                            </AuditInfo>
                            <RequestDetails>
                                <getCityRequest CountryCode="' . $country->Code . '" />
                            </RequestDetails>
                        </Request> 
                    ')
                    ->send();
                
                if (empty($response->data['ResponseDetails']['getCityResponse']['City']))
                {
                    $emptyCountries[] = $country;
                    continue;
                }
                
                foreach ($response->data['ResponseDetails']['getCityResponse']['City'] as $cityItem)
                {
                    if (empty($cityItem['CityCode']) || empty($cityItem['CityName']))
                    {
                        $emptyCodes[] = $cityItem;
                        continue;
                    }
                    
                    $cityCode = $cityItem['CityCode'];
                    $cityName = $cityItem['CityName'];
                    
                    $city = Location::find()->joinWith('lang')->where(['Name' => $cityName])->limit(1)->one();
                        
                    if (empty($city->ID))
                    {
                        $city = new Location();
                        $city->Type = Location::TypeCity;
                        $city->CountryID = $country->ID;
                        $city->ParentID = $country->ID;
                        $city->Latitude = 0;
                        $city->Longitude = 0;
                        $city->Code = $cityCode;
                        $city->save(false);

                        $cityLang = new LocationLang();
                        $cityLang->LangID = 'ro';
                        $cityLang->LocationID = $city->ID;
                        $cityLang->Name = $cityName;
                        $cityLang->save(false);
                    }

                    $operatorLocation = new OperatorLocation();
                    $operatorLocation->OperatorID = $this->operatorID;
                    $operatorLocation->LocationID = $city->ID;
                    $operatorLocation->OperatorLocationID = $cityCode;
                    $operatorLocation->save(false);
                }
            }
            
            echo '<pre>';
            print_r($emptyCountries);
            print_r($emptyCodes);
            exit;
        }
        else
        {
            echo '<pre>';
            print_r($response);
            exit;
        }
    }
    
    public function searchHotels($data, $hash)
    {
        $location = Location::find()->joinWith('operatorLocations')->where(['Location.ID' => $data['LocationID']])->one();
        
        $country = $location->country->operatorLocations[$this->operatorID];
        
        if (empty($location->operatorLocations[$this->operatorID]) || empty($country->ID))
        {
            return [];
        }
        
        $location = $location->operatorLocations[$this->operatorID];
        
        $checkIn = date('Y-m-d', strtotime($data['CheckIn']));
        $nights = (int)$data['Nights'];
        $checkOut = date('Y-m-d', strtotime('+' . $nights . ' day' . ($nights > 1 ? 's' : ''), strtotime($data['CheckIn'])));
        
        $rooms = '';
        for ($i = 1; $i <= $data['Rooms']; $i++)
        {
            $rooms .= '<Room NoAdults="' . $data['Adults'][$i] . '" ' . (!empty($data['Childs'][$i]) ? 'NoChildren="' . $data['Childs'][$i] . '"' : '') . '>';
            
            if (!empty($data['Childs'][$i]))
            {
                $rooms .= '<Children>';
                for ($j = 1; $j <= $data['Childs'][$i]; $j++)
                {
                    $rooms .= '<Age>' . $data['ChildAge'][$i][$j] . '</Age>';
                }
                $rooms .= '</Children>';
            }
            
            $rooms .= '</Room>';
        }
        
        $xml = '<?xml version="1.0" encoding="UTF-8"?>
                <Request RequestType="getHotelPriceRequest">
                    <AuditInfo>
                        <RequestId>001</RequestId>
                        <RequestUser>' . $this->login . '</RequestUser>
                        <RequestPass>' . $this->password . '</RequestPass>
                        <RequestTime>' . date('Y-m-d\TH:i:s') . '</RequestTime>
                        <RequestLang>EN</RequestLang>
                    </AuditInfo>
                    <RequestDetails>
                        <getHotelPriceRequest>
                            <CountryCode>' . $country->OperatorLocationID . '</CountryCode>
                            <CityCode>' . $location->OperatorLocationID . '</CityCode>
                            <CurrencyCode>EUR</CurrencyCode>
                            <PeriodOfStay>
                                <CheckIn>' . $checkIn . '</CheckIn>
                                <CheckOut>' . $checkOut . '</CheckOut>
                            </PeriodOfStay>
                            <Rooms>
                                ' . $rooms . '
                            </Rooms>     
                        </getHotelPriceRequest>
                    </RequestDetails>
                </Request>';
        
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('post')
            ->setHeaders([
                'Content-Type' => 'text/xml;charset=UTF-8',
            ])
            ->setUrl($this->endpoint)
            ->setContent($xml)
            ->send();
        
        if ($response->isOk)
        {
            echo '<pre>';
            print_r($response->data);
            exit;
        }
        else
        {
            echo '<pre>';
            print_r($response);
            exit;
        }
    }
    
    public function detectRoom($data)
    {
        return [
            'DB' => [
                
            ],
        ];
    }
    
}