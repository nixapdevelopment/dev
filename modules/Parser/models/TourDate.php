<?php

namespace app\modules\Parser\models;

use Yii;
use app\modules\Parser\models\Tour;

/**
 * This is the model class for table "TourDate".
 *
 * @property integer $ID
 * @property integer $TourID
 * @property string $Date
 *
 * @property Tour $tour
 */
class TourDate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'TourDate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['TourID', 'Date'], 'required'],
            [['TourID'], 'integer'],
            [['Date'], 'safe'],
            [['TourID'], 'exist', 'skipOnError' => true, 'targetClass' => Tour::className(), 'targetAttribute' => ['TourID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'TourID' => Yii::t('app', 'Tour ID'),
            'Date' => Yii::t('app', 'Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTour()
    {
        return $this->hasOne(Tour::className(), ['ID' => 'TourID']);
    }
}
