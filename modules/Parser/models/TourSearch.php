<?php

namespace app\modules\Parser\models;

use app\modules\Parser\models\Tour;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "Tour".
 *
 * @property integer $ID
 * @property integer $ResourceID
 * @property integer $ExternalID
 * @property string $Title
 * @property string $Subtitle
 * @property string $Link
 * @property string $Price
 * @property string $Currency
 * @property integer $Days
 * @property integer $IncludeTaxes
 * @property integer $Airplane
 * @property integer $Bus
 * @property integer $Ship
 * @property string $Description
 * @property string $HotelInfo
 * @property string $Itinerary
 * @property string $UsefullInfo
 * @property string $OptionalTrips
 * @property string $Images
 *
 * @property TourDate[] $tourDates
 * @property TourLocation[] $tourLocations
 */
class TourSearch extends Tour
{
    public $location = null;
    public $TourDate = null;
    public $PriceFrom = null;
    public $PriceTo = null;


    public function rules()
    {
        return [

            [['location','TourDate','PriceFrom','PriceTo'],'safe'],

        ];
    }

    public function search($params)
    {
        $this->load($params[0]);

        $query = Tour::find()->joinWith(['tourDates','tourLocations'])->where([$params['type'] => 1])->groupBy('ID');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if ($this->location){
            //$query -> orFilterWhere(['like','TourLocation.Country', $this->location]);
            $query -> andFilterWhere(['or',
                ['like','TourLocation.Country', $this->location],
                ['like', 'TourLocation.Location', $this->location]
            ]);
        }

        if ($this->PriceFrom){

            $query -> andFilterWhere(['>=', 'Tour.Price', $this->PriceFrom]);
        }
        if ($this->PriceTo){

            $query -> andFilterWhere(['<=', 'Tour.Price', $this->PriceTo]);
        }
        if($this->TourDate){
            $time = strtotime($this->TourDate);
            $from = strtotime('-3 days', $time);
            $to = strtotime('+3 days', $time);
            $query->andFilterWhere(['between', 'TourDate.Date', date('c', $from), date('c', $to)]);

        }
        if (!$this->validate()) {
            return $dataProvider;
        }
        return $dataProvider;

    }
    
    
}
