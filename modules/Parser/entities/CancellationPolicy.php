<?php

namespace app\modules\Parser\entities;

class CancellationPolicy
{
    
    /**
     * Cancellation loss amount (use amount or percent)
     * @var float
     */
    public $Amount;
    
    /**
     * Cancellation loss percent (use amount or percent)
     * @var float
     */
    public $Percent;
    
    /**
     * Cancellation date limit - string date "d.m.Y H:i:s" in local hotel time
     * @var string
     */
    public $Date;
    
    /**
     * String description of cancellation
     * @var string
     */
    public $Description;
    
}