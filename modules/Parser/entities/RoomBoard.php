<?php

namespace app\modules\Parser\entities;


class RoomBoard
{
    
    /**
     * Room board variants (room only, bed and breakfast, half board, etc...)
     * @var string
     */
    public $Name;
    
    /**
     * Room board prices - array of RoomBoardPrice classes
     * @var RoomBoardPrice[]
     */
    public $RoomBoardPrices;
    
}