<?php

namespace app\modules\Parser\entities;

class Hotel
{
    /**
     * Operator hotel ID
     * @var intiger
     */
    public $ExternalID;
    
    /**
     * Operator ID
     * @var integer
     */
    public $OperatorID;
    
    /**
     * Search data hash
     * @var string
     */
    public $Hash;
    
    /**
     * Main hotel image url
     * @var string
     */
    public $MainImage;
    
    /**
     * Hotel stars - from 0 to 5
     * @var integer
     */
    public $Stars;
    
    /**
     * Hotel name
     * @var string
     */
    public $Name;
    
    /**
     * Hotel address
     * @var string
     */
    public $Address;
    
    /**
     * Hotel Latitude
     * @var float
     */
    public $Latitude;
    
    /**
     * Hotel Longitude
     * @var float
     */
    public $Longitude;
    
    /**
     * Hotel MinPrice
     * @var float
     */
    public $MinPrice;
    
    /**
     * Hotel Currency (ISO)
     * @var string
     */
    public $Currency;
    
    /**
     * Hotel RoomInfo - list all avalible room types separated by "," 
     * @var string
     */
    public $RoomInfo;
    
    /**
     * Hotel Email - non required
     * @var string
     */
    public $Email;
    
    /**
     * Hotel Images - array of hotel images full paths
     * @var array
     */
    public $Images = [];
    
    /**
     * Hotel Room types - list of classes HotelRoom
     * @var HotelRoom[] 
     */
    public $Rooms = [];
    
    /**
     * Hotel Is Best
     * @var bool
     */
    public $IsBest = false;
    
    /**
     * Hotel description
     * @var string
     */
    public $Description;
    
    /**
     * Hotel reviews - Array of classes HotelReview
     * @var HotelReview[]
     */
    public $Reviews = [];
    
    /**
     * Return hotel main image
     * @return string the hotel main image full path
     */
    public function getMainImage()
    {
        $mainImage = reset($this->Images);
        return empty($mainImage) ? 'https://www.mayflex.com/sites/default/files/no-image-box.png' : $mainImage;
    }
    
    /**
     * Return hotel min price
     * @return float the hotel hotel min price
     */
    public function getMinPrice()
    {
        $minPrice = 0;
        
        $rooms = $this->Rooms;
        foreach ($rooms as $room)
        {
            foreach ($room->Boards as $board)
            {
                foreach ($board->RoomBoardPrices as $roomBoardPrice)
                {
                    $minPrice = $minPrice == 0 || $minPrice > $roomBoardPrice->Price ? $roomBoardPrice->Price : $minPrice;
                }
            }
        }
        
        return $minPrice;
    }
    
}