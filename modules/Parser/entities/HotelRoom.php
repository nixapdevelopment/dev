<?php

namespace app\modules\Parser\entities;

class HotelRoom
{
    
    /**
     * Hotel room name (room type: single, double, etc...)
     * @var string
     */
    public $Name;
    
    /**
     * Room board - array of RoomBoard classes
     * @var RoomBoard[]
     */
    public $Boards = [];
    
}