<?php


namespace app\modules\Parser\components;


class RoomNameConverter
{
    
    private static $_rooms = [
        'sgl' => 'Single room',
        'tsu' => 'Twin for sole use',
        'twn' => 'Twin room',
        'dbl' => 'Double room',
        'trp' => 'Triple room',
        'qud' => 'Quadruple room',
        'fam' => 'Family Room (2+2)',
    ];

    public static function convert($operatorRoomCode)
    {
        if (!isset(self::$_rooms[$operatorRoomCode]))
        {
            return $operatorRoomCode;
        }
        
        return isset(self::$_rooms[$operatorRoomCode]) ? self::$_rooms[$operatorRoomCode] : 'Standart room';
    }
    
}
