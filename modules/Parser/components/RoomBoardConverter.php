<?php

namespace app\modules\Parser\components;


class RoomBoardConverter
{
    
    private static $_boards = [
        'RO' => 'No meal',
        'RB' => 'Overnight and breakfast',
        'RL' => 'Overnight and lunch',
        'RD' => 'Overnight and dinner',
        'FB' => 'Full board',
    ];

    public static function convert($operatorBoardCode)
    {
        return isset(self::$_boards[$operatorBoardCode]) ? self::$_boards[$operatorBoardCode] : 'No meal';
    }
    
}
