<?php

namespace app\modules\Parser\components;


class RattingConverter
{
    
    private static $_rates = [
        '3EST' => 3,
        '4EST' => 4,
        'SUP' => 4,
        '2EST' => 2,
        '1EST' => 1,
        'HS' => 1,
        'ALBER' => 0,
        'HS2' => 2,
        '5LUX' => 5,
        'AT1' => 0,
        '5EST' => 5,
        '4LL' => 4,
        'APTH4' => 4,
        'APTH' => 1,
        'SPC' => 0,
        'H3_5' => 3,
        'APTH3' => 3,
        '2LL' => 2,
        '3LL' => 3,
        'VTV' => 0,
        '5 STARS' => 5,
        '4 STARS' => 4,
        '3 STARS' => 3,
        '2 STARS' => 2,
        '1 STARS' => 1,
        '1 STAR' => 1,
        'SUPERIOR 4*' => 4,
        'HOSTEL 2*' => 2,
        'HOSTEL 1*' => 1,
        'HOSTEL' => 0,
        '4 STARS LUXURY' => 4,
        'APARTMENT 1ST CATEGORY' => 1,
        '5 KEYS' => 5,
        '4 KEYS' => 4,
        '3 KEYS' => 3,
        '2 KEYS' => 2,
        '1 KEYS' => 1,
        '5 STARS LUXURY' => 5,
        'APARTHOTEL 4*' => 4,
        'APARTHOTEL 3*' => 3,
        'APARTHOTEL 1*' => 1,
        'APARTMENT 3RD CATEGORY' => 3,
        'APARTHOTEL 3*' => 3,
        '3 STARS AND A HALF' => 3,
    ];

    public static function convert($operatorRate)
    {
        return isset(self::$_rates[$operatorRate]) ? self::$_rates[$operatorRate] : 0;
    }
    
}