<?php

namespace app\modules\Parser\controllers;

use Yii;
use yii\web\Controller;
use yii\httpclient\Client;
use app\modules\Location\models\Location;
use app\modules\Location\models\LocationLang;
use app\modules\Hotel\models\Hotel;
use app\modules\Hotel\models\HotelLang;

class PrestigeController extends Controller
{
    
    public $cityStack = [];
    
    public function actionIndex()
    {
        $this->parseCities();
        $this->parseHotels();
    }
    
    public function parseCities()
    {
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('get')
            ->setUrl("http://online2.calypsotour.com/export/default.php?samo_action=api&version=1.0&oauth_token=e3d295d44ade456d8a1bbd8d245c2c79&type=xml&action=SearchTour_TOWNS")
            ->send();
        
        if ($response->isOk)
        {
            $turkey = Location::find()->where(['Code' => 'TR'])->one();
            
            foreach ($response->data['items']['item'] as $item)
            {
                $city = Location::find()->joinWith('lang')->where(['Name' => $item['name'], 'Type' => Location::TypeCity])->one();
                $region = Location::find()->joinWith('lang')->where(['Name' => $item['region'], 'Type' => Location::TypeRegion])->one();
                
                if (!$region)
                {
                    $region = new Location();
                    $region->ParentID = $turkey->ID;
                    $region->CountryID = $turkey->ID;
                    $region->Type = Location::TypeRegion;
                    $region->Latitude = 0;
                    $region->Longitude = 0;
                    $region->save(false);
                    
                    $regionLang = new LocationLang();
                    $regionLang->LangID = 'ro';
                    $regionLang->LocationID = $region->ID;
                    $regionLang->Name = $item['region'];
                    $regionLang->save(false);
                }
                
                if (!$city)
                {
                    $city = new Location();
                    $city->ParentID = $region->ID;
                    $city->CountryID = $turkey->ID;
                    $city->Type = Location::TypeCity;
                    $city->Latitude = 0;
                    $city->Longitude = 0;
                    $city->save(false);
                    
                    $cityLang = new LocationLang();
                    $cityLang->LangID = 'ro';
                    $cityLang->LocationID = $city->ID;
                    $cityLang->Name = $item['name'];
                    $cityLang->save(false);
                }
                
                $this->cityStack[$item['id']] = [
                    'city' => $city,
                    'region' => $region,
                ];
            }
        }
    }
    
    public function parseHotels()
    {
        $turkey = Location::find()->where(['Code' => 'TR'])->one();
        
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('get')
            ->setUrl("http://online2.calypsotour.com/export/default.php?samo_action=api&version=1.0&oauth_token=e3d295d44ade456d8a1bbd8d245c2c79&type=xml&action=SearchTour_HOTELS")
            ->send();
        
        if ($response->isOk)
        {
            foreach ($response->data['items']['item'] as $hotel)
            {
                if (isset($this->cityStack[$hotel['townKey']]))
                {
                    $hotelModel = new Hotel();
                    $hotelModel->LocationID = $this->cityStack[$hotel['townKey']]['city']->ID;
                    $hotelModel->CountryID = $turkey->ID;
                    $hotelModel->RegionID = $this->cityStack[$hotel['townKey']]['region']->ID;
                    $hotelModel->Stars = (int)$hotel['starKey'];
                    $hotelModel->save(false);
                    
                    $hotelLang = new HotelLang();
                    $hotelLang->HotelID = $hotelModel->ID;
                    $hotelLang->LangID = 'ro';
                    $hotelLang->Name = $hotel['name'];
                    $hotelLang->Description = ' ';
                    $hotelLang->Address = ' ';
                    $hotelLang->save(false);
                }
            }
        }
    }
    
}
