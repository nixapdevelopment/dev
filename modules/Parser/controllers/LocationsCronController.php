<?php

namespace app\modules\Parser\controllers;

use app\modules\Parser\providers\PrestigeProvider;
use app\modules\Parser\providers\HotelbedsProvider;

class LocationsCronController extends CronController
{
    
    public function actionIndex()
    {
        //$prestigeProvider = new PrestigeProvider();
        //$prestigeProvider->getLocations();
        
        $hotelbetsProvider = new HotelbedsProvider();
        $hotelbetsProvider->getLocations();
    }
    
}
