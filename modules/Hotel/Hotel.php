<?php

namespace app\modules\Hotel;

use app\components\Module\SiteModule;

/**
 * Hotel module definition class
 */
class Hotel extends SiteModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Hotel\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
