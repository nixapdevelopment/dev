<?php

namespace app\modules\Hotel\controllers;

use yii\web\Controller;

/**
 * Default controller for the `Hotel` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
