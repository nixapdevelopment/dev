<?php

namespace app\modules\Hotel\models;

use Yii;
use app\components\DataProvider\CustomActiveDataProvider;
use app\modules\Hotel\models\Hotel;
use app\modules\Hotel\models\HotelSearch;

use yii\db\Expression;

/**
 * HotelSearch represents the model behind the search form about `app\modules\Hotel\models\Hotel`.
 */
class HotelFrontSearch extends HotelSearch
{
    public $countryName = null;
    public $HotelName = null;
    public $FiveStars = null;
    public $FourStars = null;
    public $ThreeStars = null;
    public $TwoStars = null;
    public $OneStar = null;

    public function rules()
    {
        return [

            [['CountryName','HotelName','FiveStars','FourStars','ThreeStars','TwoStars','OneStar'],'safe'],


        ];
    }

    public function search($params)
    {
        $this->load($params);

        $expression1 = new Expression("
            (select LocationLang.Name from Location
                left join LocationLang on LocationLang.LocationID = Location.ID and LocationLang.LangID = '" . Yii::$app->language . "'
                where Location.ID = Hotel.CountryID
            ) as CountryName
        ");
        $expression2 = new Expression("
            (select LocationLang.Name from Location
                left join LocationLang on LocationLang.LocationID = Location.ID and LocationLang.LangID = '" . Yii::$app->language . "'
                where Location.ID = Hotel.RegionID
            ) as RegionName
        ");
        $expression3 = new Expression("
            (select LocationLang.Name from Location
                left join LocationLang on LocationLang.LocationID = Location.ID and LocationLang.LangID = '" . Yii::$app->language . "'
                where Location.ID = Hotel.LocationID
            ) as LocationName
        ");

        $query = Hotel::find()->addSelect(["*", $expression1, $expression2, $expression3, 'Hotel.ID as ID'])->joinWith('lang')->with(['country.lang','region.lang','location.lang','mainImage']);

        $dataProvider = new CustomActiveDataProvider([
            'query' => $query,
        ]);
        
        if ($this->CountryName ){

            $query -> orFilterHaving(
               ['like','CountryName', $this->CountryName]
            );
            $query -> orFilterHaving(
                ['like','RegionName',$this->CountryName]
            );
            $query -> orFilterHaving(
                ['like','LocationName', $this->CountryName]
            );
            $query -> orFilterHaving(
                ['like','HotelLang.Name', $this->CountryName]
            );

        }
        if ($this->HotelName){

            $query -> orFilterHaving(
                ['like','HotelLang.Name', $this->HotelName]
            );
        }

        if ($this->FiveStars){
            $this->FiveStars = 5;
            $query->orFilterHaving(['=', 'Stars', $this->FiveStars]);
        }
        if ($this->FourStars){
            $this->FourStars = 4;
            $query->orFilterHaving(['=', 'Stars', $this->FourStars]);
        }
        if ($this->ThreeStars){
            $this->ThreeStars = 3;
            $query->orFilterHaving(['=', 'Stars', $this->ThreeStars]);
        }
        if ($this->TwoStars){
            $this->TwoStars = 2;
            $query->orFilterHaving(['=', 'Stars', $this->TwoStars]);
        }
        if ($this->OneStar){
            $this->OneStar = 1;
            $query->orFilterHaving(['=', 'Stars', $this->OneStar]);
        }

        if (!$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;

    }
}
