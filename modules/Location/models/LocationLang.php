<?php

namespace app\modules\Location\models;

use phpDocumentor\Reflection\Type;
use Yii;

/**
 * This is the model class for table "LocationLang".
 *
 * @property integer $ID
 * @property integer $LocationID
 * @property integer $LangID
 * @property string $Name
 * @property string $Description
 * @property string $Longitude
 * @property string $Latitude
 *
 * @property Location $location
 */
class LocationLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */



    public static function tableName()
    {
        return 'LocationLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['LocationID', 'LangID', 'Name', 'Description'], 'required'],
            [['LocationID'], 'integer'],
            [['LangID'], 'string', 'max' => 2],
            [['Description'], 'string'],
            [['Name'], 'string', 'max' => 255],
            [['LocationID'], 'exist', 'skipOnError' => true, 'targetClass' => Location::className(), 'targetAttribute' => ['LocationID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'LocationID' => Yii::t('app', 'Location ID'),
            'LangID' => Yii::t('app', 'Lang ID'),
            'Name' => Yii::t('app', 'Name'),
            'Description' => Yii::t('app', 'Description'),

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(Location::className(), ['ID' => 'LocationID']);
    }
    
    public function getName()
    {
        return ucwords(strtolower($this->Name));
    }


}
