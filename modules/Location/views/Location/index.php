<?php

use yii\helpers\Html;
use app\components\GridView\GridView;
use yii\widgets\Pjax;
use app\modules\Location\models\Location;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\Location\models\LocationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Locations');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="location-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php  //echo $this->render('_search', ['model' => $searchModel]); ?>


<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'Country',
                'label' => 'Tara',
                'value' => 'country.lang.Name',
                'filter' => Html::activeDropDownList($searchModel, 'Country', Location::getList(Yii::t('app', 'Toate'),Location::TypeCountry), ['class' => 'form-control','id '=> 'countryID'])

            ],
            [
                'attribute' => 'Regions',
                'label' => 'Regiune',
                'value' => 'parentLang.Name',
                'filter' => Html::activeDropDownList($searchModel, 'ParentID', Location::getList(Yii::t('app', 'Toate'),Location::TypeRegion,$searchModel->Country), ['class' => 'form-control','id '=> 'parentID'])

            ],
            [
                'attribute' => 'City',
                'label' => 'Search Location',
                'value' => 'lang.Name'

            ],
            [
                'attribute' => 'Type',
                'label' => 'Type',

            ],
            [
                'attribute' => 'lang.Description',
                'label' => 'Description'
            ],

            [   'class' => 'app\components\GridView\ActionColumn',
                'header' => Html::a(Yii::t('app', 'Adauga'), ['create'], ['class' => 'btn btn-success']),
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
