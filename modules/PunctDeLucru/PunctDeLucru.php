<?php

namespace app\modules\PunctDeLucru;

/**
 * punct-de-lucru module definition class
 */
class PunctDeLucru extends \app\components\Module\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\PunctDeLucru\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
