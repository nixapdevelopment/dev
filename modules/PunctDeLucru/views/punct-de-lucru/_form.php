<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\modules\PunctDeLucru\models\PunctDeLucru */
/* @var $form yii\widgets\ActiveForm */
?>

<div id="edit-form-wrap">

    <?php Pjax::begin([
        'id' => 'edit-form-pjax' . md5(microtime(true)),
        'enablePushState' => false,
    ]); ?>

        <?php $form = ActiveForm::begin([
            'id' => 'edit-form-' . md5(microtime(true)),
            'options' => [
                'data-pjax' => true
            ]
        ]); ?>

        <?= $form->field($model, 'Denumire')->textInput(['maxlength' => true]) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    
    <?php Pjax::end(); ?>

</div>
