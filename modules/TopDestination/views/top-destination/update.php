<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\TopDestination\models\TopDestination */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Top Destination',
]) . $model->ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Top Destinations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="top-destination-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
