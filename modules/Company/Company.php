<?php

namespace app\modules\Company;

use app\components\Module\Module;

/**
 * company module definition class
 */
class Company extends Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Company\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
