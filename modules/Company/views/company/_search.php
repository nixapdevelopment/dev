<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\Company\models\CompanySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="company-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'Denumire') ?>

    <?= $form->field($model, 'CodFiscal') ?>

    <?= $form->field($model, 'NrRegistrulComertului') ?>

    <?= $form->field($model, 'CodCAEN') ?>

    <?php // echo $form->field($model, 'Judet') ?>

    <?php // echo $form->field($model, 'Localitate') ?>

    <?php // echo $form->field($model, 'Sector') ?>

    <?php // echo $form->field($model, 'Strada') ?>

    <?php // echo $form->field($model, 'Numar') ?>

    <?php // echo $form->field($model, 'CodPostal') ?>

    <?php // echo $form->field($model, 'Bloc') ?>

    <?php // echo $form->field($model, 'Scara') ?>

    <?php // echo $form->field($model, 'Etaj') ?>

    <?php // echo $form->field($model, 'Apartament') ?>

    <?php // echo $form->field($model, 'Telefon') ?>

    <?php // echo $form->field($model, 'Email') ?>

    <?php // echo $form->field($model, 'OperatiiInValuta') ?>

    <?php // echo $form->field($model, 'MetodaDeIesireStocuri') ?>

    <?php // echo $form->field($model, 'ModulDePlataTVA') ?>

    <?php // echo $form->field($model, 'TVAColectataLaIncasare') ?>

    <?php // echo $form->field($model, 'PersoanaJuridicaFaraScopLucrativ') ?>

    <?php // echo $form->field($model, 'Microintreprindere') ?>

    <?php // echo $form->field($model, 'CapitalSocial') ?>

    <?php // echo $form->field($model, 'TipDeCalculAlDateiScadenteiInFacturi') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
