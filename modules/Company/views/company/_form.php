<?php

use yii\helpers\Html;
use app\components\ActiveForm\ActiveForm;
use app\modules\Judet\models\Judet;
use app\components\Enum\MetodaDeIesireStocuri;
use app\components\Enum\ModulDePlataTVA;

/* @var $this yii\web\View */
/* @var $model app\modules\Company\models\Company */
/* @var $form app\components\ActiveForm\ActiveForm */
?>

<div class="company-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Denumire')->textInput(['maxlength' => true]) ?>
    
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'CodFiscal')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'NrRegistrulComertului')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'CodCAEN')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'CapitalSocial')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'PersoanaJuridicaFaraScopLucrativ')->checkbox() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'Microintreprindere')->checkbox() ?>
        </div>
        <div class="col-md-4 hidden">
            <?= $form->field($model, 'TipDeCalculAlDateiScadenteiInFacturi')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'Judet')->dropDownList(Judet::getList(Yii::t('app', 'Selectati'))) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'Localitate')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'Sector')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'Strada')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'Numar')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-1">
            <?= $form->field($model, 'Bloc')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-1">
            <?= $form->field($model, 'Scara')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-1">
            <?= $form->field($model, 'Etaj')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-1">
            <?= $form->field($model, 'Apartament')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'Telefon')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'Email')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'CodPostal')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'ModulDePlataTVA')->dropDownList(ModulDePlataTVA::getList(Yii::t('app', 'Selectati'))) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'MetodaDeIesireStocuri')->dropDownList(MetodaDeIesireStocuri::getList(Yii::t('app', 'Selectati'))) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'OperatiiInValuta')->checkbox() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'TVAColectataLaIncasare')->checkbox() ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
