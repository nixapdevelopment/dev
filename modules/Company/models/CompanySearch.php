<?php

namespace app\modules\Company\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\Company\models\Company;

/**
 * CompanySearch represents the model behind the search form about `app\modules\Company\models\Company`.
 */
class CompanySearch extends Company
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'Judet', 'OperatiiInValuta', 'TVAColectataLaIncasare', 'PersoanaJuridicaFaraScopLucrativ', 'Microintreprindere'], 'integer'],
            [['Denumire', 'CodFiscal', 'NrRegistrulComertului', 'CodCAEN', 'Localitate', 'Sector', 'Strada', 'Numar', 'CodPostal', 'Bloc', 'Scara', 'Etaj', 'Apartament', 'Telefon', 'Email', 'MetodaDeIesireStocuri', 'ModulDePlataTVA', 'TipDeCalculAlDateiScadenteiInFacturi'], 'safe'],
            [['CapitalSocial'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Company::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'Judet' => $this->Judet,
            'OperatiiInValuta' => $this->OperatiiInValuta,
            'TVAColectataLaIncasare' => $this->TVAColectataLaIncasare,
            'PersoanaJuridicaFaraScopLucrativ' => $this->PersoanaJuridicaFaraScopLucrativ,
            'Microintreprindere' => $this->Microintreprindere,
            'CapitalSocial' => $this->CapitalSocial,
        ]);

        $query->andFilterWhere(['like', 'Denumire', $this->Denumire])
            ->andFilterWhere(['like', 'CodFiscal', $this->CodFiscal])
            ->andFilterWhere(['like', 'NrRegistrulComertului', $this->NrRegistrulComertului])
            ->andFilterWhere(['like', 'CodCAEN', $this->CodCAEN])
            ->andFilterWhere(['like', 'Localitate', $this->Localitate])
            ->andFilterWhere(['like', 'Sector', $this->Sector])
            ->andFilterWhere(['like', 'Strada', $this->Strada])
            ->andFilterWhere(['like', 'Numar', $this->Numar])
            ->andFilterWhere(['like', 'CodPostal', $this->CodPostal])
            ->andFilterWhere(['like', 'Bloc', $this->Bloc])
            ->andFilterWhere(['like', 'Scara', $this->Scara])
            ->andFilterWhere(['like', 'Etaj', $this->Etaj])
            ->andFilterWhere(['like', 'Apartament', $this->Apartament])
            ->andFilterWhere(['like', 'Telefon', $this->Telefon])
            ->andFilterWhere(['like', 'Email', $this->Email])
            ->andFilterWhere(['like', 'MetodaDeIesireStocuri', $this->MetodaDeIesireStocuri])
            ->andFilterWhere(['like', 'ModulDePlataTVA', $this->ModulDePlataTVA])
            ->andFilterWhere(['like', 'TipDeCalculAlDateiScadenteiInFacturi', $this->TipDeCalculAlDateiScadenteiInFacturi]);

        return $dataProvider;
    }
}
