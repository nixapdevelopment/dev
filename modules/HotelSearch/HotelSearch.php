<?php

namespace app\modules\HotelSearch;

use Yii;
/**
 * hotel-search module definition class
 */
class HotelSearch extends \app\components\Module\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\HotelSearch\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->layoutPath = Yii::getAlias('@app/views/themes/front/layouts');
    }
}
