<?php

    use yii\helpers\Url;
    use yii\helpers\ArrayHelper;
    use app\views\themes\front\assets\FrontAsset;
    
    $childYears = [];
    for ($y = 1; $y <= 17; $y++)
    {
        $childYears[$y] = $y;
    }
    
    $bundle = FrontAsset::register($this);

?>
<br />

<?php echo $this->render('../../../../views/themes/front/_search'); ?>

<div class="container">
    <section style="overflow: visible" class="pt20 pb20">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mb10">
                    <div class="text-right">
                        <div class="sort">
                            <a href="#" data-type="price" class="order asc">Priece</a>
                            <a href="#" data-type="stars" class="order">Category</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <form id="filter-hotel-form" class="sort-hotel-sidebar">
                        <div class="hotel-name">
                            <label class="name-filter">
                                Hotel name
                            </label>
                            <div class="search-hotel-filter">
                                <input name="Name" type="text">
                                <button type="button" class="btn btn-default">
                                    <span class="fa fa-search"></span>
                                </button>
                            </div>
                        </div>
                        <br />
                        <div class="costume-input">
                            <!--div class="category-hotel">
                                <label class="name-filter">
                                    Board
                                </label>
                                <input type="checkbox" id="board1" name="board1">
                                <label for="board1">
                                    <span class="check"></span>
                                    Incl. breakfast
                                </label>
                                <input type="checkbox" id="board2" name="board2">
                                <label for="board2">
                                    <span class="check"></span>
                                    Self catering (apartments)
                                </label>
                                <input type="checkbox" id="board3" name="board3">
                                <label for="board3">
                                    <span class="check"></span>
                                    Half board
                                </label>
                                <input type="checkbox" id="board4" name="board4">
                                <label for="board4">
                                    <span class="check"></span>
                                    Room only (hotels)
                                </label>
                                <input type="checkbox" id="board5" name="board5">
                                <label for="board5">
                                    <span class="check"></span>
                                    Incl. breakfast, lunch and dinner
                                </label>
                            </div-->
                            <div class="category-hotel">
                                <label class="name-filter">
                                    Category
                                </label>
                                <input type="checkbox" id="five-disable-star" name="Star[]" value="0">
                                <label for="five-disable-star">
                                    <span class="check"></span>
                                    <img class="img-responsive" src="<?= $bundle->baseUrl ?>/images/five-disable-star.png" alt="">
                                </label>
                                <input type="checkbox" id="one-star" name="Star[]" value="1">
                                <label for="one-star">
                                    <span class="check"></span>
                                    <img class="img-responsive" src="<?= $bundle->baseUrl ?>/images/one-star.png" alt="">
                                </label>
                                <input type="checkbox" id="two-star" name="Star[]" value="2">
                                <label for="two-star">
                                    <span class="check"></span>
                                    <img class="img-responsive" src="<?= $bundle->baseUrl ?>/images/two-star.png" alt="">
                                </label>
                                <input type="checkbox" id="three-star" name="Star[]" value="3">
                                <label for="three-star">
                                    <span class="check"></span>
                                    <img class="img-responsive" src="<?= $bundle->baseUrl ?>/images/three-star.png" alt="">
                                </label>
                                <input type="checkbox" id="four-star" name="Star[]" value="4">
                                <label for="four-star">
                                    <span class="check"></span>
                                    <img class="img-responsive" src="<?= $bundle->baseUrl ?>/images/four-star.png" alt="">
                                </label>
                                <input type="checkbox" id="five-star" name="Star[]" value="5">
                                <label for="five-star">
                                    <span class="check"></span>
                                    <img class="img-responsive" src="<?= $bundle->baseUrl ?>/images/five-star.png" alt="">
                                </label>
                            </div>
                            <br />
                            <div class="price">
                                <label class="name-filter">
                                    Price
                                </label>
                                <b><span class="data-slider-min">0</span> €</b>
                                <input class="filter-price span2" name="_Price" type="text"  value="" data-slider-min="0" data-slider-max="10000" data-slider-step="10" data-slider-value="[0,10000]"/>
                                <b>€ <span class="data-slider-max">10000</span></b>
                                <input name="MinPrice" value="" class="hidden" />
                                <input name="MaxPrice" value="" class="hidden" />
                            </div>
                        </div>
                        <input type="hidden" name="order" value="priceasc" />
                    </form>
                </div>
                <div class="col-md-9">
                    <div class="restult-search">
                        <?php if (Yii::$app->request->get('LocationID')) { ?>
                        <div class="search-hotel-results">
                            <div class="text-center">
                                <img src="https://s-media-cache-ak0.pinimg.com/originals/65/97/87/659787288f1824921c38dcf4d3158768.gif" />
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
</div>

<div id="loader" style="display: none; position: fixed;z-index: 9999999999;background-color: rgba(34, 34, 34, 0.68);top: 0;width: 100%;bottom: 0;">
    <div style="position: absolute;background: #fff;left: 50%;top: 40%;width: 240px;height: 150px;margin-left: -120px;">
        <div style="text-align: center;font-size: 34px;line-height: 140px;">
            Loading...
        </div>
    </div>
</div>

<style>
    .room-controls, .child-age-control {
        display: none;
    }
</style>

<?php if (Yii::$app->request->get('LocationID')) {
    $this->registerJs("
        $.post('" . Url::to(['/hotel-search/hotel-search/ajax-hotel-search']) . "', {data: " . json_encode(ArrayHelper::merge(Yii::$app->request->get(), [Yii::$app->request->csrfParam => Yii::$app->request->getCsrfToken()])) . ", filters: {}}, function(html){
            $('.search-hotel-results').html(html);
            $('#pager-wrap').easyPaginate({
                paginateElement: '.page-item',
                elementsPerPage: 30,
                effect: 'fade'
            });
        });
        
        $(document).on('clcik', '.easyPaginateNav a', function(){
            $('html, body').animate({scrollTop: 0}, 500);
        });
        
        $('a.order').click(function(){
            var elem = $(this);
            
            $('a.order').not(this).removeClass('asc').removeClass('desc');
            
            var order = 'asc';
            if (elem.hasClass('asc'))
            {
                elem.removeClass('asc').addClass('desc');
                order = 'desc';
            }
            else
            {
                elem.removeClass('desc').addClass('asc');
                order = 'asc';
            }
            
            $('input[name=\"order\"]').val(elem.attr('data-type') + order).trigger('change');
        });
        
        $('#filter-hotel-form input').not('input[name=\"_Price\"]').change(function(e){
            e.preventDefault();
            $('#loader').fadeIn();
            $.post('" . Url::to(['/hotel-search/hotel-search/ajax-hotel-search']) . "', {data: " . json_encode(ArrayHelper::merge(Yii::$app->request->get(), [Yii::$app->request->csrfParam => Yii::$app->request->getCsrfToken()])) . ", filters: $('#filter-hotel-form').serialize()}, function(html){
                $('.search-hotel-results').html(html);
                $('#pager-wrap').easyPaginate({
                    paginateElement: '.page-item',
                    elementsPerPage: 30,
                    effect: 'fade'
                });
                $('#loader').fadeOut();
            });
        });
        
        $('select[name=LocationID]').append('<option value=\"$selectedLocation->ID\">" . $selectedLocation->lang->Name . "</option>').val($selectedLocation->ID).trigger('change');
    ");
} ?>

<?php $this->registerJs("
    $('select[name=Rooms]').change(function(){
        $('.room-controls').not(':first').hide().find('select').attr('disabled', 'disabled');

        var rooms = parseInt($(this).val());
        
        for (i = rooms - 1; i > 0; i--)
        {
            $('.room-controls').eq(i).show().find('select').removeAttr('disabled');
        }
    });
    
    $('select[name^=Childs]').change(function(){
        $(this).closest('.room-controls').find('.child-age-control').hide().find('select').attr('disabled', 'disabled');
        var childs = parseInt($(this).val());
        for (i = childs - 1; i >= 0; i--)
        {
            $(this).closest('.room-controls').find('.child-age-control').eq(i).show().find('select').removeAttr('disabled');
        }
    });
") ?>

<style>
    .select2-container {
        max-width: 97%;
    }
    .input-group.date {
        max-width: 170px;
    }
    #w2-kvdate {
        max-width: 143px;
    }
</style>