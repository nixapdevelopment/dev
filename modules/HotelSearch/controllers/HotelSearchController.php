<?php

namespace app\modules\HotelSearch\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use app\controllers\FrontController;
use app\modules\Location\models\Location;
use app\modules\Parser\providers\PrestigeProvider;
use app\modules\Parser\providers\HotelbedsProvider;
use app\modules\Parser\providers\W2MProvider;
use app\modules\Parser\providers\SunhotelsProvider;
use app\modules\Parser\providers\WhlProvider;
use app\modules\Booking\models\SearchInfo;
use app\modules\Parser\entities\Hotel;
use app\modules\Parser\entities\HotelRoom;
use app\modules\Parser\entities\RoomBoard;
use app\modules\Parser\entities\RoomBoardPrice;
use app\modules\Parser\entities\CancellationPolicy;

/* @var $results app\modules\Parser\entities\Hotel[] */

class HotelSearchController extends FrontController
{

    public function actionIndex()
    {
        $selectedLocation = Yii::$app->request->get('LocationID', false) ? Location::find()->with('lang', 'country')->where(['ID' => Yii::$app->request->get('LocationID')])->one() : false;
        
        return $this->render('index', [
            'selectedLocation' => $selectedLocation,
        ]);
    }
    
    public function actionAjaxHotelSearch()
    {
        $data = \Yii::$app->request->post('data');
        unset($data['_csrf']);
        
        $back = \Yii::$app->request->get('back', 0);
        
        $hash = md5(serialize($data));
        
        $filters = \Yii::$app->request->post('filters');
        
        parse_str($filters, $filters);
        
        $searchInfoModel = new SearchInfo([
            'Hash' => $hash,
            'Data' => serialize($data),
        ]);
        $searchInfoModel->save();
        
        Yii::$app->session->set('searchData', $data);
        Yii::$app->session->set('searchHash', $hash);
        
        chmod(Yii::getAlias('@app/modules/HotelSearch/cache/'), 0777);
        $cacheFile = Yii::getAlias('@app/modules/HotelSearch/cache/' . $hash . '.txt');
        
        if (file_exists($cacheFile) && filemtime($cacheFile) + 3600 > time())
        {
            $results = unserialize(file_get_contents($cacheFile));
        }
        else
        {
            $results = [];
            
            $provider = new HotelbedsProvider();
            $results = $provider->searchHotels($data, $hash);
            
            $provider = new PrestigeProvider();
            $results += $provider->searchHotels($data, $hash);
			
            $provider = new SunhotelsProvider();
            $results += $provider->searchHotels($data, $hash);
            
            $provider = new WhlProvider();
            $results += $provider->searchHotels($data, $hash);
            
//            $provider = new \app\modules\Parser\providers\ParalelaProvider();
//            $provider->searchHotels($data, $hash);
//            exit;

            $f = fopen($cacheFile, "a+");
            fwrite($f, serialize($results));
            fclose($f);
        }
        
        $minPrice = 0;
        $maxPrice = 0;
        foreach ($results as $result)
        {
            $minPrice = $result->MinPrice < $minPrice || $minPrice == 0 ? $result->MinPrice : $minPrice;
            $maxPrice = $result->MinPrice > $maxPrice || $maxPrice == 0 ? $result->MinPrice : $maxPrice;
        }
        
        $results = array_filter($results, function($value) use ($filters) {
            
            if ($value->MinPrice == 0)
            {
                return false;
            }
            
            if (!empty($filters['Star']))
            {
                if (!in_array($value->Stars, $filters['Star']))
                {
                    return false;
                }
            }
            
            if (!empty($filters['Name']))
            {
                if (strpos(strtolower($value->Name), strtolower(trim($filters['Name']))) === false)
                {
                    return false;
                }
            }
            
            if (!empty($filters['MinPrice']))
            {
                if ($value->MinPrice <= $filters['MinPrice'])
                {
                    return false;
                }
            }
            
            if (!empty($filters['MaxPrice']))
            {
                if ($value->MinPrice >= $filters['MaxPrice'])
                {
                    return false;
                }
            }
            
            return true;
        });
        
        if (!empty($filters['order']))
        {
            switch ($filters['order'])
            {
                case 'pricedesc':
                    $attr = 'MinPrice';
                    $order = SORT_DESC;
                    break;
                case 'priceasc':
                    $attr = 'MinPrice';
                    $order = SORT_ASC;
                    break;
                case 'starsasc':
                    $attr = 'Stars';
                    $order = SORT_ASC;
                    break;
                case 'starsdesc':
                    $attr = 'Stars';
                    $order = SORT_DESC;
                    break;
                default:
                    break;
            }
            
            ArrayHelper::multisort($results, $attr, $order);
        }
        
        return $this->renderAjax('search-results', [
            'results' => $results,
            'searchID' => $searchInfoModel->ID,
            'minPrice' => $minPrice,
            'maxPrice' => $maxPrice,
            'filters' => $filters,
            'back' => $back,
        ]);
    }
    
    public function actionGetHotelImages($hotelKey)
    {
        $cacheFile = Yii::getAlias('@app/modules/HotelSearch/cache/' . Yii::$app->session->get('searchHash') . '.txt');
        $results = unserialize(file_get_contents($cacheFile));
        
        return $this->renderPartial('hotel-images', [
            'images' => isset($results[$hotelKey]->Images) ? $results[$hotelKey]->Images : [],
            'hotelKey' => $hotelKey,
        ]);
    }
    
    public function actionGetHotelMap($hotelKey)
    {
        $cacheFile = Yii::getAlias('@app/modules/HotelSearch/cache/' . Yii::$app->session->get('searchHash') . '.txt');
        $results = unserialize(file_get_contents($cacheFile));
        
        if ($results[$hotelKey]->Latitude == 0 || $results[$hotelKey]->Longitude == 0)
        {
            $query = urlencode($results[$hotelKey]->Address . ', ' . $results[$hotelKey]->Name);
        }
        else
        {
            $query = $results[$hotelKey]->Latitude . ',' . $results[$hotelKey]->Longitude;
        }
        
        $query = urlencode($results[$hotelKey]->Address . ', ' . $results[$hotelKey]->Name);
        
        echo '<iframe width="99%" height="300" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyA0F1THcq9awmwX68ZGgySFau6Ky4wPUxY&q=' . $query . '" allowfullscreen></iframe>';
    }
    
    public function actionParse()
    {
        $provider = new \app\modules\Parser\providers\ParalelaProvider();
        $provider->parse();
    }

}
