<?php

namespace app\modules\Site\controllers;

use yii\web\Controller;
use app\controllers\SiteBackendController;

/**
 * Default controller for the `Site` module
 */
class DefaultController extends SiteBackendController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionError()
    {
        throw new NotFoundHttpException("Page not found");
    }
}
