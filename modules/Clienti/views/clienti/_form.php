<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\Judet\models\Judet;
use app\modules\Agenti\models\Agenti;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\modules\Clienti\models\Clienti */
/* @var $form yii\widgets\ActiveForm */
?>

<div id="edit-form-wrap">
    
    <?php Pjax::begin([
        'id' => 'edit-form-pjax' . md5(microtime(true)),
        'enablePushState' => false,
    ]); ?>

        <?php $form = ActiveForm::begin([
            'id' => 'edit-form-' . md5(microtime(true)),
            'options' => [
                'data-pjax' => true
            ]
        ]); ?>
    
        <div class="row">
            <div class="col-md-7">
                <?= $form->field($model, 'Denumire')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-5">
                <?= $form->field($model, 'CodFiscal')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <?= $form->field($model, 'Judet')->dropDownList(Judet::getList(Yii::t('app', 'Selectati'))) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'Adresa')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'Telefon')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'Email')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'ContBancar')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'Banca')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'Delegat')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'MijloculDeTransport')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'CISerie')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'CINumar')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'EliberatDe')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-md-2">
                <?= $form->field($model, 'ContAnalitic')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'Reducere')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'NrRegistrulComertului')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'Agent')->dropDownList(Agenti::getList(Yii::t('app', 'Selectati'))) ?>
            </div>
        </div>
    
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    
    <?php Pjax::end(); ?>

</div>
