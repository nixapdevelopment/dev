<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\Clienti\models\Clienti */

$this->title = Yii::t('app', 'Create Clienti');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Clientis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clienti-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
