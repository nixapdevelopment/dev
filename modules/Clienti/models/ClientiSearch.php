<?php

namespace app\modules\Clienti\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\Clienti\models\Clienti;

/**
 * ClientiSearch represents the model behind the search form about `app\modules\Clienti\models\Clienti`.
 */
class ClientiSearch extends Clienti
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'Judet', 'Agent'], 'integer'],
            [['Denumire', 'CodFiscal', 'ContAnalitic', 'Adresa', 'ContBancar', 'Banca', 'Telefon', 'Email', 'NrRegistrulComertului', 'Delegat', 'CISerie', 'CINumar', 'EliberatDe', 'MijloculDeTransport'], 'safe'],
            [['Reducere'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Clienti::find()->with(['judet', 'agent']);

        // add conditions that should always apply here

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'Judet' => $this->Judet,
            'Agent' => $this->Agent,
            'Reducere' => $this->Reducere,
        ]);

        $query->andFilterWhere(['like', 'Denumire', $this->Denumire])
            ->andFilterWhere(['like', 'CodFiscal', $this->CodFiscal])
            ->andFilterWhere(['like', 'ContAnalitic', $this->ContAnalitic])
            ->andFilterWhere(['like', 'Adresa', $this->Adresa])
            ->andFilterWhere(['like', 'ContBancar', $this->ContBancar])
            ->andFilterWhere(['like', 'Banca', $this->Banca])
            ->andFilterWhere(['like', 'Telefon', $this->Telefon])
            ->andFilterWhere(['like', 'Email', $this->Email])
            ->andFilterWhere(['like', 'NrRegistrulComertului', $this->NrRegistrulComertului])
            ->andFilterWhere(['like', 'Delegat', $this->Delegat])
            ->andFilterWhere(['like', 'CISerie', $this->CISerie])
            ->andFilterWhere(['like', 'CINumar', $this->CINumar])
            ->andFilterWhere(['like', 'EliberatDe', $this->EliberatDe])
            ->andFilterWhere(['like', 'MijloculDeTransport', $this->MijloculDeTransport]);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }
}
