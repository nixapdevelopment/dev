<?php

namespace app\modules\Clienti;

/**
 * clienti module definition class
 */
class Clienti extends \app\components\Module\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Clienti\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
