<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model app\modules\Category\models\Category */
/* @var $form yii\widgets\ActiveForm */


?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'CreatedAt')->textInput() ?>

    <?= $form->field($model, 'ParentID')->textInput() ?>

    <?php
    $items = [];
    foreach ($model->langs as $langID => $langModel)
    {
        $items[] = [
            'label' => strtoupper($langID),
            'content' => $this->render('_lang_form',[
                'form' => $form,
                'langModel' => $langModel,
            ]),

        ];
    }

    echo Tabs::widget([
        'items' => $items,
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
