<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'layout' => 'frontend',
    'language' => 'ro',
    'defaultRoute' => 'home/home',
    'components' => [
        'errorHandler' => [
            'errorAction' => 'front/error',
        ],

        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'ABT28K3bcULxr0bUEKoJdNcDXD1AzR0j',
            'baseUrl' => '',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'assetManager' => [
            //'forceCopy' => true,
        ],
        'user' => [
            'identityClass' => 'app\modules\User\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                //'backoffice' => 'backoffice/default/index',
                //'backoffice/<module>/<controller>/<action>' => 'backoffice',
                'booking/default/try-booking' => 'booking/default/try-booking',
                'backoffice/hotel/hotel/<slug:(.*)>' => '/backoffice/hotel/hotel/view',
                'backoffice/book/succeess' => '/backoffice/book/succeess',
                'hotel/index/<id:\d+>-<slug:([a-zA-z0-9-]+)>' => '/hotel/index',
                'hotel/<slug:(.*)>' => 'hotel/view',
                'sejur/sejur-view/<id:\d+>-<slug:([a-zA-z0-9-]+)>' => 'sejur/sejur-view'
            ],
        ],
        'view' => [
            'class' => 'yii\web\View',
            'theme' => [
                'basePath' => '@app/views/themes/buh',
                'pathMap' => [
                    '@app/views' => '@app/views/themes/buh',
                ],
            ],
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                    'fileMap' => [
                        'app' => 'app.php',
                    ],
                ],
            ],
        ],
    ],
    'modules' => [
        'backoffice' => [
            'class' => 'app\modules\BackOffice\BackOffice',
            'modules' => [
                'hotel' => [
                    'class' => 'app\modules\BackOffice\modules\Hotel\Hotel',
                ],
                'booking' => [
                    'class' => 'app\modules\BackOffice\modules\Booking\Booking',
                ],
            ]
        ],
        'site' => [
            'class' => 'app\modules\Site\Site',
            'modules' => [
                'questions' => [
                    'class' => 'app\modules\Questions\Questions',
                    'defaultRoute' => 'questions/index',
                ],
                'feedback' => [
                    'class' => 'app\modules\Feedback\Feedback',
                    'defaultRoute' => 'feedback/index',
                ],
                'newsletter' => [
                    'class' => 'app\modules\Newsletter\Newsletter',
                    'defaultRoute' => 'newsletter/index',
                ],

                'slider' => [
                    'class' => 'app\modules\Slider\Slider',
                    'defaultRoute' => 'default/index',
                ],
                'seo-post' => [
                    'class' => 'app\modules\SeoPost\SeoPost',
                    'defaultRoute' => 'seo-post/index',
                ],
                'our-info' => [
                    'class' => 'app\modules\OurInfo\OurInfo',
                    'defaultRoute' => 'our-info/index',
                ],
                'top-destination' => [
                    'class' => 'app\modules\TopDestination\TopDestination',
                    'defaultRoute' => 'top-destination/index',
                ],
                'testimonials' => [
                    'class' => 'app\modules\Testimonials\Testimonials',
                    'defaultRoute' => 'testimonials/index',
                ],
                'location' => [
                    'class' => 'app\modules\Location\Location',
                    'defaultRoute' => 'location/index',
                ],
                'hotel' => [
                    'class' => 'app\modules\Hotel\Hotel',
                    'defaultRoute' => 'hotel/index',
                ],
                'blog' => [
                    'class' => 'app\modules\Blog\Blog',
                    'modules' => [
                        'post' => [
                            'class' => 'app\modules\Post\Post',
                            'defaultRoute' => 'post/index',
                        ],
                        'category' => [
                            'class' => 'app\modules\Category\Category',
                            'defaultRoute' => 'category/index',
                        ],
                        'comment' => [
                            'class' => 'app\modules\Comment\Comment',
                            'defaultRoute' => 'comment/index',
                        ],

                    ],
                ],
            ],
        ],

        'gridview' =>  [
            'class' => '\kartik\grid\Module'
            // 'downloadAction' => 'gridview/export/download',
            // 'i18n' => []
        ],
        'treemanager' =>  [
            'class' => '\kartik\tree\Module',
            'treeViewSettings'=> [
                'nodeView' => '@app/modules/Articole/views/articole/_form',
            ],
            'dataStructure' => [
                'nameAttribute' => 'Name',
            ],
            // other module settings, refer detailed documentation
        ],
        'operator' => [
            'class' => 'app\modules\Operator\Operator',
        ],
        'user' => [
            'class' => 'app\modules\User\User',
        ],
        'company' => [
            'class' => 'app\modules\Company\Company',
        ],
        'dashboard' => [
            'class' => 'app\modules\Dashboard\Dashboard',
        ],
        'judet' => [
            'class' => 'app\modules\Judet\Judet',
        ],
        'articole' => [
            'class' => 'app\modules\Articole\Articole',
            'defaultRoute' => 'articole/index'
        ],
        'tipuri-de-articole' => [
            'class' => 'app\modules\TipuriDeArticole\TipuriDeArticole',
            'defaultRoute' => 'tipuri-de-articole/index',
        ],
        'furnizori' => [
            'class' => 'app\modules\Furnizori\Furnizori',
            'defaultRoute' => 'furnizori/index',
        ],
        'agenti' => [
            'class' => 'app\modules\Agenti\Agenti',
            'defaultRoute' => 'agenti/index',
        ],
        'clienti' => [
            'class' => 'app\modules\Clienti\Clienti',
            'defaultRoute' => 'clienti/index',
        ],
        'salariati' => [
            'class' => 'app\modules\Salariati\Salariati',
            'defaultRoute' => 'salariati/index',
        ],
        'punct-de-lucru' => [
            'class' => 'app\modules\PunctDeLucru\PunctDeLucru',
            'defaultRoute' => 'punct-de-lucru/index',
        ],
        'parser' => [
            'class' => 'app\modules\Parser\Parser',
        ],
        'hotel-search' => [
            'class' => 'app\modules\HotelSearch\HotelSearch',
        ],
        'tour' => [
            'class' => 'app\modules\Tour\Tour',
        ],
        'booking' => [
            'class' => 'app\modules\Booking\Booking',
        ],
    ],
    'params' => $params,
    'defaultRoute' => 'home/home',
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
