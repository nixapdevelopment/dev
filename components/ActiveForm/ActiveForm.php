<?php
namespace app\components\ActiveForm;

class ActiveForm extends \yii\bootstrap\ActiveForm
{
    
    public $fieldConfig = [
        'checkboxTemplate' => '
            <div class="checkbox">
                <div>&nbsp;</div>
                <div style="position: relative;top: 0px;">
                    {beginLabel}{input} <b style="position:relative; top: 0px;">{labelTitle}</b>{endLabel}
                </div>
                <div>{error}{hint}</div>
            </div>'
    ];
    
    public $fieldClass = 'app\components\ActiveForm\ActiveField';
    
}