<?php
namespace app\components\GridView;

use kartik\grid\ActionColumn as KActionColumn;

class ActionColumn extends KActionColumn
{
    
    public $template = '{update}&nbsp;&nbsp;&nbsp;&nbsp;{delete}';
    
    public $options = [
        'width' => '100px',
        'class' => 'grid-action-column'
    ];
    
}
