<?php

namespace app\components\Module;

use Yii;


class BackOfficeModule extends yii\base\Module
{
    
    public function init()
    {
        parent::init();
        
        $this->layoutPath = Yii::getAlias('@app/views/themes/buh/layouts');
    }
    
}
