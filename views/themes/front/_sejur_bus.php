<?php

use app\views\themes\front\assets\FrontAsset;
use yii\bootstrap\Html;
use kartik\rating\StarRating;
use yii\helpers\Url;

$bundle = FrontAsset::register($this);
?>



<style>
    .tur-item {
        margin: 0 0 15px 0;
        padding: 0 0 15px 0;
        border-bottom: 1px dashed #DDDDDD;
    }
    .tur-thumb {
        height: 150px;
        overflow: hidden;
    }
    .tur-thumb > a {
        display: block;
    }
    .tur-thumb > a > img {
        width: 100%;
    }
    .tur-name {
        font-size: 13px;
        font-weight: bold;
        text-transform: uppercase;
    }
    .tur-name > a {
        color: #000000;
        margin: 0 15px 0 0;
    }
    .tur-location {
        
    }
    .tur-location > a {
        color: #000000;
    }
    .tur-location > a > small {
        
    }
    .tur-stars {
        color: rgb(254, 186, 2);
    }
    .tur-info {
        font-size: 11px;
    }
    .tur-price {
        text-align: right;
    }
    .tur-price > a {
        display: block;
        color: #FF6600;
        font-family: monospace;
        font-size: 22px;
        font-weight: bold;
    }
    .tur-rating {
        text-align: right;
    }
</style>

<div class="tur-item">
    <div class="row">
        <div class="col-md-5">
            <div class="tur-thumb">
                <a href="<?= $model->seoUrl;?>">
                    <?php echo Html::img($model->image,[]);?>
                </a>
            </div>
        </div>
        <div class="col-md-7">
            <div class="row">
                <div class="col-md-6">
                    <div class="tur-name">
                        <a href="<?= $model->seoUrl;?>"><?php echo $model->Title; ?></a>
                    </div>
                    <div class="tur-location">
                   </div>
                    <hr>
                    <div class="tur-info">
                        <div> <?php if($model->date){echo('Plecare:'.$model->date);};?></div>
                        <div> <?php if($model->locations){echo('Opriri:'.$model->locations);};?></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="tur-price">
                        <a href=""><?php echo $model->Price; ?><sup>EUR</sup></a>
                    </div>
                    <div class="tur-rating">
<!--                        <img src="--><?php //echo $bundle->baseUrl ?><!--/images/_trip.png" />-->
                    </div>
                    <div class="tur-go">
                        <a href="<?= $model->seoUrl;?>">Vezi oferta</a>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>






