<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\web\JsExpression;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use app\modules\Location\models\Location;

/* @var $this yii\web\View */
/* @var $model app\modules\Hotel\models\HotelSearch */
/* @var $form yii\widgets\ActiveForm */

$locationID = Yii::$app->request->get('LocationID', false);

$preselectedLocation = ['' => ''];
if ($locationID)
{
    $location = Location::find()->with('lang', 'country.lang')->where(['ID' => $locationID])->limit(1)->one();
    $preselectedLocation = [$location->ID => $location->lang->Name . ' (' . $location->country->lang->Name . ')'];
}

$CheckIn = Yii::$app->request->get('CheckIn', date('d.m.Y'));
$CheckOut = Yii::$app->request->get('CheckOut', date('d.m.Y', strtotime('+1 day')));
$Nights = Yii::$app->request->get('Nights', 1);
$Rooms = Yii::$app->request->get('Rooms', 1);
$Adults = Yii::$app->request->get('Adults', []);
$Childs = Yii::$app->request->get('Childs', []);
$ChildAge = Yii::$app->request->get('ChildAge', []);

?>

<div class="container">
    <div class="filter-tab">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#hoteluri" aria-controls="hoteluri" role="tab" data-toggle="tab">
                    <span class="fa fa-building"></span>
                    Hoteluri
                </a>
            </li>
            <li role="presentation">
                <a href="#">
                    <span class="fa fa-plane"></span>
                    Sejur Avion
                </a>
            </li>
            <li role="presentation">
                <a href="#">
                    <span class="fa fa-bus"></span>
                    Sejur Autocar
                </a>
            </li>
        </ul>
        <div class="content-filter">
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="hoteluri">
                    <?php $form = ActiveForm::begin([
                        'action' => Url::to('/hotel-search/hotel-search'),
                        'method' => 'get',
                        'id'     => 'search-form',
                    ]); ?>
                        <div class="row">
                            <div class="col-md-7 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                        <div>
                                            <label>
                                                Destinatie
                                            </label>
                                            <?= Select2::widget([
                                                'name' => 'LocationID',
                                                'options' => ['placeholder' => 'Cautare ...', 'required' => 'reqired', 'width' => '100%'],
                                                'data' => $preselectedLocation,
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                    'minimumInputLength' => 2,
                                                    'language' => [
                                                        'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                                                    ],
                                                    'ajax' => [
                                                        'url' => Url::to(['/site/location/front-ajax/ajax-locations-search']),
                                                        'dataType' => 'json',
                                                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                                    ],
                                                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                                    'templateResult' => new JsExpression('function(city) { return city.text; }'),
                                                    'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                                                ],
                                            ]) ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12 remove-padding">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 remove-padding">
                                                <div class="calendar-elem left">
                                                    <label>
                                                        Check-in
                                                    </label>
                                                    <div class="form-group">
                                                        <?= DatePicker::widget([
                                                            'name' => 'CheckIn',
                                                            'value' => $CheckIn,
                                                            'removeButton' => false,
                                                            'pluginOptions' => [
                                                                'format' => 'dd.mm.yyyy',
                                                                'todayHighlight' => true,
                                                                'autoclose' => true,
                                                                'startDate' => date('d.m.Y'),
                                                            ]
                                                        ]) ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 remove-padding">
                                                <div class="calendar-elem right">
                                                    <label>
                                                        Check-out
                                                    </label>
                                                    <?= DatePicker::widget([
                                                            'name' => 'CheckOut',
                                                            'value' => $CheckOut,
                                                            'removeButton' => false,
                                                            'pluginOptions' => [
                                                                'format' => 'dd.mm.yyyy',
                                                                'todayHighlight' => true,
                                                                'autoclose' => true,
                                                                'startDate' => date('d.m.Y', strtotime('+1 day')),
                                                            ]
                                                        ]) ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="small-select col-md-5 col-sm-12 col-xs-12 ">
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 remove-padding">
                                                <div class="remove-inline-style" style="display:inline-block">
                                                    <label>
                                                        Nr. nopti
                                                    </label>
                                                    <select class="style-select" name="Nights">
                                                        <?php for ($i = 1; $i <= 30; $i++) { ?>
                                                        <option <?= $Nights == $i ? 'selected' : '' ?> value="<?= $i ?>"><?= $i ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 remove-padding">
                                                <div class="remove-inline-style" style="display:inline-block">
                                                    <label>
                                                        Camere
                                                    </label>
                                                    <select name="Rooms" class="style-select">
                                                        <?php for ($i = 1; $i <= 4; $i++) { ?>
                                                        <option <?= $Rooms == $i ? 'selected' : '' ?> value="<?= $i ?>"><?= $i ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-6 text-left">
                                        <?php for ($r = 1; $r <= 4; $r++) { ?>
                                        <div class="room-controls" <?= $r > $Rooms ? 'style="display: none"' : 'style="display: block"' ?>>
                                            <div class="row">
                                                <div class="col-md-2 col-sm-3 col-xs-12 remove-padding">
                                                    <div class="form-group">
                                                        <label class="control-label">Adulti</label>
                                                        <select class="form-control style-select" name="Adults[<?= $r ?>]">
                                                            <?php for ($i = 1; $i <= 4; $i++) { ?>
                                                            <option <?= !empty($Adults[$r]) && $Adults[$r] == $i ? 'selected' : '' ?> value="<?= $i ?>"><?= $i ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-sm-3 col-xs-12 remove-padding">
                                                    <div class="form-group">
                                                        <label class="control-label">Copii</label>
                                                        <select class="form-control style-select" name="Childs[<?= $r ?>]">
                                                            <?php for ($i = 0; $i <= 4; $i++) { ?>
                                                            <option <?= !empty($Childs[$r]) && $Childs[$r] == $i ? 'selected' : '' ?> value="<?= $i ?>"><?= $i ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <?php for ($c = 1; $c <= 4; $c++) { ?>
                                                <div class="col-md-2 col-sm-3 col-xs-12 remove-padding margin-left">
                                                    <div class="child-age-control" style="<?= isset($ChildAge[$r][$c]) ? 'display: block;' : 'display: none;' ?>">
                                                        <label class="label-control">
                                                            Virsta
                                                        </label>
                                                        <div class="form-group">
                                                            <select class="form-control style-select" name="ChildAge[<?= $r ?>][<?= $c ?>]">
                                                                <?php for ($ca = 1; $ca <= 18; $ca++) { ?>
                                                                <option <?= isset($ChildAge[$r][$c]) && $ChildAge[$r][$c] == $ca ? 'selected' : '' ?> value="<?= $ca ?>"><?= $ca ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="mt15">
                                    <button type="button" id="submit-search-form">
                                        Cauta
                                    </button>
                                </div>
                            </div>
                        </div>
                    <?php ActiveForm::end(); ?>
                </div>
                <div role="tabpanel" class="tab-pane" id="sejur-avion">

                </div>
                <div role="tabpanel" class="tab-pane" id="sejur-automobil">

                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->registerJs("
    $('#submit-search-form').click(function(e){
        $('#search-form select[name^=\"Adults\"], #search-form select[name^=\"Childs\"], #search-form select[name^=\"ChildAge\"]').filter(':hidden').each(function(){
            $(this).attr('disabled', 'disabled');
        });
        $('#search-form').submit();
    });
"); ?>