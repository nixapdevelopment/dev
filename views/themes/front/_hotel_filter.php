<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\Hotel\models\HotelSearch */
/* @var $form yii\widgets\ActiveForm */
?>



        <?php $form = ActiveForm::begin([
            'action'=> Url::to('/front/hotel-search'),
            'method' => 'get',
            'id'=>'search-form',
            'fieldConfig' =>[
                'options' => [
                    'tag' => false,
                ]
    ],
        ]); ?>

        <div class="small-title">
            Cauta hotel
        </div>
        <div>
            <label>
                <?= $form->field($model, 'HotelName')->input('text',['placeholder' => "Numele Hotelului",'class'=>''])->label(false) ?>
            </label>
        </div>
        <div class="small-title">
            Stabileste buget/sejur
        </div>
        <div>
            <label>
                <input name="buget1" type="text" placeholder="120">
            </label>
        </div>
        <div>
            <label>
                <input name="buget2" type="text" placeholder="2270">
            </label>
        </div>
        <div class="small-title">
            Categorie
        </div>
        <div>
            <label class="five-star-label" for="FiveStars">

                <?= $form->field($model, 'FiveStars',['template' => "{input}"])->input('checkbox',['class'=>'style-checkbox','id'=>'FiveStars'])->label(false) ?>

            </label>
        </div>
        <div>
            <label class="four-star-label" for="FourStars">
                <?= $form->field($model, 'FourStars',['template' => "{input}"])->input('checkbox',['class'=>'style-checkbox','id'=>'FourStars'])->label(false) ?>
            </label>
        </div>
        <div>
            <label class="three-star-label" for="ThreeStars">
                <?= $form->field($model, 'ThreeStars',['template' => "{input}"])->input('checkbox',['class'=>'style-checkbox','id'=>'ThreeStars'])->label(false) ?>
            </label>
        </div>
        <div>
            <label class="two-star-label" for="TwoStars">
                <?= $form->field($model, 'TwoStars',['template' => "{input}"])->input('checkbox',['class'=>'style-checkbox','id'=>'TwoStars'])->label(false) ?>
            </label>
        </div>
        <div>
            <label class="one-star-label" for="OneStar">
                <?= $form->field($model, 'OneStar',['template' => "{input}"])->input('checkbox',['class'=>'style-checkbox','id'=>'OneStar'])->label(false) ?>
            </label>
        </div>
        <div class="small-title">
            Masa
        </div>
        <div class="different-font">
            <label>
                <input name="fara-masa" type="checkbox" class="style-checkbox">
                FARA MASA
            </label>
        </div>
        <div class="different-font">
            <label>
                <input name="mic-desjun" type="checkbox" class="style-checkbox">
                MIC DEJUN
            </label>
        </div>
        <div class="different-font">
            <label>
                <input name="demipensiune" type="checkbox" class="style-checkbox">
                DEMIPENSIUNE
            </label>
        </div>
        <div class="different-font">
            <label>
                <input name="pensiune-completa" type="checkbox" class="style-checkbox">
                PENSIUNE COMPLETA
            </label>
        </div>
        <div class="different-font">
            <label>
                <input name="all-inclusive" type="checkbox" class="style-checkbox">
                ALL INCLUSIVE
            </label>
        </div>
        <div>

            <?= Html::submitButton(Yii::t('app', 'Cauta'), ['class' => 'btn btn-primary']) ?>
        </div>



<?php ActiveForm::end(); ?>