<?php
/* @var $this yii\web\View */

use yii\widgets\Pjax;
use yii\bootstrap\Html;
use kartik\rating\StarRating;
use app\views\themes\front\assets\FrontAsset;

$bundle = FrontAsset::register($this);
$this->title = $hotel->lang->Name;
?>
<section class="name-page">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="title different-title pb5 pt5">
                        <span class="strong-text">
                            <?php echo $hotel->lang->Name;?>
                        </span>

                    <span>
                            <?php if($hotel->region->lang->Name == $hotel->location->lang->Name){
                                $hotel->region->lang->Name = '';
                            }else{
                                $hotel->region->lang->Name = ' , '. $hotel->region->lang->Name;
                            }?>
                        <?php echo   $hotel->location->lang->Name .$hotel->region->lang->Name.' , '.$hotel->country->lang->Name ?>
                        </span>
                    <div>
                        <img class="img-responsive" src="<?php echo $bundle->baseUrl ?>/images/four-star.png" alt="">
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html">
                            ACASA
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        HOTELURI
                    </li>
                    <li class="breadcrumb-item active">
                        <?php echo $hotel->lang->Name;?>
                    </li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="about-hotel">
    <div class="container">
        <div class="row">
            <div class="hidden-lg hidden-md col-md-3">
                <div class="right-side-bar">
                    <div class="help-user">
                        <div class="small-title">
                            Ai nevoie de ajutor?
                        </div>
                        <a href="#" class="open-help-user">
                            <span class="fa fa-navicon"></span>
                        </a>
                        <div class="toggle-help">
                            <div class="short-description">
                                Dialect Tour iti sta la dispozitie cu orice informatie
                                despre ofertele prezentate pe acest site.
                            </div>
                            <div>
                                <a href="#" class="phone">
                                    <span class="fa fa-phone"></span>
                                    +373 12345678
                                </a>
                            </div>
                            <div>
                                <a href="#" class="email">
                                    <span class="fa fa-envelope"></span>
                                    info@dialect.com
                                </a>
                            </div>
                            <a href="#" class="chat-online">
                                Chat Online
                            </a>
                        </div>
                    </div>
                    <div class="why-us">
                        <div class="small-title">
                            De ce Dialect Tour?
                        </div>
                        <a href="#" class="toggle-facility-box">
                            <span class="fa fa-navicon"></span>
                        </a>
                        <div class="facility-toggle">
                            <div class="facility-box">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="icon-box">
                                            <span class="fa fa-building"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="text">
                                            Pentru ca hotelurile si pachetele noastre de vacanta au fost
                                            alese cu grija astfel incat tu sa ai parte de cele mai bune
                                            servicii la tarife preferentiale.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="facility-box">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="icon-box">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 424.98 424.98" style="enable-background:new 0 0 424.98 424.98;" xml:space="preserve">
                                                <g>
                                                    <g>
                                                        <g>
                                                            <path d="M241.395,282.304c-1.587-1.738-3.595-3.038-5.67-4.121c-4.518-2.356-9.459-3.785-14.365-5.075v38.016     c7.963-0.9,17.105-3.79,21.286-11.224l0,0c1.996-3.551,2.393-7.914,1.58-11.867C243.785,285.891,242.874,283.925,241.395,282.304     z" fill="#FFFFFF"/>
                                                        </g>
                                                        <g>
                                                            <path d="M242.604,299.973c0.016-0.027,0.025-0.044,0.042-0.073l0,0C242.632,299.924,242.618,299.948,242.604,299.973z" fill="#FFFFFF"/>
                                                        </g>
                                                        <g>
                                                            <path d="M184.009,221.532c-1.369,1.999-2.228,4.27-2.465,6.684c-0.237,2.419-0.104,5.11,0.815,7.387     c0.875,2.17,2.708,3.772,4.6,5.062c2.123,1.444,4.458,2.572,6.836,3.528c1.995,0.803,4.239,1.571,6.658,2.313v-34.4     C194.342,213.41,187.665,216.194,184.009,221.532z" fill="#FFFFFF"/>
                                                        </g>
                                                        <g>
                                                            <path d="M242.804,299.619c-0.05,0.089-0.104,0.182-0.157,0.28l0,0C242.709,299.785,242.758,299.701,242.804,299.619z" fill="#FFFFFF"/>
                                                        </g>
                                                        <g>
                                                            <path d="M243.004,299.263C243.017,299.239,243.019,299.237,243.004,299.263L243.004,299.263z" fill="#FFFFFF"/>
                                                        </g>
                                                        <g>
                                                            <path d="M234.753,92.469c32.329-27.158,53.931-88.341,40.637-91.017c-17.664-3.557-56.022,12.04-74.562,14.788     c-26.296,3.175-54.936-28.515-71.012-10.851c-13.071,14.362,9.371,66.592,44.482,89.346     C69.546,146.219-77.69,404.673,179.171,423.426C534.582,449.375,356.615,142.639,234.753,92.469z M265.276,296.298     c-1.093,10.076-6.433,19.188-14.415,25.374c-8.428,6.532-18.999,9.57-29.502,10.421v11.133c0,2.979-1.301,5.86-3.531,7.832     c-3.065,2.712-7.569,3.381-11.289,1.667c-3.673-1.69-6.086-5.457-6.086-9.499v-12.168c-1.801-0.342-3.589-0.749-5.356-1.234     c-9.816-2.697-18.921-7.954-25.572-15.732c-3.313-3.877-6.014-8.276-7.882-13.025c-0.488-1.241-0.923-2.505-1.304-3.783     c-0.345-1.157-0.701-2.333-0.824-3.539c-0.207-2.023,0.194-4.087,1.137-5.889c1.938-3.707,6.022-5.946,10.192-5.574     c4.104,0.364,7.701,3.212,8.993,7.124c0.398,1.205,0.668,2.44,1.115,3.632c0.443,1.184,0.978,2.335,1.607,3.431     c1.242,2.158,2.798,4.148,4.59,5.875c3.694,3.559,8.399,5.872,13.304,7.248v-41.362c-9.591-2.483-19.491-5.69-27.411-11.848     c-3.849-2.994-7.115-6.714-9.254-11.117c-2.257-4.647-3.192-9.824-3.23-14.966c-0.039-5.221,0.953-10.396,3.131-15.153     c2.04-4.454,4.977-8.453,8.578-11.768c7.7-7.087,17.928-11.04,28.187-12.492v-0.91v-10.647c0-2.978,1.301-5.86,3.531-7.832     c3.066-2.711,7.568-3.381,11.289-1.667c3.672,1.691,6.086,5.457,6.086,9.499v10.647v0.847c1.367,0.172,2.73,0.378,4.086,0.624     c10.074,1.823,19.927,5.983,27.294,13.246c3.49,3.44,6.347,7.539,8.356,12.009c0.561,1.247,1.052,2.523,1.477,3.824     c0.396,1.213,0.794,2.462,0.983,3.728c0.302,2.021-0.006,4.109-0.871,5.958c-1.772,3.788-5.746,6.2-9.927,6.021     c-4.108-0.179-7.83-2.854-9.301-6.694c-0.438-1.142-0.657-2.351-1.104-3.49c-0.451-1.153-1.035-2.253-1.708-3.292     c-1.308-2.02-3.003-3.752-4.938-5.179c-4.19-3.094-9.272-4.706-14.35-5.607v39.582c6.035,1.445,12.075,3.021,17.857,5.301     c8.739,3.446,17.02,8.73,21.79,17.062c-0.74-1.298-1.46-2.563,0.025,0.043c1.458,2.56,0.762,1.34,0.03,0.057     C264.854,280.704,266.101,288.701,265.276,296.298z" fill="#FFFFFF"/>
                                                        </g>
                                                        <g>
                                                            <path d="M242.493,300.169c-0.061,0.109-0.114,0.205-0.156,0.278C242.373,300.384,242.427,300.289,242.493,300.169z" fill="#FFFFFF"/>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="text">
                                            Pentru ca o vacanta fantastica este si mai frumoasa daca
                                            vine la un tarif redus. De aceea, noi iti oferim cele mai bune
                                            oferte, pe care nu le vei mai gasi nicaieri, astfel incat si
                                            portofelul tau sa se relaxeze in vacanta.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="facility-box">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="icon-box">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 349.667 349.667" style="enable-background:new 0 0 349.667 349.667;" xml:space="preserve" width="512px" height="512px">
                                                <g>
                                                    <path d="M174.833,197.204c24.125,0,80.846-29.034,80.846-98.603C255.68,44.145,248.329,0,174.833,0   c-73.495,0-80.846,44.145-80.846,98.602C93.987,168.17,150.708,197.204,174.833,197.204z M106.07,82.146   c5.679-10.983,17.963-23.675,44.381-23.112c0,0,15.746,38.194,93.05,21.042c0.312,6.101,0.41,12.326,0.41,18.526   c0,34.005-15.015,55.075-27.612,66.762c-15.872,14.727-33.494,20.072-41.466,20.072c-7.972,0-25.594-5.345-41.466-20.072   c-12.597-11.687-27.612-32.757-27.612-66.762C105.756,93.101,105.836,87.581,106.07,82.146z" fill="#FFFFFF"/>
                                                    <path d="M324.926,298.327c-4.127-25.665-12.625-58.724-29.668-70.472c-11.638-8.024-52.243-29.718-69.582-38.982l-0.3-0.16   c-1.982-1.059-4.402-0.847-6.17,0.541c-9.083,7.131-19.033,11.937-29.573,14.284c-1.862,0.415-3.39,1.738-4.067,3.521   l-10.733,28.291l-10.733-28.291c-0.677-1.783-2.205-3.106-4.067-3.521c-10.54-2.347-20.49-7.153-29.573-14.284   c-1.768-1.388-4.188-1.601-6.17-0.541c-17.133,9.155-58.235,31.291-69.831,39.107c-19.619,13.217-28.198,61.052-29.718,70.507   c-0.151,0.938-0.063,1.897,0.253,2.792c0.702,1.982,18.708,48.548,149.839,48.548s149.137-46.566,149.839-48.548   C324.989,300.224,325.077,299.264,324.926,298.327z M264.5,282.666l-25.667,8l-25.667-8v-13.81H264.5V282.666z" fill="#FFFFFF"/>
                                                </g>
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="text">
                                            Pentru ca motorul de cautare Dialect Tour te va ajuta sa
                                            gasesti imediat destinatiile tale favorite si cu doar cateva
                                            click-uri iti vei putea rezerva vacanta care ti-o doresti. De
                                            restul ne ocupam noi!
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="sortable-tab tab-left">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#info-hotel" aria-controls="info-hotel" role="tab" data-toggle="tab">
                                Info hotel
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#harta" aria-controls="harta" role="tab" data-toggle="tab">
                                Harta
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#review" aria-controls="review" role="tab" data-toggle="tab">
                                Review
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="info-hotel">
                            <div class="small-title">
                                    <span class="strong-text">
                                        <?php echo $hotel->lang->Name;?>
                                    </span>
                                <span>
                                        - in poze
                                    </span>
                            </div>
                            <div class="hotel-slider">
                                <div class="swiper-container hotel-view">
                                    <div class="swiper-wrapper">
                                        <?php foreach ($hotel->images as $image){?>
                                            <div class="swiper-slide">
                                                <?php
                                                echo Html::img($image->imagePath,['class' => 'img-responsive','alt'=>'']);
                                                ?>
                                            </div>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                                <!-- Add Arrows -->
                                <div class="swiper-button-next"></div>
                                <div class="swiper-button-prev"></div>
                            </div>
                            <div class="small-title">
                                Despre <?php echo $hotel->lang->Name;?>
                            </div>
                            <div class="long-description">
                                <?php echo $hotel->lang->Description;?>
                            </div>
                            <div class="small-title">
                                Camere disponibile
                            </div>

                            <table>
                                <thead>
                                <tr>
                                    <td class="big-width">
                                        Tip camera:
                                    </td>
                                    <td>
                                        Tip masa:
                                    </td>
                                    <td>
                                        Status:
                                    </td>
                                    <td colspan="2" class="big-width">
                                        Tarif total:
                                    </td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="big-width type-room">
                                        1 x STU ES - STUDIO
                                        STANDARD - Top promo -
                                        NON REFUNDABLE (2+0)
                                    </td>
                                    <td>
                                        Fara masa
                                    </td>
                                    <td class="confirm">
                                        Confirmare
                                        imediata
                                    </td>
                                    <td  class="big-width">
                                                <span class="price">
                                                    36 EUR
                                                </span>
                                        <a href="#" class="reservations">
                                            Rezerva
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="big-width type-room">
                                        1 x STU ES - STUDIO
                                        STANDARD - Top promo -
                                        NON REFUNDABLE (2+0)
                                    </td>
                                    <td>
                                        Fara masa
                                    </td>
                                    <td class="confirm">
                                        Confirmare
                                        imediata
                                    </td>
                                    <td class="big-width">
                                                <span class="price">
                                                    36 EUR
                                                </span>
                                        <a href="#" class="reservations">
                                            Rezerva
                                        </a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                            <div class="small-title">
                                Modifica criteriile de cautarea
                            </div>
                            <div class="filter-tab">
                                <div class="content-filter">
                                    <!-- Tab panes -->
                                    <?php  echo $this->render('_search2', ['model' => $searchModel]); ?>
                                </div>
                            </div>



                        </div>
                        <div role="tabpanel" class="tab-pane" id="harta">
                            <div id="map-2">

                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="review">
                            miau
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 hidden-sm hidden-xs">
                <div class="right-side-bar">
                    <div class="help-user">
                        <div class="small-title">
                            Ai nevoie de ajutor?
                        </div>
                        <div class="short-description">
                            Dialect Tour iti sta la dispozitie cu orice informatie
                            despre ofertele prezentate pe acest site.
                        </div>
                        <div>
                            <a href="#" class="phone">
                                <span class="fa fa-phone"></span>
                                +373 12345678
                            </a>
                        </div>
                        <div>
                            <a href="#" class="email">
                                <span class="fa fa-envelope"></span>
                                info@dialect.com
                            </a>
                        </div>
                        <a href="#" class="chat-online">
                            Chat Online
                        </a>
                    </div>
                    <div class="why-us">
                        <div class="small-title">
                            De ce Dialect Tour?
                        </div>
                        <div class="facility-box">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="icon-box">
                                        <span class="fa fa-building"></span>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="text">
                                        Pentru ca hotelurile si pachetele noastre de vacanta au fost
                                        alese cu grija astfel incat tu sa ai parte de cele mai bune
                                        servicii la tarife preferentiale.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="facility-box">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="icon-box">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 424.98 424.98" style="enable-background:new 0 0 424.98 424.98;" xml:space="preserve">
                                                <g>
                                                    <g>
                                                        <g>
                                                            <path d="M241.395,282.304c-1.587-1.738-3.595-3.038-5.67-4.121c-4.518-2.356-9.459-3.785-14.365-5.075v38.016     c7.963-0.9,17.105-3.79,21.286-11.224l0,0c1.996-3.551,2.393-7.914,1.58-11.867C243.785,285.891,242.874,283.925,241.395,282.304     z" fill="#FFFFFF"/>
                                                        </g>
                                                        <g>
                                                            <path d="M242.604,299.973c0.016-0.027,0.025-0.044,0.042-0.073l0,0C242.632,299.924,242.618,299.948,242.604,299.973z" fill="#FFFFFF"/>
                                                        </g>
                                                        <g>
                                                            <path d="M184.009,221.532c-1.369,1.999-2.228,4.27-2.465,6.684c-0.237,2.419-0.104,5.11,0.815,7.387     c0.875,2.17,2.708,3.772,4.6,5.062c2.123,1.444,4.458,2.572,6.836,3.528c1.995,0.803,4.239,1.571,6.658,2.313v-34.4     C194.342,213.41,187.665,216.194,184.009,221.532z" fill="#FFFFFF"/>
                                                        </g>
                                                        <g>
                                                            <path d="M242.804,299.619c-0.05,0.089-0.104,0.182-0.157,0.28l0,0C242.709,299.785,242.758,299.701,242.804,299.619z" fill="#FFFFFF"/>
                                                        </g>
                                                        <g>
                                                            <path d="M243.004,299.263C243.017,299.239,243.019,299.237,243.004,299.263L243.004,299.263z" fill="#FFFFFF"/>
                                                        </g>
                                                        <g>
                                                            <path d="M234.753,92.469c32.329-27.158,53.931-88.341,40.637-91.017c-17.664-3.557-56.022,12.04-74.562,14.788     c-26.296,3.175-54.936-28.515-71.012-10.851c-13.071,14.362,9.371,66.592,44.482,89.346     C69.546,146.219-77.69,404.673,179.171,423.426C534.582,449.375,356.615,142.639,234.753,92.469z M265.276,296.298     c-1.093,10.076-6.433,19.188-14.415,25.374c-8.428,6.532-18.999,9.57-29.502,10.421v11.133c0,2.979-1.301,5.86-3.531,7.832     c-3.065,2.712-7.569,3.381-11.289,1.667c-3.673-1.69-6.086-5.457-6.086-9.499v-12.168c-1.801-0.342-3.589-0.749-5.356-1.234     c-9.816-2.697-18.921-7.954-25.572-15.732c-3.313-3.877-6.014-8.276-7.882-13.025c-0.488-1.241-0.923-2.505-1.304-3.783     c-0.345-1.157-0.701-2.333-0.824-3.539c-0.207-2.023,0.194-4.087,1.137-5.889c1.938-3.707,6.022-5.946,10.192-5.574     c4.104,0.364,7.701,3.212,8.993,7.124c0.398,1.205,0.668,2.44,1.115,3.632c0.443,1.184,0.978,2.335,1.607,3.431     c1.242,2.158,2.798,4.148,4.59,5.875c3.694,3.559,8.399,5.872,13.304,7.248v-41.362c-9.591-2.483-19.491-5.69-27.411-11.848     c-3.849-2.994-7.115-6.714-9.254-11.117c-2.257-4.647-3.192-9.824-3.23-14.966c-0.039-5.221,0.953-10.396,3.131-15.153     c2.04-4.454,4.977-8.453,8.578-11.768c7.7-7.087,17.928-11.04,28.187-12.492v-0.91v-10.647c0-2.978,1.301-5.86,3.531-7.832     c3.066-2.711,7.568-3.381,11.289-1.667c3.672,1.691,6.086,5.457,6.086,9.499v10.647v0.847c1.367,0.172,2.73,0.378,4.086,0.624     c10.074,1.823,19.927,5.983,27.294,13.246c3.49,3.44,6.347,7.539,8.356,12.009c0.561,1.247,1.052,2.523,1.477,3.824     c0.396,1.213,0.794,2.462,0.983,3.728c0.302,2.021-0.006,4.109-0.871,5.958c-1.772,3.788-5.746,6.2-9.927,6.021     c-4.108-0.179-7.83-2.854-9.301-6.694c-0.438-1.142-0.657-2.351-1.104-3.49c-0.451-1.153-1.035-2.253-1.708-3.292     c-1.308-2.02-3.003-3.752-4.938-5.179c-4.19-3.094-9.272-4.706-14.35-5.607v39.582c6.035,1.445,12.075,3.021,17.857,5.301     c8.739,3.446,17.02,8.73,21.79,17.062c-0.74-1.298-1.46-2.563,0.025,0.043c1.458,2.56,0.762,1.34,0.03,0.057     C264.854,280.704,266.101,288.701,265.276,296.298z" fill="#FFFFFF"/>
                                                        </g>
                                                        <g>
                                                            <path d="M242.493,300.169c-0.061,0.109-0.114,0.205-0.156,0.278C242.373,300.384,242.427,300.289,242.493,300.169z" fill="#FFFFFF"/>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="text">
                                        Pentru ca o vacanta fantastica este si mai frumoasa daca
                                        vine la un tarif redus. De aceea, noi iti oferim cele mai bune
                                        oferte, pe care nu le vei mai gasi nicaieri, astfel incat si
                                        portofelul tau sa se relaxeze in vacanta.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="facility-box">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="icon-box">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 349.667 349.667" style="enable-background:new 0 0 349.667 349.667;" xml:space="preserve" width="512px" height="512px">
                                                <g>
                                                    <path d="M174.833,197.204c24.125,0,80.846-29.034,80.846-98.603C255.68,44.145,248.329,0,174.833,0   c-73.495,0-80.846,44.145-80.846,98.602C93.987,168.17,150.708,197.204,174.833,197.204z M106.07,82.146   c5.679-10.983,17.963-23.675,44.381-23.112c0,0,15.746,38.194,93.05,21.042c0.312,6.101,0.41,12.326,0.41,18.526   c0,34.005-15.015,55.075-27.612,66.762c-15.872,14.727-33.494,20.072-41.466,20.072c-7.972,0-25.594-5.345-41.466-20.072   c-12.597-11.687-27.612-32.757-27.612-66.762C105.756,93.101,105.836,87.581,106.07,82.146z" fill="#FFFFFF"/>
                                                    <path d="M324.926,298.327c-4.127-25.665-12.625-58.724-29.668-70.472c-11.638-8.024-52.243-29.718-69.582-38.982l-0.3-0.16   c-1.982-1.059-4.402-0.847-6.17,0.541c-9.083,7.131-19.033,11.937-29.573,14.284c-1.862,0.415-3.39,1.738-4.067,3.521   l-10.733,28.291l-10.733-28.291c-0.677-1.783-2.205-3.106-4.067-3.521c-10.54-2.347-20.49-7.153-29.573-14.284   c-1.768-1.388-4.188-1.601-6.17-0.541c-17.133,9.155-58.235,31.291-69.831,39.107c-19.619,13.217-28.198,61.052-29.718,70.507   c-0.151,0.938-0.063,1.897,0.253,2.792c0.702,1.982,18.708,48.548,149.839,48.548s149.137-46.566,149.839-48.548   C324.989,300.224,325.077,299.264,324.926,298.327z M264.5,282.666l-25.667,8l-25.667-8v-13.81H264.5V282.666z" fill="#FFFFFF"/>
                                                </g>
                                            </svg>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="text">
                                        Pentru ca motorul de cautare Dialect Tour te va ajuta sa
                                        gasesti imediat destinatiile tale favorite si cu doar cateva
                                        click-uri iti vei putea rezerva vacanta care ti-o doresti. De
                                        restul ne ocupam noi!
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="recomandations">
    <div class="container">
        <div class="title different-title">
                <span class="strong-text">
                    Iti recomandam
                </span>
            <span>
                    in <?= $hotel->location->lang->Name?> , <?= $hotel->country->lang->Name ?> si hotelurile
                </span>
        </div>
        <div class="row">
            <?foreach ($recomandations as $recomandation) {
                ?>
                <a href="<?= $recomandation->seoUrl;?>">
                <div class="col-md-3">
                    <div class="recomandation-box">
                        <div class="img-box">

                            <?php
                            if ($recomandation->mainImage){
                            echo Html::img($recomandation->mainImage->imagePath,['class' => 'img-responsive','alt'=>'']);
                            }else {
                                ?>
                                <img class="img-responsive" src="<?= $bundle->baseUrl?>/images/recomandation-img.png" alt="">
                                <?php
                            }
                            ?>
                        </div>
                        <div class="info">
                            <div class="title">
                                <?= $recomandation->lang->Name?>
                            </div>
                            <div class="some-text">
                                <?= $recomandation->lang->Address?>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="some-text small-text">
                                        CEL MAI BUN TARIF
                                    </div>
                                    <div class="price">
                                        140 EURO
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <a href="<?= $recomandation->seoUrl;?>" class="reservations">
                                        Rezerva
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </a>
                <?php
            }
            ?>
        </div>
    </div>
</section>
