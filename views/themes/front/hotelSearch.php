<?php


use yii\helpers\Html;
use app\components\GridView\GridView;
use yii\widgets\Pjax;
use yii\widgets\ListView;
use app\modules\Post\models\Post;


/* @var $this yii\web\View */
/* @var $searchModel app\modules\Post\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<section class="name-page big-padding-bottom">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="title">
                    Căutare
                </div>
            </div>
            <div class="col-md-6">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html">
                            ACASA
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        TOATE DESTINATIILE
                    </li>

                </ol>
            </div>
        </div>
    </div>
</section>

<div class="post-index">

    <div class="filter">
        <div class="container">
                <?php echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <section class="sortable">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="sortable-tab">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#hotel-view" aria-controls="hotel-view" role="tab" data-toggle="tab">
                                    Hotel view
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#maps-hotels" aria-controls="maps-hotels" role="tab" data-toggle="tab">
                                    Map view
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="hotel-view">
                            <?php
                            echo ListView::widget([
                                'dataProvider' => $dataProvider,
                                'itemView' => '_hotel',
                                'pager' => [
                                    'prevPageLabel' => 'previous',
                                    'nextPageLabel' => 'next',
                                    'maxButtonCount' => 5,
                                ]]);
                            ?>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="maps-hotels">
                            <div id="map-1">

                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-3 hidden-sm hidden-xs">
                    <div class="right-side-bar">
                        <div class="help-user">
                            <div class="small-title">
                                Ai nevoie de ajutor?
                            </div>
                            <div class="short-description">
                                Dialect Tour iti sta la dispozitie cu orice informatie
                                despre ofertele prezentate pe acest site.
                            </div>
                            <div>
                                <a href="#" class="phone">
                                    <span class="fa fa-phone"></span>
                                    +373 12345678
                                </a>
                            </div>
                            <div>
                                <a href="#" class="email">
                                    <span class="fa fa-envelope"></span>
                                    info@dialect.com
                                </a>
                            </div>
                            <a href="#" class="chat-online">
                                Chat Online
                            </a>
                        </div>
                        <div class="filter-hotel">
                            <?php echo $this->render('_hotel_filter', ['model' => $searchModel]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>
