var Maper = {
    hotelsMap: function () {
        var map1 = new google.maps.Map(document.getElementById('map-1'), {
            zoom: 13,
            center: {lat: 48.1707422, lng: 27.2683663},
            zoomControl: false,
            disableDoubleClickZoom: true,
            disableDefaultUI: true,
            scrollwheel: false
        });
    },
    hotelMap: function () {
        var map2 = new google.maps.Map(document.getElementById('map-2'), {
            zoom: 13,
            center: {lat: 48.1707422, lng: 27.2683663},
            zoomControl: false,
            disableDoubleClickZoom: true,
            disableDefaultUI: true,
            scrollwheel: false
        });
    },
    contacts_pageMap: function () {
        var map3 = new google.maps.Map(document.getElementById('map-3'), {
            zoom: 13,
            center: {lat: 48.1707422, lng: 27.2683663},
            zoomControl: false,
            disableDoubleClickZoom: true,
            disableDefaultUI: true,
            scrollwheel: false
        });
    }
};

$(document).ready(function(){

    /* FIXED HEADER */
    $window = $(window);

    $window.scroll(function() {
        if ( $window.scrollTop() >  10 && $(window).width() >  992) {
            $(".header-menu").css({
                position: 'fixed' ,
                transition: 'all 0.3s ease-in' ,
                left: 0 ,
                top: 0 ,
                width: "100%" ,
                'z-index': '10' ,
                background: '#fff'
            });
            $("header .header-bar").slideUp( 100 );
            $(".top-nav nav > ul").css({
                padding: "9px 0" ,
                margin: 0
            });
            $("header .logo").css({
               'max-width': '90px',
                padding: '5px'
            });
        }
        else if($(window).scrollTop() < 10 ){
            $(".header-menu").css({
                transition: 'all 0.3s ease-in' ,
                position: 'static' ,
                left: 0 ,
                right: 0 ,
                width: "100%" ,
                'z-index': '10' ,
                background: 'inhirit'
            });
            $("header .header-bar").slideDown(100);
            $(".top-nav nav > ul").css({
                transition: 'all 0.3s ease-in' ,
                padding: "20px 0" ,
                margin: '0'
            });
            $("header .logo").css({
                transition: 'all 0.3s ease-in' ,
                'max-width': '140px',
                padding: '5px'
            });
        }
    });


    $('a[href="#maps-hotels"]').on('shown.bs.tab', function (e) {
        e.preventDefault();
        Maper.hotelsMap();
    });

    $('a[href="#harta"]').on('shown.bs.tab', function (e) {
        e.preventDefault();
        Maper.hotelMap();
    });


    var swiper = new Swiper('.presentation-slider', {
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        slidesPerView: 1,
        paginationClickable: true,
        loop: true,
        pagination: '.presentation-slider-pagination'
    });

    var swiper = new Swiper('.hotel-view', {
        pagination: '.swiper-pagination',
        slidesPerView: 3,
        paginationClickable: true,
        spaceBetween: 50,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev'
    });

    var swiper = new Swiper('.search-hotel', {
        pagination: '.swiper-pagination',
        slidesPerView: 4,
        paginationClickable: true,
        spaceBetween: 50,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev'
    });



    /* HEADER TOGGLE SUBMENU */

    $('.open-submenu').on("click", function(e){
        e.preventDefault();
        $(this).toggleClass("active");
        $("*").not(this).removeClass("active");
        $("ul.toggle-submenu").not($(this).closest("li").find("ul")).slideUp();
        $(this).closest("li").find("ul").slideToggle();
    });

    $('#check-in').datepicker({
        calendarWeeks: true,
        todayHighlight: true
    });
    $('#check-out').datepicker({
        calendarWeeks: true,
        todayHighlight: true
    });

    /* TOGGLE MENU FOR MOBILE */

    $(".open-nav").click(function(){
       $(".top-nav").find("nav").slideToggle();
    });


    /* SELECT 2*/


    $('.style-select').select2();

    /* FORM STYLER */

    $('.style-checkbox').styler();

    $("#write-review .submit-btn").click(function(){
        $(".message-for-user").slideToggle(300);
    });


    $(".show-content-form").click(function(){
       $(this).closest(".auth").find(".hidden-content").slideDown();
       $(this).attr("type", "submit");
    });

    $(".open-help-user").click(function(e){
        e.preventDefault();
        $(".toggle-help").slideToggle();
    });

    $(".toggle-facility-box").click(function(e){
        e.preventDefault();
       $(".facility-toggle").slideToggle();
    });


    /* TOGGLE FOR QUESTIONS */


    $(".questions").click(function(){
       var curent =  $(this).toggleClass("active-toggle").next(".answer").slideToggle(200);
       $(".answer").not(curent).slideUp(200);
    });

    $(".questions:last").removeClass("border-questions");
    
    
    $('select[name="SearchFormModel[Rooms]"]').change(function(){
        $('.room-controls').not(':first').hide().find('select').attr('disabled', 'disabled');

        var rooms = parseInt($(this).val());

        for (i = rooms - 1; i > 0; i--)
        {
            $('.room-controls').eq(i).show().find('select').removeAttr('disabled');
        }
    });
    
    $('select[name=Rooms]').change(function(){
        $('.room-controls').not(':first').hide().find('select').attr('disabled', 'disabled');

        var rooms = parseInt($(this).val());

        for (i = rooms - 1; i > 0; i--)
        {
            $('.room-controls').eq(i).show().find('select').removeAttr('disabled');
        }
    });

    $('select[name^=Childs]').change(function(){
        $(this).closest('.room-controls').find('.child-age-control').hide().find('select').attr('disabled', 'disabled');
        var childs = parseInt($(this).val());
        for (i = childs - 1; i >= 0; i--)
        {
            $(this).closest('.room-controls').find('.child-age-control').eq(i).show().find('select').removeAttr('disabled');
        }

    });

    $(".open-all-room").click(function(e){
        e.preventDefault();
       $(this).closest(".sejur-item").find(".toggling-room").slideToggle();
       $(this).find(".fa-plus-square").toggleClass("hidden-icon");
       $(this).find(".fa-minus-square").toggleClass("hidden-icon");
    });

    if ($(".filter-price").length > 0)
    {
        initPriceSlider();
    }

});

/* VALIDATE FORM */
function validateForm(){
    $(".require-form").validate(
        {
            language: "ro",
            messages: {
                name: {
                    pattern: "Introduceti numele Dvs."
                },
                number: {
                    pattern: "Introduceti numarul de telefon"
                },
                email: {
                    pattern: "Introduceti email-ul Dvs."
                }
            }
        }
    );
    superplaceholder({
        el: inp3,
        sentences: [ 'info@mail.ru', 'info@icloud.com', 'info@gmail.com', 'info@yahoo.com' ],
        options: {
            letterDelay: 390,
            loop: true,
            startOnFocus: false
        }
    });
    superplaceholder({
        el: inp1,
        sentences: [ 'Andrei' ],
        options: {
            letterDelay: 120,
            loop: true,
            startOnFocus: false
        }
    });
    superplaceholder({
        el: inp2,
        sentences: [ '+37369123940' ],
        options: {
            letterDelay: 200,
            loop: true,
            startOnFocus: false
        }
    });

}

function initPriceSlider(min = 0, max = 10000)
{
    try {
        if ($(".filter-price").slider("isEnabled"))
        {
            $(".filter-price").slider('destroy');
        }

        $(".filter-price").slider('destroy');
    }
    catch (exception)
    {
        
    }

    
    $(".filter-price").slider({min: min, max: max, step: 10, range: true}).on('slideStop', function(e) {
        $('input[name="MinPrice"]').val(e.value[0]);
        $('input[name="MaxPrice"]').val(e.value[1]);
        $('input[name="MinPrice"]').trigger('change');
    });
}


/* PRELOADER PAGE */

// var loading_screen = pleaseWait({
//     logo: "",
//     backgroundColor: 'rgba(50,166,243, .8)',
//     loadingHtml: "<div class='title-preloader'>DialectTur</div>" +
//     "<div class='sk-folding-cube'>" +
//     "<div class='sk-cube1 sk-cube'> </div>" +
//     "<div class='sk-cube2 sk-cube'></div> " +
//     " <div class='sk-cube4 sk-cube'> </div> " +
//     "<div class='sk-cube3 sk-cube'></div> </div>"
// });
//
//
// var ele = $(".pg-loading-screen");
// var removeMe = $("body");
// var showMe = $(".wrapper");
// setTimeout(function(){showMe.addClass("show-wrapper")} , 3000);
// setTimeout(function(){removeMe.removeClass("pg-loading")} , 3000);
// setTimeout(function() { ele.hide(); }, 3000);




////


$(document).ready(function(){
    $('input[name=CheckIn], input[name=CheckOut], select[name=Nights]').change(function(){
        
        var changed = $(this).attr('name');
        
        $.get('/home/recalc-date', {CheckIn: $('input[name=CheckIn]').val(), CheckOut: $('input[name=CheckOut]').val(), Nights: $('select[name=Nights]').val(), Changed: changed}, function(json){
            $('input[name=CheckIn]').val(json.CheckIn);
            $('input[name=CheckOut]').val(json.CheckOut);
            $('select[name=Nights]').val(json.Nights);
            setTimeout(function(){
                $('select[name=Nights]').select2();
            }, 300);
        }, 'json');
    });
});


