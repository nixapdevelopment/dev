<?php

namespace app\views\themes\front\assets;

use yii\web\AssetBundle;

class FrontAsset extends AssetBundle
{
    public $sourcePath = '@app/views/themes/front/assets/files';

    public $css = [
        'css/bootstrap.min.css',
        'css/font-awesome.min.css',
        'css/swiper.min.css',
        'css/please-wait.css',
        'css/bootstrap-datepicker.css',
        'css/select2.min.css',
        'css/jquery.formstyler.css',
        'css/normalize.css',
        'css/demo.css',
        'css/set2.css',
        'css/lightbox.min.css',
        'css/bootstrap-slider.min.css',
        'css/main.css',
    ];

    public $js = [
        'js/jquery.matchHeight-min.js',
        'js/swiper.min.js',
        'js/jquery.scrollme.js',
        'js/please-wait.min.js',
        'js/bootstrap-datepicker.min.js',
        'js/select2.min.js',
        'js/jquery.formstyler.min.js',
        'js/skills1.js',
        'js/jquery.easyPaginate.js',
        'js/lightbox.min.js',
        'js/bootstrap-slider.min.js',
        'js/scripts.js',

    ];


    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'rmrevin\yii\fontawesome\AssetBundle'
    ];
}