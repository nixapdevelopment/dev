<?php
namespace app\controllers;

use Yii;
use yii\helpers\Url;
use app\modules\Booking\models\Booking;
use app\modules\Booking\models\BookingClient;
use app\modules\Booking\models\CreditCardForm;
use app\modules\Operator\Operator\models\Operator;


class BookController extends FrontController
{
    
    public function actionAccept()
    {
        $bookingModel = new Booking();
        
        Yii::$app->session->set('orderData', Yii::$app->request->post());

        if (Yii::$app->request->isPost)
        {
            $bookingModel->OperatorID = Yii::$app->request->post('OperatorID');
            $bookingModel->ExternalID = Yii::$app->request->post('ExternalID');
            $bookingModel->Hash = Yii::$app->request->post('Hash');
            $bookingModel->RateKey = Yii::$app->request->post('RateKey');
            $bookingModel->RateType = Yii::$app->request->post('RateType');
            $bookingModel->Amount = Yii::$app->request->post('Amount');
            $bookingModel->PaymentStatus = Booking::PaymentStatusPending;
            $bookingModel->PaymentType = 'Pay';
            $bookingModel->Type = Booking::TypeHotel;
            
            $operator = Operator::findOne($bookingModel->OperatorID);

            $providerClass = $operator->Class;
            $provider = new $providerClass();
            
            if ($provider->checkRate($bookingModel->RateKey))
            {
                $bookingModel->save();
                return $this->redirect(['prebooking', 'id' => $bookingModel->ID]);
            }
            else
            {
                echo 'Error';
            }
        }
    }

    public function actionPrebooking($id)
    {
        $bookingModel = Booking::findOne($id);
        $bookingClientModel = new BookingClient();
        
        $bookingClientModel->BookingID = $bookingModel->ID;

        if ($bookingClientModel->load(Yii::$app->request->post()) && $bookingClientModel->save())
        {
            if ($bookingModel->RateType == 'BOOKABLE')
            {
                $redirect = Url::to(['payment', 'id' => $bookingModel->ID]);
            }
            else
            {
                $redirect = Url::to(['succeess']);
            }
            
            $bookingInfoModel = new \app\modules\Booking\models\BookingInfo([
                'BookingID' => $bookingModel->ID,
                'Data' => serialize(Yii::$app->session->get('searchData')),
            ]);
            $bookingInfoModel->save();
            
            return $this->redirect($redirect);
        }
        
        return $this->render('prebooking', [
            'bookingModel' => $bookingModel,
            'bookingClientModel' => $bookingClientModel,
        ]);
    }
    
    public function actionPayment($id)
    {
        $bookingModel = Booking::find()->with('bookingClient')->where(['ID' => $id])->one();
        
        if ($bookingModel->RateType == 'BOOKABLE')
        {
            $operator = Operator::findOne($bookingModel->OperatorID);
            $providerClass = $operator->Class;
            $provider = new $providerClass();
            
            $model = new CreditCardForm();
            
            if ($model->load(Yii::$app->request->post()) && $model->validate())
            {
                $response = $provider->startPayment($bookingModel, $model);
                
                if ($response)
                {
                    $bookingModel->PaymentStatus = Booking::PaymentStatusPaid;
                    $bookingModel->save();
                    
                    return $this->redirect(['success', 'id' => $bookingModel->ID]);
                }
            }
            
            return $this->render('payment', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionSuccess($id)
    {
        $bookingModel = Booking::findOne($id);
        
        return $this->render('success', [
            'model' => $bookingModel,
        ]);
    }
    
}


