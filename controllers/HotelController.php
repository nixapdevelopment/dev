<?php
namespace app\controllers;

use Yii;
use app\modules\Hotel\models\Hotel;
use app\modules\Hotel\models\HotelFrontSearch;
use app\modules\Booking\models\SearchInfo;

class HotelController extends FrontController
{

    public function actionIndex($id,$slug)
    {   
        $searchModel = new HotelFrontSearch();

        $hotel = Hotel::find()->with(['lang','images'])->where(['ID'=>$id])->one();
        $recomandations = Hotel::find()->with(['lang','mainImage'])->where(['LocationID'=>$hotel->LocationID,'CountryID'=>$hotel->CountryID])->limit(8)->all();
        return $this->render('../themes/front/singleHotel', [
            'hotel' => $hotel,
            'searchModel' => $searchModel,
            'recomandations' => $recomandations,
        ]);
    }
    
    public function actionView()
    {
        $id = Yii::$app->request->post('searchID');
        $key = Yii::$app->request->post('key');
        
        $searchInfoModel = SearchInfo::findOne($id);
        
        $cacheFile = Yii::getAlias('@app/modules/HotelSearch/cache/' . $searchInfoModel->Hash . '.txt');
        
        if (file_exists($cacheFile))
        {
            $results = unserialize(file_get_contents($cacheFile));
            
            return $this->render('hotel-view', [
                'hotel' => $results[$key],
            ]);
        }
        else
        {
            exit('ERROR. PLEASE MAKE SEARCH AGAIN.');
        }
    }
    
}