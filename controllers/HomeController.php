<?php
namespace app\controllers;



use app\modules\Newsletter\models\Newsletter;
use app\modules\Parser\models\Tour;
use app\modules\SeoPost\models\SeoPost;
use app\modules\TopDestination\models\TopDestination;
use Yii;
use yii\caching\TagDependency;
use yii\base\Theme;
use app\modules\Post\models\Post;
use yii\web\NotFoundHttpException;
use app\modules\Location\models\Location;
use yii\data\ActiveDataProvider;
use app\modules\Hotel\models\Hotel;
use app\modules\Hotel\models\HotelFrontSearch;
use app\modules\Testimonials\models\Testimonials;
use app\modules\Slider\models\SliderItem;

class HomeController extends FrontController
{
    public function init()
    {
        parent::init();

        Yii::$app->view->theme = new Theme([
            'basePath' => '@app/views/themes/front',
            'pathMap' => [
                '@app/views' => '@app/views/themes/front',
            ]
        ]);
        $this->layout = 'frontend';
    }
    public function actionIndex()
    {
        return $this->redirect(['home/home']);
    }

    public function actionError()
    {
        throw new NotFoundHttpException("Page not found");
    }
    public function actionCountries()
    {
        $searchModel = new HotelFrontSearch();
        $dataProvider = new ActiveDataProvider([
            'query' => Location::find()->with('lang')->where(['Type' => 'Country']),
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render('../themes/front/countries',[


            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,

        ]);
    }
    public function actionRegions($id)
    {

        $dataProvider = new ActiveDataProvider([
            'query' => Location::find()->with('lang')->where(['Type' => Location::TypeRegion, 'ParentID'=> $id]),
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render('../themes/front/regions',[


            'dataProvider' => $dataProvider,

        ]);
    }
    public function actionCities($id)
    {

        $dataProvider = new ActiveDataProvider([
            'query' => Location::find()->with('lang')->where(['Type' => Location::TypeCity, 'ParentID'=> $id]),
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render('../themes/front/cities',[


            'dataProvider' => $dataProvider,

        ]);
    }



    public function actionHome()
    {   
        $news = Post::getDb()->cache(function ($db) {
            return Post::find()->with(['lang','mainImage'])->limit(4)->orderBy('CreatedAt DESC')->all();
        }, 3600, new TagDependency(['tags' => Post::className()]));
        
        $testimonials = Testimonials::getDb()->cache(function ($db) {
            return Testimonials::find()->limit(4)->orderBy('ID DESC')->all();
        }, 3600, new TagDependency(['tags' => Testimonials::className()]));

        $sliderItems = SliderItem::getDb()->cache(function ($db) {
            return SliderItem::find()->with('lang')->where(['SliderID'=>1])->all();
        }, 3600, new TagDependency(['tags' => SliderItem::className()]));

        $topDestinations = TopDestination::getDb()->cache(function ($db) {
            return TopDestination::find()->with('lang')->limit(6)->orderBy('CreatedAt DESC')->all();
        }, 3600, new TagDependency(['tags' => TopDestination::className()]));

        $countHotels = Hotel::getDb()->cache(function ($db) {
            return Hotel::find()->count();
        }, 3600, new TagDependency(['tags' => Hotel::className()]));

        $countLocations = Location::getDb()->cache(function ($db) {
            return Location::find()->count();
        }, 3600, new TagDependency(['tags' => Location::className()]));

        $countSejurs = Tour::getDb()->cache(function ($db) {
            return Tour::find()->count();
        }, 3600, new TagDependency(['tags' => Tour::className()]));

        $seoText = SeoPost::getDb()->cache(function ($db) {
            return SeoPost::find()->with('lang')->limit(2)->all();
        }, 3600, new TagDependency(['tags' => SeoPost::className()]));

        $newsletter = new Newsletter();
        $searchModel = new HotelFrontSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('../themes/front/home', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'news' => $news,
            'testimonials' => $testimonials,
            'sliderItems' => $sliderItems,
            'topDestinations' => $topDestinations,
            'newsletter' => $newsletter,
            'countHotels' => $countHotels,
            'countLocations' => $countLocations,
            'countSejurs' => $countSejurs,
            'seoText' => $seoText,
        ]);
    }
    
    public function actionRecalcDate($CheckIn, $CheckOut = '', $Nights = '', $Changed = 'CheckIn')
    {
        $CheckInTime = strtotime($CheckIn);
        $CheckOutTime = strtotime($CheckOut);
        
        if ($Changed == 'CheckIn')
        {
            $CheckOut = date('d.m.Y', strtotime('+1 day', strtotime($CheckIn)));
            $Nights = 1;
        }
        elseif ($Changed == 'CheckOut')
        {
            if ($CheckOutTime - 86400 <= time())
            {
                $CheckOut = date('d.m.Y', strtotime('+1 day', strtotime($CheckIn)));
                $Nights = 1;
            }
            
            $Nights = round(($CheckOutTime - $CheckInTime) / 86400);
        }
        elseif ($Changed == 'Nights')
        {
            $plus = $Nights == 1 ? '+1 day' : "+$Nights days";
            $CheckOut = date('d.m.Y', strtotime($plus, strtotime($CheckIn)));
        }
        
        echo json_encode([
            'Success' => $Success,
            'CheckIn' => $CheckIn,
            'CheckOut' => $CheckOut,
            'Nights' => $Nights,
        ]);
        
    }
    
}


